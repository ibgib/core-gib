import { GLOBAL_LOG_A_LOT } from "../../core-constants.mjs";
import { WitnessFn } from "../witness-types.mjs";
import { ANONYMOUS_FN_ATOM } from "./anonymous-fn-constants.mjs";
import { AnonymousFnWitnessData_V1 } from "./anonymous-fn-types.mjs";
import { AnonymousFnWitness_V1 } from "./anonymous-fn-v1.mjs";

const logalot = GLOBAL_LOG_A_LOT;

export function getAnonymousFnIb({
    data,
}: {
    data: AnonymousFnWitnessData_V1,
}): string {
    const lc = `[${getAnonymousFnIb.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 37bb06e0390c5bcadf04501727d64923)`); }
        if (!data?.fnHash) { throw new Error(`data.fnHash required (E: 2e560f2c59822bc5ba024d4a8e99d323)`); }
        return `witness ${ANONYMOUS_FN_ATOM} ${data.fnHash}`;
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * wraps a fn in an anonymous witness.
 *
 * @param fn fn to execute
 * @returns anonymous witness ibgib
 */
export async function fnToWitness(fn: WitnessFn): Promise<AnonymousFnWitness_V1> {
    const lc = `[${fnToWitness.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: e64fe14287de9716f2e5ff3bf9d93923)`); }
        let witness = new AnonymousFnWitness_V1(fn);
        return witness;
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

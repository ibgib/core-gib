import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";

import { WitnessData_V1, WitnessRel8ns_V1 } from "../witness-types.mjs";

export interface AnonymousFnWitnessData_V1 extends WitnessData_V1 {
    fnHash: string;
}

export interface AnonymousFnWitnessRel8ns_V1 extends WitnessRel8ns_V1 {
}

export interface AnonymousFnWitnessIbGib_V1
    extends IbGib_V1<AnonymousFnWitnessData_V1, AnonymousFnWitnessRel8ns_V1> {
}

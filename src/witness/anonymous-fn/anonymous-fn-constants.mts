import { AnonymousFnWitnessData_V1 } from "./anonymous-fn-types.mjs";

export const ANONYMOUS_FN_ATOM = 'anon_fn';

export const DEFAULT_ANONYMOUS_FN_DATA_V1: AnonymousFnWitnessData_V1 = {
    fnHash: '',
};

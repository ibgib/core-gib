import { clone, hash } from "@ibgib/helper-gib/dist/helpers/utils-helper.mjs";
import { GIB } from "@ibgib/ts-gib/dist/V1/constants.mjs";
import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";
import { getGib } from "@ibgib/ts-gib/dist/V1/transforms/transform-helper.mjs";

import { Witness, WitnessFn, } from "../witness-types.mjs";
import { ANONYMOUS_FN_ATOM, DEFAULT_ANONYMOUS_FN_DATA_V1 } from "./anonymous-fn-constants.mjs";
import { getAnonymousFnIb } from "./anonymous-fn-helper.mjs";
import { AnonymousFnWitnessData_V1, AnonymousFnWitnessIbGib_V1, AnonymousFnWitnessRel8ns_V1, } from "./anonymous-fn-types.mjs";

/**
 * Helper class that wraps a witness function.
 *
 * @see {@link AnonymousFnWitnessData_V1}
 * @see {@link AnonymousFnWitnessRel8ns_V1}
 * @see {@link WitnessFn}
 */
export class AnonymousFnWitness_V1
    implements
    Witness<IbGib_V1, IbGib_V1, AnonymousFnWitnessData_V1, AnonymousFnWitnessRel8ns_V1>,
    AnonymousFnWitnessIbGib_V1 {

    ib: string = ANONYMOUS_FN_ATOM.concat();
    gib?: string | undefined = GIB;
    data?: AnonymousFnWitnessData_V1 | undefined = clone(DEFAULT_ANONYMOUS_FN_DATA_V1);
    rel8ns?: AnonymousFnWitnessRel8ns_V1 | undefined;

    constructor(public fn: WitnessFn) {
        if (!fn) { throw new Error(`fn required (E: b1e1478f466a2a347b75e7c336f8d323)`); }
    }

    async witness(arg: IbGib_V1): Promise<IbGib_V1 | undefined> {
        this.data!.fnHash = await hash({ s: this.fn.toString() });
        this.ib = getAnonymousFnIb({ data: this.data! });
        this.gib = await getGib({ ibGib: this });
        return this.fn(arg);
    }

}

import { IbGib } from "@ibgib/ts-gib/dist/types.mjs";
import { Witness, WitnessData_V1, WitnessRel8ns_V1 } from "../witness-types.mjs";

export interface WitnessWithContextData_V1 extends WitnessData_V1 {
    /**
     * If provided, this will be the rel8nName used when calling `rel8To` function.
     *
     * ## notes
     *
     * This originated in robbot-base so may not strictly be necessary here.
     */
    defaultRel8nName?: string;
}

export interface WitnessWithContextRel8ns_V1 extends WitnessRel8ns_V1 {
}

export interface WitnessWithContextIbGib_V1<
    TIbGibIn extends IbGib,
    TIbGibOut extends IbGib,
    TData extends WitnessWithContextData_V1 = any,
    TRel8ns extends WitnessWithContextRel8ns_V1 = WitnessWithContextRel8ns_V1,
> extends Witness<TIbGibIn, TIbGibOut, TData, TRel8ns> {
}

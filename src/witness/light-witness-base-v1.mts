import { IbGibRel8ns } from '@ibgib/ts-gib/dist/types.mjs';
import { getIbGibAddr, } from '@ibgib/ts-gib/dist/helper.mjs';
import { IbGib_V1, IbGibRel8ns_V1, sha256v1, } from '@ibgib/ts-gib/dist/V1/index.mjs';
import { getGibInfo } from '@ibgib/ts-gib/dist/V1/transforms/transform-helper.mjs';


import { WitnessData_V1, WitnessRel8ns_V1, Witness_V1, } from './witness-types.mjs';
import { validateGib, validateIb, validateIbGibIntrinsically } from '@ibgib/ts-gib/dist/V1/validate-helper.mjs';
import { ErrorIbGib_V1 } from '../common/error/error-types.mjs';
import { toDto } from '../common/other/ibgib-helper.mjs';
import { GLOBAL_LOG_A_LOT } from '../core-constants.mjs';
import { clone, getUUID, pretty } from '@ibgib/helper-gib';

const logalot = GLOBAL_LOG_A_LOT || false;

export abstract class LightWitnessBase_V1<
    TData extends WitnessData_V1 = WitnessData_V1,
    TRel8ns extends IbGibRel8ns_V1 = IbGibRel8ns_V1
>
    implements Witness_V1<
        any, any, IbGib_V1, // options arg can be any ibgib
        any, any, IbGib_V1, // result can be any ibgib
        TData, TRel8ns                   // this witness itself
    > {

    /**
     * Log context for convenience with logging. (Ignore if you don't want to use this.)
     */
    protected lc: string = `[${LightWitnessBase_V1.name}]`;

    public instanceId: string = '';

    // #region IbGib interface fields: ib, gib, data, rel8ns

    /**
     * Used per use case in implementing class.
     *
     * This property is a simple property (no getter/setter with backing
     * fields).  This is to simplify usage with DTOs (Data Transfer Objects) for
     * storing in spaces.
     */
    ib: string = '';

    /**
     * Used per use case in implementing class.
     *
     * This property is a simple property (no getter/setter with backing
     * fields).  This is to simplify usage with DTOs (Data Transfer Objects) for
     * storing in spaces.
     */
    gib: string | undefined;

    /**
     * Used per use case in implementing class.
     *
     * This property is a simple property (no getter/setter with backing
     * fields).  This is to simplify usage with DTOs (Data Transfer Objects) for
     * storing in spaces.
     */
    data: TData | undefined;

    /**
     * Used per use case in implementing class.
     *
     * This property is a simple property (no getter/setter with backing
     * fields).  This is to simplify usage with DTOs (Data Transfer Objects) for
     * storing in spaces.
     */
    rel8ns: TRel8ns | undefined;

    // #endregion IbGib interface fields: ib, gib, data, rel8ns

    /**
     * await this to ensure the witness is ready.
     *
     * This is called in the base `Witness.witness` function before `witnessImpl`.
     */
    initialized: Promise<void> | undefined;
    /**
     * synchronous and faster boolean for checking faster.
     *
     * so normally, do a check like...
     *    if (!this._isInitialized) { await this.initialized; }
     */
    protected _isInitialized: boolean = false;

    constructor(initialData?: TData, initialRel8ns?: TRel8ns) {
        if (initialData) { this.data = initialData; }
        if (initialRel8ns) { this.rel8ns = initialRel8ns; }
        this.initialized = this.initialize().then(() => { this._isInitialized = true });
    }

    /**
     * by default, simply returns true atow in base class.
     * override this to perform code before any other code is executed.
     */
    protected async initialize(): Promise<void> {
        this.instanceId = await getUUID();
    }

    /**
     * Creates a data transfer object (dto) snapshot out of this
     * witness' `ib`, `gib`, `data` and `rel8ns` properties.
     *
     * I say "snapshot" because this copies each property
     * (`ib`, `gib`, `data`, `rel8ns`).
     *
     * ## thoughts
     *
     * Witness classes need to be able to persist their ibgib
     * just as regular data. But witnesses have the additional
     * layer of behavior (e.g. the `witness` function) that
     * will not persist (until we get more integrated version control
     * types of functionality in ibgib).
     *
     * @returns dto ibgib object with just clones of this.ib/gib/data/rel8ns props.
     *
     * @see {loadIbGibDto}
     */
    toIbGibDto(): IbGib_V1<TData, TRel8ns> {
        return toDto({ ibGib: this });
    }

    /**
     * (Re)hydrates this witness class with the ibgib information from the dto.
     *
     * ## notes
     *
     * * You can extend this function for witness-specific behavior when loading.
     *
     * @param dto ib, gib, data & rel8ns to load for this witness ibgib instance.
     *
     * @see {toIbGibDto}
     */
    loadIbGibDto(dto: IbGib_V1<TData, TRel8ns>): Promise<void> {
        const lc = `${this.lc}[${this.loadIbGibDto.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }

            if (!dto.ib) { console.warn(`${lc} dto.ib is falsy.`); }
            if (!dto.gib) { console.warn(`${lc} dto.gib is falsy.`); }

            this.ib = clone(dto.ib);
            this.gib = clone(dto.gib);
            if (dto.data) {
                this.data = clone(dto.data);
            } else {
                delete this.data;
            }
            if (dto.rel8ns) { this.rel8ns = clone(dto.rel8ns); } else { delete this.rel8ns; }

            return Promise.resolve();
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * The primary function of a witness is...well... to witness things.
     *
     * So this is the base implementation that includes validation
     * plumbing, tracing, error checking/catching - all depending
     * on witness configuration.
     *
     *
     * ## usage
     *
     * Only override this function if you really want custom handling of
     * the plumbing.  Instead override `witnessImpl`.
     *
     * {@see validateThis}
     * {@see validateWitnessArg}
     *
     * @param arg
     * @returns
     */
    abstract witness(arg: IbGib_V1): Promise<IbGib_V1 | undefined>;

    /**
     * Validate this witness object, checking its own `data` and `rel8ns`, and
     * possibly other state.
     *
     * ## notes
     *
     * ATOW base implementation of this just checks for non-falsy
     * `this.ib` and `this.gib`
     */
    protected async validateThis(): Promise<string[]> {
        const lc = `${this.lc}[${this.validateThis.name}]`;
        const errors: string[] = [];
        try {
            if (!this.ib) { errors.push(`this.ib is falsy.`); }
            if (!this.gib) { errors.push(`this.gib is falsy.`); }
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        }
        return errors;
    }

}

/**
 * LightWitness with `any` types in generic.
 */
export type LightWitnessAny = LightWitnessBase_V1<any, any>;

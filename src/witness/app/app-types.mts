import { IbGibAddr, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/index.mjs';


import {
  WitnessData_V1, WitnessRel8ns_V1,
  WitnessResultData, WitnessResultRel8ns, WitnessResultIbGib,
} from '../witness-types.mjs';
import { IbGibSpaceData_Subpathed } from '../space/space-types.mjs';
import { IbGibAppAny } from './app-base-v1.mjs';
import { WitnessCmdData, WitnessCmdRel8ns, WitnessCmdIbGib, } from '../witness-cmd/witness-cmd-types.mjs';



/**
 * ...hmm...?
 *
 * This originally descends from IonicSpaceData_V1. I need to figure out what
 * this is doing, because this shouldn't be scoped to even subpathed strategy.
 * That is entirely an implementation decision.
 */
export interface AppSpaceData extends IbGibSpaceData_Subpathed { }

export enum AppSpaceRel8n {
  roots = 'roots',
  tags = 'tags',
  latest = 'latest',
  outerspaces = 'outerspaces',
  secrets = "secrets",
  encryptions = "encryptions",
}

export interface AppSpaceRel8ns extends IbGibRel8ns_V1 {
  [AppSpaceRel8n.tags]?: IbGibAddr[];
  [AppSpaceRel8n.roots]?: IbGibAddr[];
  [AppSpaceRel8n.latest]?: IbGibAddr[];
  [AppSpaceRel8n.outerspaces]?: IbGibAddr[];
  [AppSpaceRel8n.secrets]?: IbGibAddr[];
  [AppSpaceRel8n.encryptions]?: IbGibAddr[];
}

/**
 * An app space itself works as the config instead of
 * having an external config object.
 *
 * ## notes
 *
 * Probably need to change this at some point...
 */
export interface ConfigIbGib_V1 extends IbGib_V1<AppSpaceData, AppSpaceRel8ns> { }


export interface AppData_V1 extends WitnessData_V1 {

  /**
   * Name of a App instance...after all, Robbie was only the first App.
   */
  name: string;

  /**
   * Redeclared over {@link WitnessData_V1.uuid} making it a required field.
   */
  uuid: string;

  /**
   * Right now, this will be just for ionicons.
   */
  icon: string;

}

export interface AppRel8ns_V1 extends WitnessRel8ns_V1 {
}

/**
 */
export interface AppIbGib_V1<TData extends AppData_V1 = any, TRel8ns extends AppRel8ns_V1 = any>
  extends IbGib_V1<TData, TRel8ns> {
}


/**
 * Cmds for interacting with ibgib app witnesses.
 *
 * Not all of these will be implemented for witness.
 *
 * ## todo
 *
 * change these commands to better structure, e.g., verb/do/mod, can/get/addrs
 * */
export type AppCmd =
  'ib' | 'gib' | 'ibgib';
/** Cmds for interacting with ibgib app witnesses. */
export const AppCmd = {
  /**
   * it's more like a grunt that is intepreted by context from the app.
   *
   * my initial use of this will be to
   * ...
   */
  ib: 'ib' as AppCmd,
  /**
   * it's more like a grunt that is intepreted by context from the app.
   *
   * my initial use of this will be to indicate to a app ...
   */
  gib: 'gib' as AppCmd,
  /**
   * third placeholder command.
   *
   * I imagine this will be like "what's up", but who knows.
   */
  ibgib: 'ibgib' as AppCmd,
}

/**
 * Flags to affect the command's interpretation.
 */
export type AppCmdModifier =
  'ib' | 'gib' | 'ibgib';
/**
 * Flags to affect the command's interpretation.
 */
export const AppCmdModifier = {
  /**
   * hmm...
   */
  ib: 'ib' as AppCmdModifier,
  /**
   * hmm...
   */
  gib: 'gib' as AppCmdModifier,
  /**
   * hmm...
   */
  ibgib: 'ibgib' as AppCmdModifier,
}

/** Information for interacting with spaces. */
export interface AppCmdData<TData extends AppCmd = any, TRel8ns extends AppCmdModifier = any>
  extends WitnessCmdData<TData, TRel8ns> {
}

export interface AppCmdRel8ns extends WitnessCmdRel8ns {
}

/**
 * Shape of options ibgib if used for a app.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface AppCmdIbGib<
  TIbGib extends IbGib_V1 = IbGib_V1,
  TCmd extends AppCmd = AppCmd,
  TCmdModifier extends AppCmdModifier = AppCmdModifier,
  TCmdData extends AppCmdData<TCmd, TCmdModifier> = AppCmdData<TCmd, TCmdModifier>,
  TCmdRel8ns extends AppCmdRel8ns = AppCmdRel8ns,
> extends WitnessCmdIbGib<TIbGib, AppCmd, AppCmdModifier, TCmdData, TCmdRel8ns> {
}

/**
 * Optional shape of result data to app interactions.
 *
 * This is in addition of course to {@link WitnessResultData}.
 *
 * ## notes
 *
 * * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface AppResultData extends WitnessResultData {
}

/**
 * Marker interface rel8ns atm...
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface AppResultRel8ns extends WitnessResultRel8ns { }

/**
 * Shape of result ibgib if used for a app.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface AppResultIbGib<
  TIbGib extends IbGib_V1,
  TResultData extends AppResultData,
  TResultRel8ns extends AppResultRel8ns
>
  extends WitnessResultIbGib<TIbGib, TResultData, TResultRel8ns> {
}

// /**
//  * When coming from the extension in firefox/chrome(chromium), this info
//  * represents a selection to be converted/saved in an ibgib graph.
//  */
// export interface ExtensionSelectionInfo {
//   type: 'comment' | 'link';
//   text: string;
//   url?: string;
//   children?: ExtensionSelectionInfo[];
// }

/**
 * this is generated in background.js (WARNING! background.js atow is generated
 * from background.v2.js or background.v3.js . ...do NOT make changes directly
 * in background.js!).
 *
 * When the user is in their browser with the ibgib extension and selects some
 * text or clicks an ibgib context menu item to initiate the app, this info is
 * passed to the app via query params (atow).
 */
// export interface ExtensionLaunchInfo {
//   /** brand the event so we know it's ours */
//   ib: boolean;
//   /**
//    * indicate to the receiving angular app that we're launching
//    * from an extension in firefox/chrome. (this is obvious here in
//    * background.js but in the angular app, not so much).
//    */
//   isExtensionLaunch: boolean;
//   /**
//    * So consumer knows where this is coming from. (obvious to us
//    * here, but helps consumer)
//    */
//   lc: string,
//   /**
//    * text of the context menu clicked
//    * @link https://developer.chrome.com/docs/extensions/reference/contextMenus/#type-OnClickData
//    */
//   menuItemId: string;
//   /**
//    * url of the page that _initiates_ the click and starts the app.
//    * so if the user is on wikipedia.org, selects some text and clicks on the ibgib link,
//    * in order to generate some ibgib data based on the page, this will be
//    * https://en.wikipedia.org/wiki/Phanerozoic (or whatever).
//    *
//    * @link https://developer.chrome.com/docs/extensions/reference/contextMenus/#type-OnClickData
//    */
//   pageUrl: string;
//   /**
//    * info about selections user made when starting the (extension) app.
//    *
//    * So if they highlight some text and add it, that is one info. If they highlight a whole paragraph
//    * it will create a comment for the p and possibly children infos for links in that paragraph.
//    *
//    * Same goes for if they select a header tag.
//    *
//    * @link https://developer.chrome.com/docs/extensions/reference/contextMenus/#type-OnClickData
//    */
//   selectionInfos?: ExtensionSelectionInfo[];
// }

export type AppPromptResult = TransformResult<IbGibAppAny>;

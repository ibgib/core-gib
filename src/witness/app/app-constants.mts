import { Ib } from '@ibgib/ts-gib'

export const DEFAULT_META_IB_STARTS: Ib[] = [
    'tags', 'tag ', 'settings', 'setting ', 'witness space ',
]
export const SPECIAL_URLS = [
    'tags', 'home'
];

/**
 * When showing a menu item, this is the max length
 */
export const MENU_ITEM_IB_SUBSTRING_LENGTH = 20;

export const QUERY_PARAM_PAUSED = 'paused';
export const QUERY_PARAM_ROBBOT = 'robbot';

export const APP_NAME_REGEXP = /^[a-zA-Z0-9_\-.]{1,32}$/;
export const APP_REL8N_NAME = 'app';

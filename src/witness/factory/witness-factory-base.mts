/**
 * @module witness-factory-base
 *
 * this was a probably-klugy plumbing attempt for ionic-gib to instantiate ibgib
 * witness objects.
 *
 * Specifically this was to go from angular forms to witness ibgibs. The trick
 * was that ibgib's are data objects and you had to load the witness ibgibs
 * which actually had functions/methods on them.
 *
 * So the workflow was you get a form component with some fields, and you
 * generate the witness via this factory.
 *
 * it worked, but it's not strong I don't think.
 *
 * the main weak point is that i didn't fill out the form-items enough and i
 * never got it so that you could create nested forms.
 *
 * in its favor, i do use this (although klugily) in the ibgib rcli app, where
 * instead of a visual form it prompts the user. terrible ux but it did "work"
 * as a quick and dirty implementation. it could be polished going forward (i
 * dont think the design is inherently flawed).
 */

import { IbGibRel8ns_V1, IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";
import { TransformResult } from "@ibgib/ts-gib/dist/types.mjs";

import { Witness, WitnessData_V1, WitnessRel8ns_V1 } from "../witness-types.mjs";

// import * as c from '../constants';

// const logalot = c.GLOBAL_LOG_A_LOT || false;

/**
 * The idea is that any model can be injected via this factory provider.
 */
export abstract class WitnessFactoryBase<
    TWitnessData extends WitnessData_V1,
    TWitnessRel8ns extends WitnessRel8ns_V1,
    TWitness extends Witness<IbGib_V1, IbGib_V1, TWitnessData, TWitnessRel8ns>
> {
    protected lc: string = `[${WitnessFactoryBase.name}]`;

    /**
     * override this to provide the name of the class for the factory.
     *
     * ## usage
     *
     * atm I'm using this to return the classname for the witness.
     *
     * @example MyWitnessClass.classname
     */
    abstract getName(): string;

    /**
     * creates a new witness. This should return the transform result with the
     * new ibgib being the witness class itself, not just the ibgib dto.
     *
     * So said in another way, the `result.newIbGib` should include the class
     * instantiation which has the `witness` function on it.
     *
     * @see {@link WitnessB}
     */
    abstract newUp({ data, rel8ns }: { data?: TWitnessData, rel8ns?: TWitnessRel8ns }):
        Promise<TransformResult<TWitness>>;

}

export type WitnessFactoryAny = WitnessFactoryBase<any, any, any>;

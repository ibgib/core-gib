import {
    firstOfEach, firstOfAll, ifWe,
    lastOfEach, lastOfAll,
    ifWeMight, iReckon, respecfully
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;
// import { getIbAndGib } from '@ibgib/ts-gib/dist/types.mjs';
// import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/index.mjs';
// import { getGibInfo } from 'ts-gib/dist/V1/transforms/transform-helper';
// import { ROBBOT_SESSION_ATOM } from '../types/robbot.mjs';
// import { WordyRobbot_V1_Factory } from '../witnesses/robbots/wordy-robbot/wordy-robbot-v1';
// import { createCommentIbGib } from './comment';
// import { getSpaceDelimitedSaferRequestText, getRobbotSessionIb, parseRobbotSessionIb } from './robbot';
// import { getTimestampInTicks } from './utils';

import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";
import { getSpaceDelimitedSaferRequestText } from "./robbot-helper.mjs";

await respecfully(sir, 'getRequestTextFromComment', async () => {

    await ifWe(sir, 'should get request text with single-char escape', () => {
        ['?', '??', '/', '/esc'].forEach(escape => {
            ['help', 'hi',].forEach(requestText => {
                const text = escape + requestText;
                const ibGib: IbGib_V1 = {
                    ib: 'comment testib',
                    gib: 'gib',
                    rel8ns: { 'ancestor': ['comment^gib'], },
                    data: { text }
                }
                const resRequestText = getSpaceDelimitedSaferRequestText({ ibGib, requestEscapeString: escape });
                iReckon(sir, resRequestText).isGonnaBe(requestText);
            });
        });
    });
});



// await respecfully(sir, 'RobbotSessionIb', () => {

//     // one big test because jasmine unfortunately makes you declare variables
//     // multiple times to get async working, which doesn't scale, so it's a bad
//     // design no matter what.
//     // https://github.com/jasmine/jasmine/issues/1487 (?)
//     it('get/parse', async () => {
//         let f: WordyRobbot_V1_Factory = new WordyRobbot_V1_Factory();
//         let robbot = (await f.newUp({})).newIbGib;
//         let timestampInTicks = getTimestampInTicks();
//         let commentText = 'hey';
//         let commentIbGib = (await createCommentIbGib({ text: commentText })).newIbGib;
//         // let commentAddr = getIbGibAddr({ ibGib: commentIbGib });
//         let { gib: commentGib } = getIbAndGib({ ibGib: commentIbGib });
//         let commentGibInfo = getGibInfo({ gib: commentGib });
//         let sessionId = 'session_yo';
//         let addlMetadata = 'here_is_some_valid_metadata';
//         let addlMetadata_Invalid = 'invalid spaces here';

//         // #region get/parse normal, no metadata

//         let ib = getRobbotSessionIb({
//             robbot,
//             timestampInTicks,
//             contextTjpGib: commentGibInfo.tjpGib,
//             sessionId,
//         });
//         expect(ib).isGonnaBeTruthy();
//         expect(ib.startsWith(ROBBOT_SESSION_ATOM)).isGonnaBe(true, `doesnt start with robbot session atom. (${ib})`);
//         expect(ib.match(timestampInTicks)).isGonnaBeTruthy(`timestampInTicks not included. (${ib}\n${timestampInTicks})`);
//         expect(ib.match(sessionId)).isGonnaBeTruthy(`sessionId.\nib: ${ib}\nsessionId: ${sessionId}`);
//         expect(ib.match(robbot.data.name)).isGonnaBeTruthy('robbot name');
//         expect(ib.match(robbot.data.uuid)).isGonnaBeTruthy('robbot uuid');

//         let { timestamp, robbotName,
//             robbotClassname, robbotId, robbotTjpGib,
//             sessionId: sessionId_Out, contextTjpGib,
//             addlMetadata: addlMetadata_Out
//         } = parseRobbotSessionIb({ ib });

//         expect(timestamp).isGonnaBe(timestampInTicks, 'timestampInTicks');
//         expect(robbotName).isGonnaBe(robbot.data.name, 'robbot name');
//         expect(robbotClassname).isGonnaBe(robbot.data.classname, 'robbot classname');
//         expect(robbotId).isGonnaBe(robbot.data.uuid, 'robbot uuid');
//         expect(robbotTjpGib).isGonnaBe(getGibInfo({ gib: robbot.gib }).tjpGib, 'robbot tjpgib');
//         expect(sessionId_Out).isGonnaBe(sessionId, 'sessionId');
//         expect(contextTjpGib).isGonnaBe(getGibInfo({ gib: commentIbGib.gib }).tjpGib, 'contextTjpGib');
//         expect(addlMetadata_Out).isGonnaBe(undefined, 'addlMetadata');

//         // #endregion get/parse normal, no metadata

//         // #region with addl metadata

//         ib = getRobbotSessionIb({
//             robbot,
//             timestampInTicks,
//             contextTjpGib: commentGibInfo.tjpGib,
//             sessionId,
//             addlMetadata,
//         });

//         expect(ib).isGonnaBeTruthy();
//         expect(ib.startsWith(ROBBOT_SESSION_ATOM)).isGonnaBe(true, `2 doesnt start with robbot session atom. (${ib})`);
//         expect(ib.match(timestampInTicks)).isGonnaBeTruthy(`2 timestampInTicks not included. (${ib}\n${timestampInTicks})`);
//         expect(ib.match(sessionId)).isGonnaBeTruthy(`2 sessionId.\nib: ${ib}\nsessionId: ${sessionId}`);
//         expect(ib.match(robbot.data.name)).isGonnaBeTruthy('2 robbot name');
//         expect(ib.match(robbot.data.uuid)).isGonnaBeTruthy('2 robbot uuid');
//         expect(ib.match(addlMetadata)).isGonnaBeTruthy('addlMetadata');

//         // thanks https://flaviocopes.com/javascript-destructure-object-to-existing-variable/
//         ({
//             timestamp, robbotName,
//             robbotClassname, robbotId, robbotTjpGib,
//             sessionId: sessionId_Out, contextTjpGib, addlMetadata: addlMetadata_Out
//         } = parseRobbotSessionIb({ ib }));

//         expect(timestamp).isGonnaBe(timestampInTicks, '2 timestampInTicks');
//         expect(robbotName).isGonnaBe(robbot.data.name, '2 robbot name');
//         expect(robbotClassname).isGonnaBe(robbot.data.classname, '2 robbot classname');
//         expect(robbotId).isGonnaBe(robbot.data.uuid, '2 robbot uuid');
//         expect(robbotTjpGib).isGonnaBe(getGibInfo({ gib: robbot.gib }).tjpGib, '2 robbot tjpgib');
//         expect(sessionId_Out).isGonnaBe(sessionId, '2 sessionId');
//         expect(contextTjpGib).isGonnaBe(getGibInfo({ gib: commentIbGib.gib }).tjpGib, '2 contextTjpGib');
//         expect(addlMetadata_Out).isGonnaBe(addlMetadata, 'addlMetadata');

//         // #endregion with addl metadata

//         // #region with INVALID addl metadata

//         expect(() => getRobbotSessionIb({
//             robbot,
//             timestampInTicks,
//             contextTjpGib: commentGibInfo.tjpGib,
//             sessionId,
//             addlMetadata: addlMetadata_Invalid,
//         })).toThrowMatching(e => e.message.match(/26f52b1378d1ad01f20f8ef5a5441722/));

//         // #endregion with INVALID addl metadata
//     });

// })

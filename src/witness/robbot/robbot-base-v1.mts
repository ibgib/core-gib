import { Gib, Ib, IbGibAddr, } from '@ibgib/ts-gib/dist/types.mjs';
import { getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';
import { getGibInfo, IbGib_V1, IbGibRel8ns_V1 } from '@ibgib/ts-gib/dist/V1/index.mjs';

import { GLOBAL_LOG_A_LOT } from '../../core-constants.mjs';
import {
    RobbotData_V1, RobbotRel8ns_V1, RobbotIbGib_V1,
    RobbotCmd, RobbotCmdData, RobbotCmdRel8ns, RobbotCmdIbGib,
    RobbotResultData, RobbotResultRel8ns, RobbotResultIbGib, ROBBOT_MY_COMMENTS_REL8N_NAME,
    ROBBOT_CONTEXT_REL8N_NAME,
    SemanticHandler, SemanticId, SemanticInfo,
    RobbotInteractionData_V1, RobbotInteractionType, RobbotPropsData,
    DEFAULT_ROBBOT_LEX_DATA, DEFAULT_HUMAN_LEX_DATA,
    SemanticHandlerResult,
    ROBBOT_ATOM,
} from './robbot-types.mjs';
import { CommentIbGib_V1 } from '../../common/comment/comment-types.mjs';
import { PicIbGib_V1 } from '../../common/pic/pic-types.mjs';
import { getInteractionIbGib_V1, validateCommonRobbotData } from './robbot-helper.mjs';
import { argy_, isArg, resulty_ } from '../witness-helper.mjs';
import { ErrorIbGib_V1 } from '../../common/error/error-types.mjs';
import { Lex, clone, delay, getTimestamp, getUUID, pretty } from '@ibgib/helper-gib';
import { getErrorIbGib } from '../../common/error/error-helper.mjs';
import { parseCommentIb } from '../../common/comment/comment-helper.mjs';
import { getFromSpace, } from '../space/space-helper.mjs';
import { WitnessWithContextBase_V1 } from '../witness-with-context/witness-with-context-base-v1.mjs';
import { WitnessArgIbGib } from '../witness-types.mjs';

const logalot = GLOBAL_LOG_A_LOT;

export interface RobbotBaseParseMetadataResult {
    robbotClassnameIsh?: string;
    robbotNameIsh?: string;
    robbotIdIsh?: string;
}

/**
 * ## distinguishing characteristics of robbots
 *
 * With any witness ibgib, we are concerned with interpreting an incoming
 * arg ibgib and producing at least one result ibgib. Most often, there will
 * be additional ibgibs created, either creating "new" ibgibs via `fork`
 * transforms, or changing existing ibgibs through `mut8` and `rel8`
 * transforms. And almost always these will be persisted in at least
 * one space.
 *
 * But Robbots in particular are meant to be increasingly adaptable over time.
 *
 * This results in a couple of notes:
 *
 * 1. They should be able to handle any incoming ibgib, including primitives.
 * 2. If they are learning robbots, then they will mutate internally at some rate.
 * 3. Often robbots' output ibgib(s) will be intended for others'
 *    consumption, in service of others - be they humans or other
 *    biologically living organisms, other robbots or even later versions of
 *    themselves.
 *
 * So for example, one of the simplest robbots is one which simply echos the
 * incoming ibgib arg.

 * ## architecture
 *
 * At the base, any robbot should be able to handle any incoming ibgib.  This is
 * in contrast to, e.g., a Space, which is more rigid in design.
 *
 * Certainly there are robbot's who will be more rigid, but this will be an
 * abstract class on top of this one, and all robbots should be able to react to
 * all incoming ibgibs.
 *
 * Output will default to a simple ok^gib or (ROOT) ib^gib primitive...perhaps
 * we'll go with ack^gib, but whatever it is, it's just an acknowledgement of
 * input received.  (I'll put it in the constants file).
 *
 * Side effects should occur in a parallel execution thread, which ideally would
 * work in a completely parallel execution context (like a service worker). But
 * then we have to deal with race conditions and the real solution there is to
 * abstract to the robbot having its own space and the synchronization happening
 * exactly like any other sync space.
 *
 * For now, we'll spin off a promise with some intermittent `await delay`
 * calls if they end up called for, effectively the equivalent of the
 * old-fashioned "ProcessMessages/DoEvents" hack.
 */
export abstract class RobbotBase_V1<
    TLexPropsData extends RobbotPropsData = RobbotPropsData,
    TOptionsData extends any = any,
    TOptionsRel8ns extends IbGibRel8ns_V1 = IbGibRel8ns_V1,
    TOptionsIbGib extends IbGib_V1<TOptionsData, TOptionsRel8ns>
    = IbGib_V1<TOptionsData, TOptionsRel8ns>,
    TResultData extends any = any,
    TResultRel8ns extends IbGibRel8ns_V1 = IbGibRel8ns_V1,
    TResultIbGib extends IbGib_V1<TResultData, TResultRel8ns> | ErrorIbGib_V1
    = IbGib_V1<TResultData, TResultRel8ns>,
    TData extends RobbotData_V1 = RobbotData_V1,
    TRel8ns extends RobbotRel8ns_V1 = RobbotRel8ns_V1,
>
    extends WitnessWithContextBase_V1<
        TOptionsData, TOptionsRel8ns, TOptionsIbGib,
        TResultData, TResultRel8ns, TResultIbGib,
        TData, TRel8ns>
    implements RobbotIbGib_V1 {

    /**
     * Log context for convenience with logging. (Ignore if you don't want to use this.)
     *
     * Often used in conjunction with `logalot`.
     */
    protected lc: string = `[${RobbotBase_V1.name}]`;

    /**
     * When the robbot has to get some ibgibs, might as well store them here so
     * we don't have to get them again from storage.
     */
    protected _cacheIbGibs: { [addr: string]: IbGib_V1 } = {};

    protected _semanticHandlers: { [semanticId: string]: SemanticHandler[] } = {};

    /**
     * lex that the robbot uses to speak.
     */
    protected _robbotLex: Lex<TLexPropsData> | undefined;
    /**
     * lex that the robbot uses to interpret humans.
     */
    protected _userLex: Lex<TLexPropsData> | undefined;

    constructor(initialData?: TData, initialRel8ns?: TRel8ns) {
        super(initialData, initialRel8ns);
        this.initialized = this.initialize();
    }

    /**
     * At this point in time, the arg has already been intrinsically validated,
     * as well as the internal state of this robbot. so whatever this robbot's
     * function is, it should be good to go.
     *
     * In the base class, this just returns {@link routeAndDoArg}. If you don't
     * want to route, then override this.
     */
    protected async witnessImpl(arg: TOptionsIbGib): Promise<TResultIbGib | undefined> {
        const lc = `${this.lc}[${this.witnessImpl.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }

            await this.loadNewerSelfIfAvailable();

            let result: TResultIbGib | undefined;
            if (isArg({ ibGib: arg as IbGib_V1 })) { result = await this.routeAndDoArg({ arg }); }
            if (!result) { result = await this.doDefault({ ibGib: arg }); }
            if (!result) { console.warn(`${lc} result falsy...Could not produce result? Was doDefault implemented in concrete class? (W: 67f5fc5e6e864a13b0053fb54cfba100)`); }
            return result;
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async handleSemanticDefault(info: SemanticInfo): Promise<SemanticHandlerResult> {
        const lc = `${this.lc}[${this.handleSemanticDefault.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 01a2d1781851cc36b674f44b4fb69522)`); }
            if (!this._robbotLex) { throw new Error(`robbotLex not initialized (E: 7be2a470645eab44ca950e9ec8b72723)`); }

            const speech = this._robbotLex.get(SemanticId.unknown, {
                props: (props: TLexPropsData) =>
                    props.semanticId === SemanticId.unknown,
            });

            const data = await this.getRobbotInteractionData({
                type: RobbotInteractionType.info,
                commentText: speech?.text ?? 'hmm, something went wrogn Dave',
            });

            const interaction = await getInteractionIbGib_V1({ data });
            return { interaction };
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // #region initialize

    /**
     * Initializes to default space values.
     */
    protected async initialize(): Promise<void> {
        const lc = `${this.lc}[${this.initialize.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: f0e65ab0f80046a59668ddfbf9f47a4a5)`); }
            await super.initialize();

            await this.initialize_semanticHandlers();
            await this.initialize_lex();
        } catch (error) {
            console.error(`${lc} ${error.message}`);
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async initialize_semanticHandlers(): Promise<void> {
        const lc = `${this.lc}[${this.initialize_semanticHandlers.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: a0f2a11688963b0156e337e7f8604f22)`); }
            this._semanticHandlers = {
                [SemanticId.default]: [
                    {
                        handlerId: "c8054c0b77fb4b37bff693e54e1f66bd",
                        semanticId: SemanticId.default,
                        fnCanExec: (info) => Promise.resolve(true),
                        fnExec: (info) => { return this.handleSemanticDefault(info); },
                    }
                ],
                [SemanticId.unknown]: [
                    {
                        handlerId: "9eb4c537cddb40d4927c2aa58e4e6f4b",
                        semanticId: SemanticId.default,
                        fnCanExec: (info) => Promise.resolve(true),
                        fnExec: (info) => { return this.handleSemanticDefault(info); },
                    }
                ],
            }
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected async initialize_lex(): Promise<void> {
        const lc = `${this.lc}[${this.initialize_lex.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: a4668a7473027e56df42909c09f70822)`); }
            this._robbotLex = new Lex<TLexPropsData>(clone(DEFAULT_ROBBOT_LEX_DATA), {
                defaultPropsMode: 'props',
                defaultKeywordMode: 'all',
                defaultLineConcat: 'paragraph', // outgoing robbot defaults to multiple paragraphs.
            });

            this._robbotLex.data[SemanticId.unknown] = [
                {
                    texts: [
                        `huh?`, // silly
                    ],
                    props: {
                        semanticId: SemanticId.unknown,
                    } as TLexPropsData,
                }
            ];

            this._userLex = new Lex<TLexPropsData>(clone(DEFAULT_HUMAN_LEX_DATA), {
                defaultPropsMode: 'props',
                defaultKeywordMode: 'all',
                defaultLineConcat: 'sentence', // incoming user lex defaults to combining sentences.
            });
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // #endregion initialize

    /**
     * Base routing executes different if incoming is a cmd options arg, i.e.,
     * if the `data.cmd` is truthy (atow). If {@link isArg} is true, then routes
     * to {@link doCmdArg}; else routes to {@link doDefault}.
     *
     * Override this function to create more advanced custom routing.
     *
     * ## notes
     *
     * I'm not overly thrilled with this, but it's a watered down version of
     * what I've implemented in the space witness hierarchy.
     *
     * @see {@link isArg}
     * @see {@link doCmdArg}
     * @see {@link doDefault}.
     */
    protected async routeAndDoArg({
        arg,
    }: {
        arg: TOptionsIbGib,
    }): Promise<TResultIbGib | undefined> {
        const lc = `${this.lc}[${this.routeAndDoArg.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            if (!isArg({ ibGib: arg as IbGib_V1 })) {
                console.error(`${lc} (UNEXPECTED) at this point, ibgib is expected to be arg (via isArg) but this is falsy. this is an error, but not throwing. returning undefined which should be handled, usually by doDefault. (E: 2f93ce1ceac24c72a7d8c25d37ebf5b4)`);
                return undefined; /* <<<< returns early */
            }

            if ((arg.data as any)?.cmd) {
                return this.doCmdArg({ arg: arg as RobbotCmdIbGib<IbGib_V1, RobbotCmdData, RobbotCmdRel8ns> });
            } else {
                console.error(`${lc} (UNEXPECTED) at this point, ibgib is expected to be arg (via arg.data.cmd) but this is falsy. this is an error, but not throwing. returning undefined which should be handled, usually by doDefault. (E: f74935ce9b4442a4be528115f8004ecd)`);
                return undefined; /* <<<< returns early */
            }
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            if (this.data?.catchAllErrors) {
                return (await getErrorIbGib({ rawMsg: error.message })) as TResultIbGib;
            } else {
                throw error;
            }
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // #region do cmd args

    /**
     * By default, this routes to {@link doCmdIb}, {@link doCmdGib} & {@link doCmdIbgib}.
     * Override this and route to other commands before calling this with `super.doCmdArg`
     * (if you still want to use this function.)
     *
     * You can also override the routing
     */
    protected doCmdArg({
        arg,
    }: {
        arg: RobbotCmdIbGib<IbGib_V1, RobbotCmdData, RobbotCmdRel8ns>,
    }): Promise<TResultIbGib> {
        const lc = `${this.lc}[${this.doCmdArg.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            if (!arg.data?.cmd) { throw new Error(`invalid cmd arg. arg.data.cmd required. (E: aec4dd5bd967fbf36f9c4fad22210222)`); }
            if (arg.data.cmd === RobbotCmd.ib) {
                return this.doCmdIb({ arg: arg });
            } else if (arg.data.cmd === RobbotCmd.gib) {
                return this.doCmdGib({ arg: arg });
            } else if (arg.data.cmd === RobbotCmd.ibgib) {
                return this.doCmdIbgib({ arg: arg });
            } else if (arg.data.cmd === RobbotCmd.activate) {
                return this.doCmdActivate({ arg: arg });
            } else if (arg.data.cmd === RobbotCmd.deactivate) {
                return this.doCmdDeactivate({ arg: arg });
            } else {
                throw new Error(`unknown arg.data.cmd: ${arg.data.cmd} (E: 721fa6a5166327134f9504c1caa3e422)`);
            }
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected doCmdIb({
        arg,
    }: {
        arg: IbGib_V1,
    }): Promise<TResultIbGib> {
        const lc = `${this.lc}[${this.doCmdIb.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            throw new Error(`not implemented in base class (E: 7298662a2b8f67611d16a8af0e499422)`);
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
    protected doCmdGib({
        arg,
    }: {
        arg: IbGib_V1,
    }): Promise<TResultIbGib> {
        const lc = `${this.lc}[${this.doCmdGib.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            throw new Error(`not implemented in base class (E: b6bf2c788c734051956481be7283d006)`);
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
    protected doCmdIbgib({
        arg,
    }: {
        arg: IbGib_V1,
    }): Promise<TResultIbGib> {
        const lc = `${this.lc}[${this.doCmdIbgib.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            throw new Error(`not implemented in base class (E: 4fee11f05315467abd036cd8555d27db)`);
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
    protected async doCmdActivate({
        arg,
    }: {
        arg: RobbotCmdIbGib,
    }): Promise<TResultIbGib> {
        const lc = `${this.lc}[${this.doCmdActivate.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: d453593a295d4cdbae2acb71d9f6de35)`); }
            await this.initialized;

            if (!arg) { throw new Error(`arg required (E: 9cddabc6dc96ddb37846807feb093423)`); }

            if (arg.ibGibs?.length ?? 0 > 0) {
                await this.initializeContext({
                    arg: arg as WitnessArgIbGib<IbGib_V1, TData, TRel8ns>,
                    rel8nName: ROBBOT_CONTEXT_REL8N_NAME,
                });
            }

            const result = await this.resulty({
                resultData: {
                    optsAddr: getIbGibAddr({ ibGib: arg }),
                    success: true,
                },
            })
            return result as TResultIbGib;
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
    protected async doCmdDeactivate({
        arg,
    }: {
        arg: RobbotCmdIbGib,
    }): Promise<TResultIbGib> {
        const lc = `${this.lc}[${this.doCmdDeactivate.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: d453593a295d4cdbae2acb71d9f6de35)`); }
            await this.initialized;

            await this.finalizeContext({
                arg: arg as WitnessArgIbGib<IbGib_V1, TData, TRel8ns>,
            });

            const result = await this.resulty({
                resultData: {
                    optsAddr: getIbGibAddr({ ibGib: arg }),
                    success: true,
                },
            })
            return result as TResultIbGib;
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // #endregion do cmd args

    // #region other stubbed do functions (doPic, doComment, doDefault)

    /**
     * Stubbed in base class for convenience. Doesn't have to be implemented.
     */
    protected doPic({
        ibGib,
    }: {
        ibGib: PicIbGib_V1,
    }): Promise<TResultIbGib | undefined> {
        const lc = `${this.lc}[${this.doPic.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            throw new Error(`not implemented in base class (E: 16ba889931644d42ad9e476757dd0617)`);
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * Stubbed in base class for convenience. Doesn't have to be implemented.
     */
    protected doComment({
        ibGib,
    }: {
        ibGib: CommentIbGib_V1,
    }): Promise<TResultIbGib | undefined> {
        const lc = `${this.lc}[${this.doComment.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }

            throw new Error(`not implemented in base class (E: 0486a7864729456d993a1afe246faea4)`);
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * Stubbed in base class for convenience. Doesn't have to be implemented.
     */
    protected doDefault({
        ibGib,
    }: {
        ibGib: TOptionsIbGib,
    }): Promise<TResultIbGib | undefined> {
        const lc = `${this.lc}[${this.doDefault.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            throw new Error(`not implemented in base class (E: 5038662186617aaf1f0cc698fd1f9622)`);
            // return this.doDefaultImpl({ibGib});
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // #endregion other stubbed do functions (doPic, doComment, doDefault)

    /**
     * validates against common robbot qualities.
     *
     * Override this with a call to `super.validateThis` for custom validation
     * for descending robbot classes.
     *
     * @returns validation errors common to all robbots, if any errors exist.
     */
    protected async validateThis(): Promise<string[]> {
        const lc = `${this.lc}[${this.validateThis.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            const errors = [
                // ...await super.validateThis(),
                ...validateCommonRobbotData({ robbotData: this.data }),
            ];
            return errors;
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * builds an arg ibGib.
     *
     * wrapper convenience to avoid long generic calls.
     */
    async argy<
        TCmdOptionsData extends RobbotCmdData = RobbotCmdData,
        TCmdOptionsRel8ns extends RobbotCmdRel8ns = RobbotCmdRel8ns,
        TCmdOptionsIbGib extends RobbotCmdIbGib<IbGib_V1, TCmdOptionsData, TCmdOptionsRel8ns> =
        RobbotCmdIbGib<IbGib_V1, TCmdOptionsData, TCmdOptionsRel8ns>
    >({
        argData,
        ibMetadata,
        noTimestamp,
        ibGibs,
    }: {
        argData: TCmdOptionsData,
        ibMetadata?: string,
        noTimestamp?: boolean,
        ibGibs?: IbGib_V1[],
    }): Promise<TCmdOptionsIbGib> {
        const arg = await argy_<TCmdOptionsData, TCmdOptionsRel8ns, TCmdOptionsIbGib>({
            argData,
            ibMetadata,
            noTimestamp
        });

        if (ibGibs) { arg.ibGibs = ibGibs; }

        return arg;
    }

    /**
     * builds a result ibGib, if indeed a result ibgib is required.
     *
     * This is only useful in robbots that have more structured inputs/outputs.
     * For those that simply accept any ibgib incoming and return a
     * primitive like ib^gib or whatever, then this is unnecessary.
     *
     * wrapper convenience to avoid long generic calls.
     */
    async resulty<
        TResultData extends RobbotResultData = RobbotResultData,
        TResultRel8ns extends RobbotResultRel8ns = RobbotResultRel8ns,
        TResultIbGib extends RobbotResultIbGib<IbGib_V1, TResultData, TResultRel8ns> =
        RobbotResultIbGib<IbGib_V1, TResultData, TResultRel8ns>
    >({
        resultData,
        ibGibs,
    }: {
        resultData: TResultData,
        ibGibs?: IbGib_V1[],
    }): Promise<TResultIbGib> {
        const result = await resulty_<TResultData, TResultIbGib>({
            // ibMetadata: getRobbotResultMetadata({space: this}),
            resultData,
        });
        if (ibGibs) { result.ibGibs = ibGibs; }
        return result;
    }

    protected getThisAtom(): string {
        return ROBBOT_ATOM; // "robbot" atow
    }

    protected parseAddlMetadataString<RobbotBaseParseMetadataResult>({ ib }: { ib: Ib }): RobbotBaseParseMetadataResult {
        const lc = `${this.lc}[${this.parseAddlMetadataString.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: d0cf162bc5f4cbf65bf1f7e29e3bd922)`); }
            const { safeIbCommentMetadataText } = parseCommentIb({ ib });

            if (safeIbCommentMetadataText) {
                const [_, robbotClassnameIsh, robbotNameIsh, robbotIdIsh] = safeIbCommentMetadataText.split('__');
                return { robbotClassnameIsh, robbotNameIsh, robbotIdIsh } as RobbotBaseParseMetadataResult;
            } else {
                return { robbotClassnameIsh: undefined, robbotNameIsh: undefined, robbotIdIsh: undefined } as RobbotBaseParseMetadataResult;
            }
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * by default in base class, this compares exactly the metadata string in a comment
     * with this robbot.getaddlmetadata result.
     */
    protected isMyComment({ ibGib }: { ibGib: IbGib_V1 }): boolean {
        const lc = `${this.lc}[${this.isMyComment.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 18fc0c7132e2b5297a9c00df7f79e622)`); }
            const { safeIbCommentMetadataText } = parseCommentIb({ ib: ibGib.ib });
            const isMine = safeIbCommentMetadataText === this.getAddlMetadata();
            if (logalot) { console.log(`${lc} metadata (${safeIbCommentMetadataText ?? 'undefined'}) isMine: ${isMine} (I: 1915fc7fd20e0ee8a2f8b554741b6622)`); }
            return isMine;
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * gets ibgibs to which this robbot is directly rel8d WRT rel8nNames.
     *
     * @returns map of rel8nName -> ibgibs that this robbot is related to.
     */
    protected async getRel8dIbGibs({
        rel8nNames,
    }: {
        rel8nNames?: string[],
    }): Promise<{ [rel8nName: string]: IbGib_V1[] }> {
        const lc = `${this.lc}[${this.getRel8dIbGibs.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: f183ca9d23e8f67721b614cf01327b22)`); }
            if (!rel8nNames) { rel8nNames = this.data?.allRel8nNames ?? []; }
            if (!this.data) { console.warn(`${lc} (UNEXPECTED) this.data falsy? (W: d9dc3b322da443228c35209621ae96a4)`); }
            if (this.data?.defaultRel8nName && !rel8nNames.includes(this.data.defaultRel8nName)) {
                rel8nNames.push(this.data.defaultRel8nName);
            }
            if (rel8nNames.length === 0) { throw new Error(`rel8nNames arg required or this.data.allRel8nNames must have at least one rel8nName. (E: 32380232f04f6cb01e0791a754672722)`); }

            await this.loadNewerSelfIfAvailable();

            if (!this.ibgibsSvc) { throw new Error(`this.ibgibsSvc falsy (E: fd2f9e4384a725122f4de2eb76696923)`); }
            const space = await this.ibgibsSvc.getLocalUserSpace({ lock: true });

            const rel8dIbGibs: { [rel8nName: string]: IbGib_V1[] } = {};

            // todo: adjust to use the cacheIbGibs (not get them if already have in cache, but still return them in this fn's result)
            for (let i = 0; i < rel8nNames.length; i++) {
                const rel8nName = rel8nNames[i];
                const rel8dAddrs: IbGibAddr[] = this.rel8ns ? (this.rel8ns[rel8nName] ?? []) : undefined ?? [];

                if (rel8dAddrs.length > 0) {
                    const resGet = await getFromSpace({ addrs: rel8dAddrs, space });
                    if (resGet.success && resGet.ibGibs?.length === rel8dAddrs!.length) {
                        rel8dIbGibs[rel8nName] = resGet.ibGibs.concat();
                    } else {
                        throw new Error(`there was a problem getting this robbot's rel8d ibgibs. ${resGet.errorMsg ?? 'some kinda problem...hmm..'} (E: 81e26309422ab87e5bd6bcf152059622)`);
                    }
                } else {
                    rel8dIbGibs[rel8nName] = [];
                }
            }

            return rel8dIbGibs;
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * helper function to wrap text in outputPrefix/Suffix
     *
     * @returns wrapped text, depending on param values.
     */
    protected async getOutputText({
        text,
        skipPrefix,
        skipSuffix,
        prefixOverride,
        suffixOverride,
    }: {
        text: string,
        skipPrefix?: boolean,
        skipSuffix?: boolean,
        prefixOverride?: string,
        suffixOverride?: string,
    }): Promise<string> {
        const lc = `${this.lc}[${this.getOutputText.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: 7107ae1bcbee485e96123ea1733cc191)`); }
            if (!text) {
                console.error(`${lc} text required. (E: e1f5716c2a5744b5b5ca22dee6af8a85)`);
                text = 'urmm, something went awry...the text is empty at this point.';
            }

            if (!this.data) { throw new Error(`(UNEXPECTED) this.data falsy? (E: 0f0aab6b7f258d1a386cfb22349e9923)`); }

            let resText = skipPrefix || !this.data.outputPrefix ? text : `${prefixOverride ?? this.data.outputPrefix}\n\n${text}`;
            if (!skipSuffix && this.data.outputSuffix) {
                resText += `\n\n${suffixOverride ?? this.data.outputSuffix}`;
            }

            return resText;
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * By default, this rel8s to the given ibgibs via this.data.defaultRel8nName
     */
    protected async lookAt({
        ibGibs
    }: {
        ibGibs: IbGib_V1[]
    }): Promise<void> {
        const lc = `${this.lc}[${this.lookAt.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: ea0e0ed92756ca339b8c03b700599722)`); }
            await this.rel8To({ ibGibs });
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }

    }

    protected async getRobbotInteractionData({
        type,
        commentText,
        subjectTjpGibs,
        details,
        expectingResponse,
        uuid,
        timestamp,
    }: {
        /**
         * the tyep of interaction.
         *
         * @see {@link RobbotInteractionType}
         */
        type: RobbotInteractionType,
        /**
         * the comment text of the interaction.
         *
         * Does not include any prefix/suffix of the robbot.
         */
        commentText: string,
        /**
         * If the interaction has one or more specific target/subject, then this
         * is where their gibs go.
         *
         * ## driving use case
         *
         * I need a way from starting with an ibgib and getting its related
         * interactions.
         */
        subjectTjpGibs?: Gib[],
        /**
         * interaction details.
         */
        details?: any,
        /**
         * if interaction intends to evoke a response
         */
        expectingResponse?: boolean,
        /**
         * If provided, will be the id of the interaction.
         */
        uuid?: string,
        /**
         * if provided, will be the timestamp of the interaction data.
         */
        timestamp?: string,
    }): Promise<RobbotInteractionData_V1> {
        const lc = `${this.lc}[${this.getRobbotInteractionData.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: b05837a91c6973326fde8cf51aac1922)`); }

            if (!type) { throw new Error(`type required (E: 737f8ecc21c519d1d61bce2a91fe2d22)`); }
            if (!this._currentWorkingContextIbGib?.gib) { throw new Error(`this._currentWorkingContextIbGib?.gib is falsy (E: 6489a36ab0c797f1b5d52db709ea0322)`); }

            uuid = uuid || await getUUID();
            timestamp = timestamp || getTimestamp();

            let contextTjpGib: Gib | undefined = getGibInfo({ gib: this._currentWorkingContextIbGib.gib }).tjpGib;
            if (!contextTjpGib) {
                contextTjpGib = this._currentWorkingContextIbGib.gib;
                if (!contextTjpGib) { throw new Error(`(UNEXPECTED) this._currentWorkingContextIbGib.gib falsy?  (E: 50cddb787f1a0af19567531b30bd5522)`); }
                console.warn(`${lc} contextTjpGib is falsy? This means that the robbot context does not have a timeline. This might be ok, but not expected. contextTjp will be considered the gib of the context (${contextTjpGib}) (W: f2a6f91ef5e44170a5e3d5456f2fb2f2)`);
            }

            const data: RobbotInteractionData_V1 = {
                uuid,
                timestamp,
                type,
                contextTjpGib,
                commentText,
                expectingResponse,
                subjectTjpGibs,
            };

            if (details) { data.details = details; }

            return data;
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
}

export interface IbGibRobbotAny
    extends RobbotBase_V1<any, any, any, any, any, any, any, any> {
}

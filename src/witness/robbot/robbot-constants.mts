/**
 * In a Robbot's rel8ns, this is the rel8n name that links to tags that the
 * Robbot uses for tagging its output.
 */
export const ROBBOT_TAG_TJP_ADDRS_REL8N_NAME = 'tagTjpAddrs';
/**
 * A spaces ibGib uses this rel8n name for related sync spaces, used
 * in replicating ibgib spaces.
 */
export const ROBBOT_REL8N_NAME = 'robbot';
/**
 * When a robbot witnesses an ibgib, it will "remember" the ibgib
 * by relating the target ibgib to itself via this rel8nName.
 */
export const DEFAULT_ROBBOT_TARGET_REL8N_NAME = 'x';

/**
 * wah wah wah...
 */
export const IBGIB_ROBBOT_NAME_DEFAULT = 'i';

/**
 * Icon for a single robbot
 */
export const DEFAULT_ROBBOT_ICON = 'body-outline';

/**
 * @module space-respec-helper contains respec factory functions that are
 * designed to help ensure common functionality across all spaces.
 *
 * Individual space respecs should call these in their own respec files.
 *
 * In order to get them to short-circuit (only run/report these respecs), the
 * calling space respec file should enclose them in a respecfullyDear block.
 *
 * ## note on maam, sir, etc. respec titles
 *
 * since this file is designed for the caller to have context, the titles (maam,
 * sir, etc.) used in calling the respec functions are delegated to the caller.
 */

import {
    firstOfEach, firstOfAll, ifWe,
    lastOfEach, lastOfAll,
    ifWeMight, iReckon, respecfully
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
// this respec helper file passes this responsibility off to the caller
// const maam = `[${import.meta.url}]`, sir = maam;

import { delay, extractErrorMsg, getTimestampInTicks, pickRandom_Letters, pretty } from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { IbGibAddr, } from "@ibgib/ts-gib/dist/types.mjs";
import { getIbGibAddr } from "@ibgib/ts-gib/dist/helper.mjs";
import { IbGib_V1, } from "@ibgib/ts-gib/dist/V1/types.mjs";
import { Factory_V1 as factory } from '@ibgib/ts-gib/dist/V1/factory.mjs';

import {
    deleteFromSpace, getFromSpace, getLatestAddrs, parseSpaceIb,
    persistTransformResult, putInSpace, registerNewIbGib
} from "./space-helper.mjs";
import { IbGibSpaceAny } from "./space-base-v1.mjs";
import { GetIbGibResult, IbGibTimelineUpdateInfo } from '../../common/other/other-types.mjs';
import { Subject_V1_Factory } from '../../common/pubsub/subject/subject-v1.mjs';
import { newupSubject } from '../../common/pubsub/subject/subject-helper.mjs';
import { fnObs } from '../../common/pubsub/observer/observer-helper.mjs';
import { getGib } from '@ibgib/ts-gib/dist/V1/transforms/transform-helper.mjs';
import { mut8 } from '@ibgib/ts-gib/dist/V1/transforms/mut8.mjs';

export interface CommonSpaceSpecOptions {
    /**
     * sir, maam, whatever the title is
     */
    respecfulTitle: string,
    /** @optional additional description label for testing */
    desc?: string,
    /**
     * expression to get the initial data when constructing the space
     */
    fnGetInitialSpaceData: () => Promise<any | undefined>,
    /**
     * expression to get the initial rel8ns when constructing the space
     */
    fnGetInitialSpaceRel8ns: () => Promise<any | undefined>,
    /**
     * expression to get the space, used in a `beforeEach` call
     * @param initialData passed into ctor of space
     * @param initialRel8ns passed into ctor of space
     * @returns space instance
     */
    fnGetSpace: (initialData: any, initialRel8ns: any) => Promise<IbGibSpaceAny>,
    // /**
    //  * expression to get the initial data when constructing the space
    //  */
    // fnGetInitialData_zerospace?: () => Promise<any | undefined>,
    // /**
    //  * expression to get the initial rel8ns when constructing the space
    //  */
    // fnGetInitialRel8ns_zerospace?: () => Promise<any | undefined>,
    // /**
    //  * expression to get the zerospace, used in a `beforeEach` call
    //  * @param initialData passed into ctor of space
    //  * @param initialRel8ns passed into ctor of space
    //  * @returns space instance
    //  */
    // fnGetSpace_zerospace?: (initialData: any, initialRel8ns: any) => Promise<IbGibSpaceAny>,
    /**
     * if true, will use `fit` to short-circuit testing.
     */
    shortcircuitRespec?: boolean,
    fnBeforeEach?: () => Promise<void>,
    fnAfterEach?: () => Promise<void>,
    /**
     * @optional flag to use verbose logging at this level.
     */
    logalot?: boolean;
}

/**
 * Executes tests related to:
 *
 * space creation
 * `await space.initialized`
 * space ib related (`getSpaceIb` & `parseSpaceIb`)
 *
 * run tests on an ibgib space that are common to all/most spaces.
 *
 * So, we can run these against any space, inner, outer, local ionic/filesystem based, etc.
 *
 * NOTE: Be sure to await these tests.
 */
export async function testSpace_createAndInit({
    respecfulTitle: sir,
    desc,
    fnGetInitialSpaceData,
    fnGetInitialSpaceRel8ns,
    fnGetSpace,
    shortcircuitRespec,
    fnBeforeEach, fnAfterEach,
}: CommonSpaceSpecOptions): Promise<void> {
    const logContext = `[${testSpace_putGetDelete.name}]${desc ?? ''}`;

    await respecfully(sir, logContext, async () => {

        let initialData: any | undefined;
        let initialRel8ns: any | undefined;
        let space: IbGibSpaceAny;

        firstOfEach(sir, async () => {
            if (fnBeforeEach) { await fnBeforeEach(); }
            initialData = await fnGetInitialSpaceData();
            initialRel8ns = await fnGetInitialSpaceRel8ns();
        });

        if (fnAfterEach) {
            lastOfEach(sir, async () => {
                await fnAfterEach();
            });
        }

        const fnIfWeBlock: any = shortcircuitRespec ? ifWeMight : ifWe;

        await fnIfWeBlock(sir, `when created and initialized, space should be initialized`, async () => {
            space = await fnGetSpace(initialData, initialRel8ns);
            await space.initialized;

            iReckon(sir, space).asTo('space').isGonnaBeTruthy();
            iReckon(sir, space.ib).asTo('ib').isGonnaBeTruthy();
            iReckon(sir, space.gib).asTo('gib').isGonnaBeTruthy();
            if (initialData) {
                iReckon(sir, space.data).asTo('data, initialData truthy').isGonnaBeTruthy();
            } else {
                iReckon(sir, space.rel8ns).asTo('data, initialData falsy').isGonnaBeUndefined();
            }
            if (initialRel8ns) {
                iReckon(sir, space.rel8ns).asTo('rel8ns, initialRel8ns truthy').isGonnaBeTruthy();
            } else {
                iReckon(sir, space.rel8ns).asTo('rel8ns, initialRel8ns falsy').isGonnaBeUndefined();
            }
        });

        await fnIfWeBlock(sir, `space.ib should contain correct info per initialData`, async () => {
            space = await fnGetSpace(initialData, initialRel8ns);
            await space.initialized;

            iReckon(sir, space.ib).includes(initialData.name);
            iReckon(sir, space.ib).includes(initialData.uuid);

            let { spaceClassname, spaceId, spaceName, } = parseSpaceIb({ spaceIb: space.ib });
            iReckon(sir, space.data?.classname).asTo('space.data?.classname').isGonnaBe(spaceClassname);
            iReckon(sir, space.data?.name).asTo('space.data?.name, initial').isGonnaBe(initialData.name);
            iReckon(sir, space.data?.name).asTo('space.data?.name, spaceName').isGonnaBe(spaceName);
            iReckon(sir, space.data?.uuid).asTo('space.data?.uuid, initial').isGonnaBe(initialData.uuid);
            iReckon(sir, space.data?.uuid).asTo('space.data?.uuid, spaceId').isGonnaBe(spaceId);
        });

    });
}

/**
 * Executes tests related to:
 *
 * `putInSpace`
 * `getFromSpace`
 * `deleteFromSpace`
 *
 * run tests on an ibgib space that are common to all/most spaces.
 *
 * So, we can run these against any space, inner, outer, local ionic/filesystem based, etc.
 *
 * NOTE: Be sure to await these tests.
 */
export async function testSpace_putGetDelete({
    respecfulTitle: sir,
    logalot,
    desc,
    fnGetInitialSpaceData,
    fnGetInitialSpaceRel8ns,
    fnGetSpace,
    shortcircuitRespec,
    fnBeforeEach, fnAfterEach,
}: CommonSpaceSpecOptions): Promise<void> {
    const lc = `[${testSpace_putGetDelete.name}]${desc ?? ''}`;
    if (logalot) { console.log(`${lc} starting... (I: a2fca2a7da38acaba31da30cae355c23)`); }

    await respecfully(sir, lc, async () => {
        let initialData: any | undefined;
        let initialRel8ns: any | undefined;
        let space: IbGibSpaceAny;

        firstOfEach(sir, async () => {
            if (fnBeforeEach) { await fnBeforeEach(); }
            initialData = await fnGetInitialSpaceData();
            initialRel8ns = await fnGetInitialSpaceRel8ns();
            space = await fnGetSpace(initialData, initialRel8ns);
            await space.initialized;
        });

        if (fnAfterEach) {
            lastOfEach(sir, async () => {
                await fnAfterEach();
            });
        }

        const fnIfWeBlock: any = shortcircuitRespec ? ifWeMight : ifWe;

        let lcWe = `${lc}, should put/get/delete`;
        await fnIfWeBlock(sir, lcWe, async () => {
            lcWe = `[${space.data!.uuid.substring(0, 8)}]${lcWe}`;
            if (logalot) { console.log(`${lcWe} starting... (I: ab1fa44c706869fcee27f0eb53896f23)`); }

            let newIbGib: IbGib_V1 = {
                ib: 'test ib',
                data: { x: 1 },
                rel8ns: { ancestor: ['test ib^gib'] }
            }
            newIbGib.gib = await getGib({ ibGib: newIbGib, hasTjp: false });
            const newIbGibAddr: IbGibAddr = getIbGibAddr({ ibGib: newIbGib });

            // put
            if (logalot) { console.log(`${lcWe} putting... (I: ae09ddb0f348218113840d6baf193823)`); }
            try {
                let resPut = await putInSpace({ ibGib: newIbGib, space });
                iReckon(sir, resPut).asTo('resPut').isGonnaBeTruthy();
                iReckon(sir, resPut.success).isGonnaBeTrue();
            } catch (error) {
                throw new Error(`error putInSpace. error: ${extractErrorMsg(error)} (E: 2303126b96f34604a81cc23b9ad05f72)`);
            }
            if (logalot) { console.log(`${lcWe} put complete. (I: ef76b7444e404f39b1aea0053dd6c722)`); }

            // get
            if (logalot) { console.log(`${lcWe} getting... (I: 1fccaa28da404a678a1a8e0a82e7bfac)`); }
            try {
                let resGet = await getFromSpace({ addr: newIbGibAddr, space });
                iReckon(sir, resGet).asTo('resGet').isGonnaBeTruthy();
                iReckon(sir, resGet.success).asTo('resGet').isGonnaBeTrue();
                iReckon(sir, resGet.errorMsg).asTo('resGet').isGonnaBeFalsy();
                iReckon(sir, resGet.ibGibs).asTo('resGet').isGonnaBeTruthy();
                iReckon(sir, resGet.ibGibs!.length).asTo('resGet').isGonnaBe(1);
                iReckon(sir, resGet.ibGibs![0]).asTo('resGet').isGonnaBe(newIbGib);
            } catch (error) {
                throw new Error(`error getFromSpace. error: ${extractErrorMsg(error)} (E: b3972188c7084d7a9babb13fabd95490)`);
            }
            if (logalot) { console.log(`${lcWe} getting complete. (I: abe6caf301da4f5fb588d2b64ace6ecc)`); }

            // delete
            if (logalot) { console.log(`${lcWe} deleting... (I: 17ec087114a240b794bc45ccbe391aa4)`); }
            try {
                await deleteFromSpace({ addr: newIbGibAddr, space });
                const resGet = await getFromSpace({ addr: newIbGibAddr, space });
                iReckon(sir, resGet).asTo('resGet after delete').isGonnaBeTruthy();
                iReckon(sir, resGet.success).asTo('resGet after delete').isGonnaBeFalse();
                iReckon(sir, resGet.errorMsg).asTo('resGet after delete').isGonnaBeTruthy();
                iReckon(sir, resGet.ibGibs).asTo('resGet after delete').isGonnaBeUndefined();
            } catch (error) {
                throw new Error(`error delete>getFromSpace. error: ${extractErrorMsg(error)} (E: 891202eb42b745da87a6d072ac63d47e)`);
            }
            if (logalot) { console.log(`${lcWe} deleting complete. (I: b49650321343471fa27d8fdcee7d3c34)`); }

            if (logalot) { console.log(`${lcWe} complete. (I: 0fd6611c9047ffd6e1673983bd776723)`); }
        });

        const testLengths = [255, 256, 300, 500, 1000];
        for (let i = 0; i < testLengths.length; i++) {
            const testLength = testLengths[i];
            lcWe = `${lc}, should put/get/delete LONG addr (${testLength} length)`;
            await fnIfWeBlock(sir, lcWe, async () => {
                lcWe = `[${space.data!.uuid.substring(0, 8)}]${lcWe}`;
                if (logalot) { console.log(`${lcWe} starting... (I: 467f6befe68eff762fd51823c74b8923)`); }
                let newIbGib: IbGib_V1 = {
                    ib: pickRandom_Letters({ count: testLength }),
                    data: { x: 1 },
                    rel8ns: { ancestor: ['test yo^gib'] }
                }
                if (logalot) { console.log(`${lc} newIbGib.ib: ${newIbGib.ib} (I: c41d11fc9b0177c8369efa3f71108523)`); }
                newIbGib.gib = await getGib({ ibGib: newIbGib, hasTjp: false });
                const newIbGibAddr: IbGibAddr = getIbGibAddr({ ibGib: newIbGib });

                // put
                try {
                    const resPut = await putInSpace({ ibGib: newIbGib, space });
                    iReckon(sir, resPut).asTo(`resPut[l:${testLength}]`).isGonnaBeTruthy();
                    iReckon(sir, resPut.success).isGonnaBeTrue();
                } catch (error) {
                    throw new Error(`error putInSpace (long:${testLength}). error: ${extractErrorMsg(error)} (E: e725da9141fd4649a4214b0a0f8e0f44)`);
                }

                // get
                try {
                    const resGet = await getFromSpace({
                        addr: newIbGibAddr, space,
                        force: true, // don't allow cachine
                    });
                    iReckon(sir, resGet).asTo(`resGet[l:${testLength}]`).isGonnaBeTruthy();
                    iReckon(sir, resGet.success).asTo(`resGet[l:${testLength}]`).isGonnaBeTrue();
                    iReckon(sir, resGet.errorMsg).asTo(`resGet[l:${testLength}]`).isGonnaBeFalsy();
                    iReckon(sir, resGet.ibGibs).asTo(`resGet[l:${testLength}]`).isGonnaBeTruthy();
                    iReckon(sir, resGet.ibGibs!.length).asTo(`resGet[l:${testLength}]`).isGonnaBe(1);
                    iReckon(sir, resGet.ibGibs![0]).asTo(`resGet[l:${testLength}]`).isGonnaBe(newIbGib);
                } catch (error) {
                    throw new Error(`error getFromSpace (long:${testLength}). error: ${extractErrorMsg(error)} (E: 7ab8aa7e4176483586309f48ce12811a)`);
                }

                // delete
                try {
                    await deleteFromSpace({ addr: newIbGibAddr, space });
                    const resGet = await getFromSpace({ addr: newIbGibAddr, space });
                    iReckon(sir, resGet).asTo(`resGet [l:${testLength}]after delete`).isGonnaBeTruthy();
                    iReckon(sir, resGet.success).asTo(`resGet [l:${testLength}]after delete`).isGonnaBeFalse();
                    iReckon(sir, resGet.errorMsg).asTo(`resGet [l:${testLength}]after delete`).isGonnaBeTruthy();
                    iReckon(sir, resGet.ibGibs).asTo(`resGet [l:${testLength}]after delete`).isGonnaBeUndefined();
                } catch (error) {
                    throw new Error(`error delete>getFromSpace (long:${testLength}). error: ${extractErrorMsg(error)} (E: 43aa6ff9369ef08d8e90fa887a358823)`);
                }

                if (logalot) { console.log(`${lcWe} complete. (I: e4b7977a0ff101f2bb66565b46a6bc23)`); }
            });

        }
    });

    if (logalot) { console.log(`${lc} complete. (I: 517f638a95e8822a8880b376babe1f23)`); }
}

/**
 * Executes tests related to:
 *
 * `persistTransformResult`
 *
 * run tests on an ibgib space that are common to all/most spaces.
 *
 * So, we can run these against any space, inner, outer, local ionic/filesystem based, etc.
 *
 * NOTE: Be sure to await these tests.
 */
export async function testSpace_persistTransformResult({
    respecfulTitle: sir,
    logalot,
    desc,
    fnGetInitialSpaceData,
    fnGetInitialSpaceRel8ns,
    fnGetSpace,
    shortcircuitRespec,
}: CommonSpaceSpecOptions): Promise<void> {
    let lc = `[${testSpace_persistTransformResult.name}]${desc ?? ''}`;
    if (logalot) { console.log(`${lc} starting... (I: 18fc064b8c4b730a5f269f7dcd6d8a23)`); }

    await respecfully(sir, lc, async () => {

        let initialData: any | undefined;
        let initialRel8ns: any | undefined;
        let space: IbGibSpaceAny;

        firstOfEach(sir, async () => {
            initialData = await fnGetInitialSpaceData();
            initialRel8ns = await fnGetInitialSpaceRel8ns();
            space = await fnGetSpace(initialData, initialRel8ns);
            await space.initialized;
        });

        const fnIfWeBlock: any = shortcircuitRespec ? ifWeMight : ifWe;

        await fnIfWeBlock(sir, `${lc} should persist all ibgibs (new, dna, intermediate)`, async () => {
            let lcSpace = `[${space.data?.uuid.substring(0, 8)}]`;
            try {

                let resNew = await factory.firstGen({
                    ib: 'test ib',
                    parentIbGib: factory.primitive({ ib: 'test ib' }),
                    dna: true,
                    noTimestamp: true,
                });
                let { newIbGib } = resNew;

                await persistTransformResult({ resTransform: resNew, space });

                const allIbGibs = [
                    newIbGib,
                    ...(resNew.dnas ?? []),
                    ...(resNew.intermediateIbGibs ?? []),
                ];
                const allAddrs = allIbGibs.map(x => getIbGibAddr({ ibGib: x }));

                await delay(25);

                let resGet = await getFromSpace({ addrs: allAddrs, space });

                iReckon(sir, resGet).asTo(`${lcSpace} resGet`).isGonnaBeTruthy();
                iReckon(sir, resGet.success).asTo(`${lcSpace} resGet.success`).isGonnaBeTrue();
                iReckon(sir, resGet.errorMsg).asTo(`${lcSpace} resGet.errorMsg`)
                    .isGonnaBeFalsy({ addedMsg: extractErrorMsg(resGet.errorMsg) });
                iReckon(sir, resGet.ibGibs).asTo(`${lcSpace} resGet.ibGibs`).isGonnaBeTruthy();
                iReckon(sir, resGet.ibGibs!.length).asTo(`${lcSpace} resGet.ibGibs.length`).isGonnaBe(allAddrs.length);
                iReckon(sir, resGet.ibGibs![0]).asTo(`${lcSpace} resGet.ibGibs[0]`).isGonnaBe(newIbGib);
            } catch (error) {
                iReckon(sir, error).asTo(`${lcSpace} error thrown`).isGonnaBeFalsy({ addedMsg: extractErrorMsg(error) });
            }
        });

    });

    if (logalot) { console.log(`${lc} complete. (I: b12ab731e4daef514d34cebea08b8c23)`); }
}

/**
 * Executes tests related to:
 *
 * `persistTransformResult`
 * `registerNewIbGib`
 * `getLatestAddrs`
 *
 * run tests on an ibgib space that are common to all/most spaces.
 *
 * So, we can run these against any space, inner, outer, local ionic/filesystem based, etc.
 *
 * NOTE: Be sure to await these tests.
 */
export async function testSpace_registerNewIbGib_GetLatest({
    respecfulTitle: sir,
    desc,
    fnGetInitialSpaceData,
    fnGetInitialSpaceRel8ns,
    fnGetSpace,
    shortcircuitRespec,
    logalot,
    testLatestIterations = 5,
}: CommonSpaceSpecOptions & {
    /**
     * number of iterations to ibGib > mut8 > registerNewIbGib.
     * After each `registerNewIbGib` call, will check the broadcast count, as well
     * as a getLatestAddr check.
     */
    testLatestIterations: number,
}): Promise<void> {
    const lc = `[${testSpace_registerNewIbGib_GetLatest.name}]${desc ?? ''}`;
    if (logalot) { console.log(`${lc} starting... (I: 9129bfda91b9e013127a154618357b23)`); }

    await respecfully(sir, lc, async () => {

        let initialData: any | undefined;
        let initialRel8ns: any | undefined;
        let space: IbGibSpaceAny;

        firstOfEach(sir, async () => {
            initialData = await fnGetInitialSpaceData();
            initialRel8ns = await fnGetInitialSpaceRel8ns();
            space = await fnGetSpace(initialData, initialRel8ns);
            await space.initialized;
        });

        const fnIfWeBlock: any = shortcircuitRespec ? ifWeMight : ifWe;

        await fnIfWeBlock(sir, `${lc} create/persist/registerNewIbgib(s) starting...`, async () => {

            if (logalot) { console.log(`${lc} ${lc} create/persist/registerNewIbgib(s)... (I: 9a0089290bff4d699902bb0ce8e41e11)`); }
            /**
             * this array will be added to every time a broadcast even happens.
             */
            const broadcasted: IbGibTimelineUpdateInfo[] = [];
            /**
             * we pass this lambda in to the registerNewIbGib call
             * @param info
             */
            let fnBroadcast: (info: IbGibTimelineUpdateInfo) => void = (info) => {
                if (logalot) { console.log(`${lc} broadcast received (I: fdf4a3c561ae4bba930f1fd6f4324716)`) }
                broadcasted.push(info);
            }

            // create our firstGen tjp
            let resNew = await factory.firstGen({
                ib: 'test ib',
                parentIbGib: factory.primitive({ ib: 'test ib' }),
                dna: true,
                tjp: { timestamp: true, uuid: true }
            });
            let { newIbGib: newIbGib_tjp } = resNew;
            if (logalot) {
                console.log(`${lc} newIbGib.dir...`);
                console.dir(newIbGib_tjp);
            }

            // have to put it in the space before broadcasting
            await persistTransformResult({ resTransform: resNew, space });

            // adds metadata to track this ibgib as the latest & trigger a
            // broadcast if indeed it is the latest (idempotent)
            if (logalot) { console.log(`${lc} registering newIbGib_tjp (I: 7dcf5576ab07eb49395f6ed370cd7e23)`); }
            await registerNewIbGib({
                ibGib: newIbGib_tjp,
                space,
                fnBroadcast,
            });

            // broadcast is spun off with setTimeout (& not awaited)
            // i could conceivably set up an observable that waits for this,
            // and so ensures that it is done, but for now, we'll hack it with a delay
            await delay(20);
            iReckon(sir, broadcasted.length).asTo('broadcasted.length 1').isGonnaBe(1);

            if (logalot) { console.log(`${lc} registerNewIbGib complete. testing getLatestAddrs for tjp... (I: d762456b1bbc99edd7731218f4c30823)`); }

            // if we now getLatestAddrs(tjp) then we should get back the tjp addr itself
            const tjpAddr = getIbGibAddr({ ibGib: newIbGib_tjp });
            const resGetLatest_tjp = await getLatestAddrs({ ibGibs: [newIbGib_tjp], space });
            if (!resGetLatest_tjp?.data?.success) { throw new Error(`resGetLatest_tjp success falsy (E: 07b82dd59128a1b844fa306d29484423)`); }
            if (!resGetLatest_tjp.data.latestAddrsMap) { throw new Error(`resGetLatest_tjp latestAddrsMap falsy (E: 1655bd75e3fb7584b2b5063194345923)`); }
            if (logalot) {
                console.log(`${lc} console.dir(resGetLatest_tjp.data.latestAddrsMap)...(I: a75b4dd4eb048a59e1af8c688709f423)`);
                console.dir(resGetLatest_tjp.data.latestAddrsMap);
            }
            let latestAddr_tjp = resGetLatest_tjp.data.latestAddrsMap[tjpAddr];
            iReckon(sir, latestAddr_tjp).isGonnaBe(tjpAddr)
            if (logalot) { console.log(`${lc} latestAddr_tjp: ${latestAddr_tjp} (I: 5a6234dc01d5c1bc46e9a78f8030e423)`); }

            /**
             * each new ibgib will be added to this list.
             */
            const allIbGibs = [newIbGib_tjp];
            /**
             * used in mut8 call. this will be reassigned each iteration
             */
            let src = newIbGib_tjp;

            if (logalot) { console.log(`${lc} starting timeline extension and testing of getLatestAddrs... (I: 8aaedfb2e8cb353ae9bd37016a4ab923)`); }
            // we now can do multiple mut8s and rel8s and each registerNewIbGib should trigger a broadcast.
            for (let i = 0; i < testLatestIterations; i++) {
                // console.time(lc + i);
                if (logalot) { console.log(`${lc} i: ${i} (I: ea8f9e9de9ce5eaab8129d74cca32f23)`); }
                const resMut8 = await mut8({
                    src,
                    dataToAddOrPatch: { i: i, wakka: `doodle ${getTimestampInTicks()}` },
                    dna: true,
                    nCounter: true,
                });
                await persistTransformResult({ resTransform: resMut8, space });
                let { newIbGib: newIbGib_n_i } = resMut8;
                allIbGibs.push(newIbGib_n_i);
                if (logalot) { console.log(`${lc} newIbGib_n_${i} persisted. registering as new now... (I: 729eefc6509a019037147ff491b86223)`); }
                iReckon(sir, newIbGib_n_i.data?.n).asTo(`(i: ${i}) newIbGib_n_i.data?.n`).isGonnaBe(i);

                // before we register, we only have the tjp + previous (i) registered ibGibs
                iReckon(sir, broadcasted.length).asTo(`(i: ${i}) broadcasted.length before registerNewIbGib`).isGonnaBe(i + 1);

                // this should trigger a broadcast
                await registerNewIbGib({
                    ibGib: newIbGib_n_i,
                    space,
                    fnBroadcast,
                });

                // broadcast is spun off with setTimeout (& not awaited)
                // i could conceivably set up an observable that waits for this,
                // and so ensures that it is done, but for now, we'll hack it with a delay
                await delay(20);
                iReckon(sir, broadcasted.length).asTo(`(i: ${i}) broadcasted.length after registerNewIbGib`).isGonnaBe(i + 2);

                // first just see if it throws
                if (logalot) { console.log(`${lc} registerNewIbGib complete. getLatestAddrs... (I: 1c73a64f556d53032f7da41edb546523)`); }
                // console.time(lc + i + 'latest');
                let resGetLatestAddrs = await getLatestAddrs({ ibGibs: allIbGibs, space });
                // console.timeEnd(lc + i + 'latest');
                if (logalot) {
                    console.log(`${lc} getLatestAddrs complete. console.dir(resGetLatestAddrs)... (I: 8c4101f2f8f2f379568597457115ab23)`);
                    console.dir(resGetLatestAddrs);
                }

                const { latestAddrsMap } = resGetLatestAddrs.data!;
                iReckon(sir, latestAddrsMap).asTo('latestAddrsMap').isGonnaBeTruthy();
                if (!latestAddrsMap) { throw new Error(`(UNEXPECTED) latestAddrsMap falsy? (E: 5f848b7ebeaec59d01d7c898c880cb23)`); }

                const latestAddrsValues = Object.values(latestAddrsMap);

                const newIbGibAddr_n_i = getIbGibAddr({ ibGib: newIbGib_n_i });
                // all latest addrs should point to the newIbGib_n_i
                if (logalot) { console.log(`${lc} (all should be ${newIbGibAddr_n_i}) latestAddrsValues: ${pretty(latestAddrsValues)} (I: dc3eedc40a46f566d2a45ac17356c523)`); }
                const allPointToLatest =
                    latestAddrsValues.every(x => x === newIbGibAddr_n_i);
                iReckon(sir, allPointToLatest).asTo('allPointToLatest').isGonnaBeTrue();

                // prepare for next iteration
                src = newIbGib_n_i;
                // console.timeEnd(lc + i);
            }

            if (logalot) { console.log(`${lc} ${lc} create/persist/registerNewIbgib(s) complete. (I: 37b5932f1c69864a0fdc46b521608723)`); }
        });

    });

    if (logalot) { console.log(`${lc} complete. (I: 3d363d8b63527849d17733846156dc23)`); }
}

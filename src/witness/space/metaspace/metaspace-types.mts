/**
 * Gigantic file with a bunch of shift in it. Needs to be refactored when we
 * have a clearer picture and the prototype is up and limping along...
 */

// import { Observable, } from 'rxjs';


import { IbGib_V1, } from '@ibgib/ts-gib/dist/V1/index.mjs';
import { IbGibAddr, TransformResult, } from '@ibgib/ts-gib/dist/types.mjs';

import {
    DeleteIbGibOpts, DeleteIbGibResult,
    GetIbGibOpts, GetIbGibResult,
    PutIbGibOpts, PutIbGibResult
} from '../../../common/other/other-types.mjs';
import { SyncSagaInfo, } from '../../../witness/space/outer-space/outer-space-types.mjs';
import { SpaceId } from '../../../witness/space/space-types.mjs';
import { IbGibSpaceAny } from '../../../witness/space/space-base-v1.mjs';
import { IbGibTimelineUpdateInfo, SpecialIbGibType } from '../../../common/other/other-types.mjs';
import { RootData } from '../../../common/root/root-types.mjs';
import {
    CiphertextData, CiphertextIbGib_V1, CiphertextRel8ns,
    EncryptionData_V1, EncryptionIbGib, EncryptionInfo_EncryptGib,
    SecretData_V1, SecretIbGib_V1, SecretInfo_Password
} from '../../../common/encrypt/encrypt-types.mjs';
import { RobbotIbGib_V1, RobbotPromptResult } from '../../../witness/robbot/robbot-types.mjs';
import { AppIbGib_V1, AppPromptResult } from '../../../witness/app/app-types.mjs';
import { rel8ToSpecialIbGib, } from '../../../witness/space/space-helper.mjs';
import { GetDependencyGraphOptions } from '../../../common/other/graph-helper.mjs';
import { ObservableWitness, ObservableWitnessAny } from '../../../common/pubsub/observable/observable-types.mjs';

export interface CreateLocalSpaceOptions {
    allowCancel: boolean;
    spaceName: string | undefined;
    getFnPrompt: () => ((arg: { title: string, msg: string }) => Promise<string>);
    logalot?: number | boolean;
}

export type DtoToSpaceFunction = (spaceDto: IbGib_V1) => Promise<IbGibSpaceAny>;
export type ZeroSpaceFactoryFunction = () => IbGibSpaceAny;
export type LocalSpaceFactoryFunction = (opts: CreateLocalSpaceOptions) => Promise<IbGibSpaceAny | undefined>;

export interface MetaspaceInitializeOptions {
    /**
     * If provided, will initialize using this spaceName if creating a new local
     * space. if a local space already exists, i.e., if a bootstrap already points
     * to an existing local space, then this is ignored.
     */
    spaceName?: string;
    metaspaceFactory?: MetaspaceFactory;
    getFnAlert: () => ((arg: { title: string, msg: string, }) => Promise<void>);
    getFnPrompt: () => ((arg: { title: string, msg: string }) => Promise<string>);
    getFnPromptPassword: () => ((title: string, msg: string) => Promise<string | null>);
}

/**
 * space-related factory functions
 */
export interface MetaspaceFactory {
    /**
     * if this is truthy, then this will be used when the service.zerospace
     * getter property is called.
     *
     * # intent
     *
     * I'm adding this for the ability to have an in-memory only zero space and
     * other spaces implementation.
     *
     * @returns factory function for generating zero space
     */
    fnZeroSpaceFactory?: ZeroSpaceFactoryFunction;
    /**
     * If truthy, this will be used when creating local spaces by default.

     * # intent
     *
     * I'm adding this for the ability to have an in-memory only zero space and
     * other spaces implementation.
     *
     * @returns async factory function for generating local spaces
     */
    fnDefaultLocalSpaceFactory?: LocalSpaceFactoryFunction;
    fnDtoToSpace?: DtoToSpaceFunction;
}

export interface TempCacheEntry {
    /**
     * Password to the user's password.
     */
    tempMetaPassword: string;
    /**
     * Encrypted user password
     */
    encryptedPassword: string;
    /**
     * salt used in cached encryption.
     */
    salt: string;
}

/**
 * Primary "All Powerful" service (too big) that does a lot of work for a local
 * metaspace (like a node).
 *
 * This service provides conveniences for working with one or more local spaces,
 * using one space as a default space.
 *
 * For example, the metaspace maintains data about timelines via the latest^gib.
 * This provides the local source of truth for the "head" of each timeline,
 * similar to an index. This latest^gib ibgib is one example of a "special
 * ibgib". Usually these are indexes to other ibgibs.
 *
 * ## regarding special ibgibs
 *
 * Many special ibgibs are analogs to other apps' configuration data. The
 * difference is that most special ibgibs can leverage the "on-chain"
 * functionality of "regular" ibgibs.
 *
 * Here are a couple of special ibgibs atow:
 * * roots^gib
 *   * indexes root^gib ibgibs. Any ibgib in a local space will usually have
 *     its temporal junction point (the very first unique ibgib in a timeline,
 *     like a birthday) in a root^gib.
 * * latest^gib
 *   * tracks mappings between tjp -> latest ib^gib address
 *   * ephemeral (deletes past rel8ns and past ibGib frames)
 * * tags^gib
 *   * index of tag^gib ibgibs, which function like tags or bookmarks in the
 *     MVP.
 * * apps^gib
 *   * indexes app ibgibs
 *   * these hold data like app config but
 *   * also relate to witness ibgibs provide UXs for human users to interact
 *     within a context ibgib.
 * * robbots^gib
 *   * indexes robbot ibgibs, which can interact within context ibgibs.
 * * secrets^gib
 *   * indexes secret^gib that are used with creating private ibgib data.
 *   * does not contain actual secrets, but rather provides metadata to prompt
 *     the user for some secret, like hints/context to a password.
 * * encryptions^gib
 *   * encryption settings
 * * outerspaces^gib
 *   * other spaces to connect with, e.g. cloud sync space
 * * autosyncs^gib
 *   * metadata of ibgibs that get automatically synced with other space(s)
 *
 * ## regarding latest ibgibs
 *
 * The tjp (temporal junction point) defines atow the beginning of an ibGib
 * timeline.  it's like the birthday for an ibGib. (Or you can think if it as
 * the id for the stream of ibgib frames in a given timeline, but the "id" is
 * itself an ib^gib content address that includes the hash.)
 *
 * The latest ibGib in that timeline is also special, because it's often what
 * you want to work with.
 *
 * So ideally, when an ibgib, A, has a tjp A1, and it is updated to A2, A3, An
 * via `mut8` and/or `rel8` transforms, that ibgib creates a single timeline.
 * This service attempts to track the relationship between that starting tjp
 * address and its corresponding latest frame in that timeline, i.e., A1 -> An.
 *
 * ### mapping persistence implementation details
 *
 * The latest ibGib service is backed by a special ibgib that maintains the
 * mapping index.  It does this by rel8-ing that special backing ibgib via the
 * tjp pointer, e.g. [special latest ibgib^XXX000].rel8ns[A^TJP123] ===
 * [A^N12345] . It does this via the ib^gib content address pointer, so this
 * becomes a mapping from A^TJP123 to A^N12345.
 *
 * This backing ibGib is special (even for special ibGibs) in that:
 *   * it does not relate itself with the current root of the application
 *   * it does not maintain references to its past (i.e. rel8ns['past'] === [])
 *   * it DELETES its previous incarnation from the files service
 *
 * In other words, this service is meant to be as ephemeral as possible. I am
 * keeping it as an ibGib and not some other data format (like straight in
 * storage/some other db) because I've found this is often useful and what I end
 * up doing anyway to leverage other ibgib behavior. For example, in the future
 * it may be good to take snapshots, which is a simple copy operation of the
 * file persistence.
 *
 * ### current naive implementation notes
 *
 * questions:
 *   * What do we want to do if we can't locate an ibGib record?
 *   * How/when do we want to alert the user/our own code that we've found
 *     multiple timelines for an ibGib with a tjp (usually a thing we want to
 *     avoid)?
 *   * Who do we want to notify when new ibGibs arrive?
 *   * How often do we want to check external sources for latest?
 *   * When do we get to merging ibGib timelines?
 *
 * This is behavior that is somewhat taken care of, e.g. in git, with the HEAD
 * pointer for a repo.  But we're talking about here basically as a metarepo or
 * "repo of repos", and unlike git, we don't want our HEAD metadata living "off
 * chain" (outside of the DLT itself that it's modifying).  So eventually, what
 * we want is just like what we want with ALL ibGibs: perspective. From "the
 * app"'s perspective, the latest is mapped. But really, apps can't view slices
 * of ibGib graphs in all sorts of interesting ways and still be productive &
 * beneficial to the ecosystem as a whole.
 */
export interface MetaspaceService {
    instanceId: string;
    get initialized(): boolean;
    get initializing(): boolean;
    get initialized$(): ObservableWitnessAny | undefined;
    get latestObs(): ObservableWitness<IbGibTimelineUpdateInfo> | undefined;


    get getFnAlert(): (() => (arg: { title: string, msg: string, }) => Promise<void>) | undefined;
    get getFnPrompt(): (() => (arg: { title: string, msg: string }) => Promise<string>) | undefined;
    get getFnPromptPassword(): (() => (title: string, msg: string) => Promise<string | null>) | undefined;

    /**
     * gets the current local user space according to this svc.
     */
    getLocalUserSpace(arg: {
        /**
         * If true, then we lock by bootstrap/spaceId before trying to retrieve.
         *
         * @default If undefined, will default to false if platform is 'web', else true
         */
        lock?: boolean,
        /**
         * If provided, will look for the space via this id in the bootstrap ibgib.
         * If not provided, will use the bootstrap ibgib's default spaceId.
         */
        localSpaceId?: SpaceId,
    }): Promise<IbGibSpaceAny | undefined>;

    /**
     * gets all local user spaces known in bootstrap ibgib, according to
     * spaceIds property
     *
     * (`bootstrapIbGib.data[BOOTSTRAP_DATA_KNOWN_SPACE_IDS_KEY]` atow)
     *
     * ## example bootstrap ibgib atow
     *
      ```json
      {
          "ib":"bootstrap",
          "gib":"gib",
          "data":{
              "defaultSpaceId":"d455d9a72807617634ccbf1e532b71037c45762f824ec85fcd9a4c2275562f33",
              "spaceIds":["d455d9a72807617634ccbf1e532b71037c45762f824ec85fcd9a4c2275562f33"]
          },
          "rel8ns":{
              "d455d9a72807617634ccbf1e532b71037c45762f824ec85fcd9a4c2275562f33":[
                  "witness space IonicSpace_V1 oij d455d9a72807617634ccbf1e532b71037c45762f824ec85fcd9a4c2275562f33^B336251655E8C56B38E9E86F20E0E42E6C153785F1A0A798ADE6916E71CF055B"
              ]
          }
      }
      ```
     *
     * so this enumerates `data.spaceIds` and gets the corresponding addrs in the `rel8ns`.
     * it then gets the space ibgibs themselves via the local zero space.
     *
     * @returns array of known local user spaces
     *
     * @throws if no local user spaces found (there should be at least one atow i think)
     */
    getLocalUserSpaces(arg: {
        /**
         * If true, then we lock by bootstrap/spaceId before trying to retrieve.
         *
         * @default false if platform is 'web', else true
         */
        lock?: boolean,
    }): Promise<IbGibSpaceAny[]>;

    get zeroSpace(): IbGibSpaceAny;

    get syncing(): boolean;
    initialize(opts: MetaspaceInitializeOptions): Promise<void>;

    /**
     * Takes the given `key`, which should be unique in the given space (or
     * zerospace), and uses that to get the stored `addr` in that space.
     *
     * # notes
     *
     * configuration is stored in "special" ibgibs, and so we need only to persist
     * the address of that configuration.
     */
    getConfigAddr(arg: {
        key: string,
        space?: IbGibSpaceAny,
    }): Promise<string | undefined>;
    fnUpdateBootstrap: (newSpace: IbGibSpaceAny) => Promise<void>;
    fnBroadcast: (info: IbGibTimelineUpdateInfo) => void;
    /**
     * Takes the given `key`, which should be unique in the given space (or
     * zerospace), and uses that to persist the given `addr` in that space.
     *
     * # notes
     *
     * configuration is stored in "special" ibgibs, and so we need only to persist
     * the address of that configuration.
     */
    setConfigAddr(arg: {
        key: string,
        addr: string,
        space?: IbGibSpaceAny,
    }): Promise<IbGibSpaceAny>;
    /**
     * Gets the current root in the given space, or the default zerospace if not provided.
     */
    getCurrentRoot(arg: { space?: IbGibSpaceAny }): Promise<IbGib_V1<RootData> | undefined>;
    /**
     * Sets the current root in the given space, or the default zerospace if not provided.
     */
    setCurrentRoot(arg: { root: IbGib_V1<RootData>, space?: IbGibSpaceAny }): Promise<void>;
    /**
     * Every tjp should be related to one of the roots in a space.
     *
     * You should NOT relate every ibgib frame of a given ibGib.
     */
    rel8ToCurrentRoot(arg: {
        ibGib: IbGib_V1,
        linked?: boolean,
        rel8nName?: string,
        space?: IbGibSpaceAny,
    }): Promise<void>;
    /**
     * rel8s given ibgibs to special ibgib.
     * @see {@link rel8ToSpecialIbGib}
     * @returns new special ibgib addr
     */
    rel8ToSpecialIbGib(arg: {
        type: SpecialIbGibType,
        rel8nName: string,
        /**
         * multiple ibgibs to rel8
         */
        ibGibsToRel8?: IbGib_V1[],
        /**
         * multiple ibgibs to remove rel8n.
         */
        ibGibsToUnRel8?: IbGib_V1[],
        linked?: boolean,
        /**
         * Clears out the special.rel8ns.past array to an empty array.
         *
         * {@see deletePreviousSpecialIbGib} for driving use case.
         */
        severPast?: boolean,
        /**
         * Deletes the previous special ibGib.
         *
         * ## driving use case
         *
         * the latest ibGib is one that is completely ephemeral. It doesn't get attached
         * to the current root, and it only has the current instance. So we don't want to
         * keep around past incarnations.
         */
        deletePreviousSpecialIbGib?: boolean,
        space?: IbGibSpaceAny,
    }): Promise<IbGibAddr>;
    /**
     * Used for tracking tjpAddr -> latest ibGibAddr.
     *
     * Call this when you create a new ibGib.
     *
     * Need to put this in another service at some point, but crunch crunch
     * like pacman's lunch.
     */
    registerNewIbGib(arg: { ibGib: IbGib_V1, space?: IbGibSpaceAny }): Promise<void>;
    /**
     * Will trigger a latest info event to be fired.
     * @param param0
     */
    pingLatest_Local(arg: {
        ibGib: IbGib_V1<any>,
        tjpIbGib: IbGib_V1<any>,
        space?: IbGibSpaceAny,
        /**
         * If true, then will check the latest ibgib cache first. if found, will
         * just return that.
         */
        useCache: boolean,
    }): Promise<void>;
    /**
     * Convenience function for persisting a transform result, which has
     * a newIbGib and optionally intermediate ibGibs and/or dnas.
     *
     * it persists these ibgibs into the given space, else the current space.
     */
    persistTransformResult(arg: {
        resTransform: TransformResult<IbGib_V1>,
        force?: boolean,
        space?: IbGibSpaceAny,
    }): Promise<void>;
    /**
     * Wrapper for retrieving ibgib from a given space, else the current space.
     */
    get(arg: GetIbGibOpts): Promise<GetIbGibResult>;
    /**
     * Wrapper for saving ibgib in a given space, else the current space.
     */
    put(arg: PutIbGibOpts): Promise<PutIbGibResult>;
    /**
     * Wrapper for removing ibgib from the a given space, else the current space.
     */
    delete(arg: DeleteIbGibOpts): Promise<DeleteIbGibResult>;
    /**
     * Wrapper for `getDependencyGraph` fn in `helper/space.ts`, but using
     * `this.localUserSpace` as default space.
     *
     * (refactoring!)
     *
     * ## note on space
     *
     * pass in `null` for space if you want to
     *
     * ## warning
     *
     * This does not (YET) have a flag that gets the latest ibgibs for the graph.
     * It only climbs the current graph, which may not cover all ibgibs when you
     * deal with ibGibs with tjps (timelines). We're going to eventually
     * combat this with auto-updating our rel8ns, but for now we're just going
     * to earmark this for the future.
     *
     * todo: auto-update or better
     *
     * @returns map of addr => ibGib
     */
    getDependencyGraph(opts: GetDependencyGraphOptions): Promise<{ [addr: string]: IbGib_V1 }>;
    /**
     * Wrapper for getting the latest addr in the given space.
     *
     * ## warnings
     *
     * * This was written early and makes many assumptions.
     * * Meant to work with Ionic space atow.
     *
     * @returns latest addr in a given space (or localUserSpace)
     */
    getLatestAddr(arg: {
        ibGib?: IbGib_V1<any>,
        addr?: IbGibAddr,
        tjpAddr?: IbGibAddr,
        tjp?: IbGib_V1<any>,
        space?: IbGibSpaceAny,
    }): Promise<IbGibAddr | undefined>;
    /**
     * Gets the tjpIbGib for the given `ibGib` in the given `space`.
     * atow, naive must be true.
     *
     *
     *
     * @returns tjpIbGib for the given `ibGib`
     */
    getTjpIbGib(arg: {
        ibGib: IbGib_V1<any>,
        naive?: boolean,
        space?: IbGibSpaceAny,
    }): Promise<IbGib_V1<any> | undefined>;
    /**
     * Gets one of the app's special ibGibs, e.g., TagsIbGib.
     *
     * When initializing tags, this will generate some boilerplate tags.
     * I'm going to be doing roots here also, and who knows what else, but each
     * one will have its own initialize specifics.
     *
     * @param initialize initialize (i.e. create) ONLY IF IbGib not found. Used for initializing app (first run).
     *
     * @see {@link createSpecial}
     * @see {@link createTags}
     */
    getSpecialIbGib(arg: {
        type: SpecialIbGibType,
        initialize?: boolean,
        space?: IbGibSpaceAny,
        lock?: boolean,
    }): Promise<IbGib_V1 | null>;
    getSpecialRel8dIbGibs<TIbGib extends IbGib_V1 = IbGib_V1>(arg: {
        type: SpecialIbGibType,
        rel8nName: string,
        space?: IbGibSpaceAny,
    }): Promise<TIbGib[]>;
    /**
     * Feels klugy.
     */
    getPasswordForSecrets(arg: {
        secretIbGibs: IbGib_V1<SecretData_V1>[],
        fnPromptPassword: (title: string, msg: string) => Promise<string | null>,
        dontPrompt?: boolean,
        checkCacheFirst?: boolean,
        cacheAfter?: boolean,
    }): Promise<string | null>;
    getCiphertextIbGib<TEncryptionIbGib extends IbGib_V1<EncryptionData_V1>, TMetadata = any>(arg: {
        /**
         * Um...data...to...erm...encrypt...(as a string)
         */
        plaintext: string,
        /**
         * Password to perform the encryption.
         */
        password: string,
        /**
         * Information about encryption, i.e. encryption settings.
         */
        encryptionIbGib: TEncryptionIbGib,
        /**
         * Decrypts and checks against original data
         */
        confirm?: boolean,
        /**
         * If true, will persist the ibgib
         */
        persist?: boolean,
        /**
         * If you provide this, the resulting ibgib will have the following format:
         * `${ibRoot} ${publicIbMetadata}`. Otherwise, this will default to:
         * `ciphertext ${publicIbMetadata}`, or just `ciphertext` if
         * `publicIbMetadata` is falsy.
         */
        ibRoot?: string,
        /**
         * If you want to include metadata in the ib itself of the
         * ciphertext ibgib. This will of course make this metadata
         * available without loading the full ibgib, but will increase
         * storage size because every address linking to the ibgib will
         * include this as well.
         */
        publicIbMetadata?: string,
        /**
         * If you want to include public, unencrypted metadata in the ibgib's
         * data body itself.
         */
        publicMetadata?: TMetadata,
    }): Promise<TransformResult<CiphertextIbGib_V1>>;
    /**
     * Brings together a ciphertext and secretIbGibs to decrypt
     * the `ciphertextIbGib.data.ciphertext`
     * @returns plaintext string of `ciphertextIbGib.data.ciphertext`
     */
    getPlaintextString(arg: {
        ciphertextIbGib: CiphertextIbGib_V1,
        secretIbGibs: SecretIbGib_V1[],
        fnPromptPassword: (title: string, msg: string) => Promise<string | null>,
        dontPrompt?: boolean,
        space: IbGibSpaceAny,
    }): Promise<string>;
    unwrapEncryptedSyncSpace(arg: {
        encryptedSpace: IbGibSpaceAny,
        fnPromptPassword: (title: string, msg: string) => Promise<string | null>,
        dontPrompt?: boolean,
        space?: IbGibSpaceAny,
    }): Promise<IbGibSpaceAny>;
    getAppSyncSpaces(arg: {
        unwrapEncrypted: boolean,
        createIfNone: boolean,
        /**
         * If true, don't prompt the user if we don't have it already cached.
         *
         * We don't want the user to hit the page and then always have to type in
         * the password, just because my password code sucks atow.
         */
        dontPrompt?: boolean,
        space?: IbGibSpaceAny,
    }): Promise<IbGibSpaceAny[]>;
    getAppRobbotIbGibs(arg: {
        createIfNone: boolean,
        fnPromptRobbot: (space: IbGibSpaceAny, ibGib: RobbotIbGib_V1 | null) => Promise<RobbotPromptResult | undefined>;
        space?: IbGibSpaceAny,
    }): Promise<RobbotIbGib_V1[]>;
    getAppAppIbGibs(arg: {
        createIfNone: boolean,
        fnPromptApp: (space: IbGibSpaceAny, ibGib: AppIbGib_V1 | null) => Promise<AppPromptResult | undefined>,
        space?: IbGibSpaceAny,
    }): Promise<AppIbGib_V1[]>;
    syncIbGibs(arg: {
        dependencyGraphIbGibs?: IbGib_V1[],
        // confirm?: boolean,
        /**
         * If true, will watch ibgibs in dependency graph that have timelines
         * (tjps).
         */
        watch?: boolean,
        /**
         * The meat of the sync process is tracked via a status within the sync
         * spaces themselves. I'm addign this fn to track the pre and post progress
         * stuff, since decrypting takes some time, and building the dependency
         * graphs takes time. Both of these happen before the actual syncing occurs
         * in the spaces.
         */
        fnPreSyncProgress?: (msg: string) => Promise<void>,
    }): Promise<SyncSagaInfo[] | undefined>;
    // /**
    //  * Prompts the user to select a new picture for the given `picIbGib`.
    //  *
    //  * If the user does select one, this creates the appropriate ibgibs (pic,
    //  * binary, dependencies), saves them, registers the new pic (but not the bin)
    //  * in the given `space` if specified, else in the current `localUserSpace`.
    //  */
    // updatePic(arg: {
    //   /**
    //    * picIbGib to update with a new image.
    //    */
    //   picIbGib: PicIbGib_V1,
    //   /**
    //    * space within which we are working, i.e., where the incoming `picIbGib` is
    //    * and where we will save any new ibgibs.
    //    */
    //   space?: IbGibSpaceAny,
    // }): Promise<void>;
    // updateComment(arg: {
    //   /**
    //    * commentIbGib to update with a new text
    //    */
    //   commentIbGib: CommentIbGib_V1,
    //   /**
    //    * space within which we are working, i.e., where the incoming `commentIbGib` is
    //    * and where we will save any new ibgibs.
    //    */
    //   space?: IbGibSpaceAny,
    // }): Promise<void>;

    /**
     * creates a new local space with the given `fnLocalSpaceFactory` function
     * (or if falsy, uses this.fnLocalSpaceFactory this metaspace was
     * initialized with).
     *
     * ONLY is intended to create the space. does not do anything with updating
     * the bootstrap or anything else to actually register the space with this
     * metaspace.
     *
     * @see {@link createLocalSpaceAndUpdateBootstrap}
     */
    createNewLocalSpace(arg: {
        opts: CreateLocalSpaceOptions;
        fnLocalSpaceFactory?: LocalSpaceFactoryFunction;
    }): Promise<IbGibSpaceAny | undefined>;
    /**
     * should create a new local space and does additional initialization steps.
     *
     * atow this includes...
     * * putting the new local space in both this metaspace's zerospace,
     * * putting the new local space in its own space.
     *   * (does not change the gib of the space so this is doable)
     * * updates this metaspace's bootstrap ibgib with the new space thus
     *   registering the space with this metaspace for future use.
     */
    createLocalSpaceAndUpdateBootstrap(arg: {
        /**
         * should be this metaspace.zeroSpace.
         *
         * I think I have this in the impl just to avoid calling to get the
         * zeroSpace.
         */
        zeroSpace: IbGibSpaceAny,
        /**
         * if true, then the caller will be able to cancel when inputting a spaceName.
         * if false, then it will not proceed or stop until the caller provides a spaceName
         *
         * atow ignored if spaceName is given
         */
        allowCancel: boolean,
        /**
         * this should only be true when initializing the metaspace
         *
         * (probably needs to be hidden in some other internal function)
         */
        createBootstrap: boolean,
        /**
         * name of the new space to create
         */
        spaceName: string | undefined,
    }): Promise<IbGibSpaceAny | undefined>;
}

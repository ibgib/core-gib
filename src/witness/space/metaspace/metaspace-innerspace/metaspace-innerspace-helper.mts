import { extractErrorMsg, getUUID, pretty } from "@ibgib/helper-gib/dist/helpers/utils-helper.mjs";
import { getGib } from "@ibgib/ts-gib/dist/V1/transforms/transform-helper.mjs";
import { GIB } from "@ibgib/ts-gib/dist/V1/constants.mjs";

import { IbGibSpaceAny } from "../../../../witness/space/space-base-v1.mjs";
import { DtoToSpaceFunction, LocalSpaceFactoryFunction } from "../metaspace-types.mjs";
import { spaceNameIsValid } from "../../../../witness/space/space-helper.mjs";
import { DEFAULT_LOCAL_SPACE_POLLING_INTERVAL_MS, } from "../../../../witness/space/space-constants.mjs";
import { InnerSpace_V1, InnerSpace_V1_Data } from "../../inner-space/inner-space-v1.mjs";

export const fnCreateNewLocalSpace: LocalSpaceFactoryFunction = async ({
    allowCancel,
    spaceName,
    getFnPrompt,
    logalot,
}) => {
    const lc = `[${fnCreateNewLocalSpace.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: a9bc8c7d54cb4bafae4c4aa515d71580)`); }

        if (spaceName && !spaceNameIsValid(spaceName)) {
            throw new Error(`spaceName (${spaceName}) is invalid.  (E: 7769ee9ed9874ff28548214efc3639c2)`);
            // throw because this is used in RLI and automated testing...
        }
        if (!spaceName) {

            const promptName: () => Promise<void> = async () => {
                const fnPrompt = getFnPrompt();
                const resName = await fnPrompt({
                    title: 'greetings program', msg: `all of your data is stored locally on your device's "local space".

so name your space with alphanumerics & underscores, like 'phone_alice' or 'web_bob_foo', or leave blank and get a random name.

Enter space name:
`
                });

                if (resName === '' && !allowCancel) {
                    spaceName = 'space_' + (await getUUID()).slice(0, 10);
                } else {
                    if (resName && spaceNameIsValid(resName)) {
                        spaceName = resName;
                    }
                }
            };

            // ...prompt for name
            await promptName();
        }

        // ...create in memory with defaults
        const data: InnerSpace_V1_Data = {
            version: '1',
            name: spaceName || ('in-memory-space_' + (await getUUID()).slice(0, 10)),
            uuid: await getUUID(),
            description: 'ephemeral in-memory space',
            persistOptsAndResultIbGibs: false,
            validateIbGibAddrsMatchIbGibs: true,
            trace: false,
            allowPrimitiveArgs: false,
            catchAllErrors: true,
            longPollingIntervalMs: DEFAULT_LOCAL_SPACE_POLLING_INTERVAL_MS,
        };
        const newLocalSpace = new InnerSpace_V1(/*initialData*/data, /*initialRel8ns*/undefined)
        if (logalot) { console.log(`${lc} localSpace.ib: ${newLocalSpace.ib}`); }
        if (logalot) { console.log(`${lc} localSpace.gib: ${newLocalSpace.gib} (before sha256v1)`); }
        if (logalot) { console.log(`${lc} localSpace.data: ${pretty(newLocalSpace.data || 'falsy')}`); }
        if (logalot) { console.log(`${lc} localSpace.rel8ns: ${pretty(newLocalSpace.rel8ns || 'falsy')}`); }
        newLocalSpace.gib = await getGib({ ibGib: newLocalSpace, hasTjp: false });
        if (newLocalSpace.gib === GIB) { throw new Error(`localSpace.gib not updated correctly.`); }
        if (logalot) { console.log(`${lc} localSpace.gib: ${newLocalSpace.gib} (after sha256v1)`); }

        return newLocalSpace as IbGibSpaceAny;
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export const fnDtoToSpace: DtoToSpaceFunction = async (spaceDto) => {
    const lc = `[fnDtoToSpace]`;

    try {
        // afaict we should be dealing with the space itself in memory, so just
        // return the reference itself.
        if ((spaceDto as InnerSpace_V1).ibGibs) {
            return spaceDto as InnerSpace_V1;
        }

        // it's not the reference, so make a new one? (but warn)
        console.warn(`${lc} in memory innerspace dto that isnt' the reference to the object itself in memory? (W: b8973f3a43f546eeaf59f60331896e75)`)
        if (!spaceDto.data) { throw new Error(`invalid spaceDto. InnerSpace_V1 should have truthy data. (E: d402fda7a6a53668b655c2885029a423)`); }
        let space = new InnerSpace_V1(spaceDto.data as InnerSpace_V1_Data, undefined);
        await space.initialized;
        return space;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

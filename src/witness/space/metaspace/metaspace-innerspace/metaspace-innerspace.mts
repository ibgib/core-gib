/**
 * @module metaspace-innerspace
 *
 * in-memory metaspace, for running ibgib apps in memory only, where the
 * underlying data spaces do not use persistent storage mediums.
 */

import { getUUID, } from '@ibgib/helper-gib';

import { IbGibSpaceAny } from '../../../../witness/space/space-base-v1.mjs';
import { InnerSpace_V1 } from '../../../../witness/space/inner-space/inner-space-v1.mjs';
import { IbGibCacheService } from '../../../../common/cache/cache-types.mjs';
import { MetaspaceFactory } from '../metaspace-types.mjs';
import { fnCreateNewLocalSpace, fnDtoToSpace } from './metaspace-innerspace-helper.mjs';
import { MetaspaceBase } from '../metaspace-base.mjs';
import { GLOBAL_LOG_A_LOT, GLOBAL_TIMER_NAME } from '../../../../core-constants.mjs';

const logalot = GLOBAL_LOG_A_LOT;

/**
 * for use when creating a metaspace that only resides in memory, i.e.,
 * doesn't live on a persistent backing store like a filesystem.
 */
export class Metaspace_Innerspace extends MetaspaceBase {

  // we won't get an object back, only a DTO ibGib essentially
  protected lc: string = `[${Metaspace_Innerspace.name}]`;

  /**
   * since this is an in-memory metaspace, we always return the same instance
   * for the zero space.
   */
  private _zeroSpace: InnerSpace_V1 | undefined;

  /**
   * since this is an in-memory metaspace, we always return the same instance
   *
   * returns this._zeroSpace
   */
  get zeroSpace(): IbGibSpaceAny {
    const lc = `${this.lc}[get zeroSpace]`;
    if (!this._zeroSpace) { throw new Error(`${lc} this._zeroSpace falsy. should have been created on this.initialize (E: e723177fd2a74e25eb25e2299dfa4d23)`); }
    return this._zeroSpace;
  }

  constructor(
    protected cacheSvc: IbGibCacheService | undefined,
  ) {
    super(cacheSvc);
    const lc = `${this.lc}[ctor]`;
    if (logalot) {
      console.log(`${lc} ${GLOBAL_TIMER_NAME}`);
      console.timeLog(GLOBAL_TIMER_NAME);
      console.log(`${lc} created.`);
    }
  }

  protected async initializeMetaspaceFactory({ metaspaceFactory }: {
    metaspaceFactory: MetaspaceFactory,
  }): Promise<void> {
    const lc = `[${this.initializeMetaspaceFactory.name}]`;
    try {
      if (logalot) { console.log(`${lc} starting... (I: 5a29670439ac47634dcdfe25225c8223)`); }

      await super.initializeMetaspaceFactory({ metaspaceFactory });

      metaspaceFactory.fnDefaultLocalSpaceFactory ??= (opts) => fnCreateNewLocalSpace(opts);
      metaspaceFactory.fnDtoToSpace ??= (spaceDto) => fnDtoToSpace(spaceDto);

      if (this.metaspaceFactory.fnZeroSpaceFactory) {
        this._zeroSpace = this.metaspaceFactory.fnZeroSpaceFactory!() as InnerSpace_V1;
      } else {
        this._zeroSpace = await fnCreateNewLocalSpace({
          allowCancel: false,
          spaceName: 'in-memory-space_' + (await getUUID()).slice(0, 10),
          logalot,
          getFnPrompt: () => this.getFnPrompt!(),
        }) as InnerSpace_V1;
        if (!this._zeroSpace) { throw new Error(`(UNEXPECTED) create this._zeroSpce is falsy but no error after create? (E: 63c27616be41564377f843baa6725423)`); }
      }
    } catch (error) {
      console.error(`${lc} ${error.message}`);
      throw error;
    } finally {
      if (logalot) { console.log(`${lc} complete.`); }
    }
  }
}

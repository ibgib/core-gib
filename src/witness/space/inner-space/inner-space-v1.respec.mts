import {
    firstOfEach, firstOfAll, ifWe,
    lastOfEach, lastOfAll,
    ifWeMight, iReckon, respecfully
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;
import { getUUID, } from '@ibgib/helper-gib';

import {
    testSpace_createAndInit,
    testSpace_persistTransformResult,
    testSpace_putGetDelete
} from '../space-respec-helper.mjs';
import { InnerSpace_V1, } from './inner-space-v1.mjs';

await respecfully(sir, `[${InnerSpace_V1.name} specs]`, async () => {

    await testSpace_createAndInit({
        respecfulTitle: sir,
        // shortcircuitRespec: true,
        fnGetInitialSpaceData: async () => {
            return {
                name: (await getUUID()).substring(0, 8),
                uuid: await getUUID(),
            };
        },
        fnGetInitialSpaceRel8ns: () => {
            return Promise.resolve(undefined);
        },
        fnGetSpace: (initialData: any, initialRel8ns) => {
            return Promise.resolve(new InnerSpace_V1(initialData, initialRel8ns));
        },
    })

    await testSpace_putGetDelete({
        respecfulTitle: sir,
        // shortcircuitRespec: true,
        fnGetInitialSpaceData: async () => {
            return {
                name: (await getUUID()).substring(0, 8),
                uuid: await getUUID(),
            };
        },
        fnGetInitialSpaceRel8ns: () => {
            return Promise.resolve(undefined);
        },
        fnGetSpace: (initialData: any, initialRel8ns) => {
            return Promise.resolve(new InnerSpace_V1(initialData, initialRel8ns));
        },
    });

    await testSpace_persistTransformResult({
        respecfulTitle: sir,
        // shortcircuitRespec: true,
        fnGetInitialSpaceData: async () => {
            return {
                name: (await getUUID()).substring(0, 8),
                uuid: await getUUID(),
            };
        },
        fnGetInitialSpaceRel8ns: () => {
            return Promise.resolve(undefined);
        },
        fnGetSpace: (initialData: any, initialRel8ns) => {
            return Promise.resolve(new InnerSpace_V1(initialData, initialRel8ns));
        },
    });

});

import { GIB } from '@ibgib/ts-gib/dist/V1/index.mjs';

/**
 * Zero space is a default space that uses default values that should be
 * reproducible and not break or something...it's all in flux really.
 */
export const ZERO_SPACE_ID = 'zero';

/**
 * was 000_default_space in MVP. need to add a conversion
 */
export const IBGIB_SPACE_NAME_DEFAULT = 'zerospace';
// export const IBGIB_SPACE_NAME_DEFAULT = 'default_space';


/**
 * For use with documentation and UX help.
 *
 * This is probably scoped to a particular schema...(e.g. v1)
 */
export const VALID_SPACE_NAME_EXAMPLES = [
    'justLetters', 'valid_here', 'hyphens-allowed', '0CanStartOrEndWithNumbers9'
];
/**
 * For use with documentation and UX help.
 *
 * This is probably scoped to a particular schema...(e.g. v1)
 */
export const INVALID_SPACE_NAME_EXAMPLES = [
    'IHaveASymbol!', 'invalid hereWithSpace', '-cantStartWithHyphen', '_OrUnderscore'
];

/**
 *
 */
export const PERSIST_OPTS_AND_RESULTS_IBGIBS_DEFAULT = false;

/**
 * Yep, this one starts with default instead of ending with it.
 */
export const DEFAULT_LOCAL_SPACE_DESCRIPTION = `This is a local space. There are many like it, but this one is mine.`;

/**
 * The main roots^gib ibgib uses this rel8n name to keep track of roots.
 */
export const SPACE_REL8N_NAME = 'space';

/**
 * rel8n name in a space ibgib to the config ibgib(s?)
 */
export const SPACE_REL8N_NAME_CONFIG = `config`;
/**
 * note that atow this is implied in the regexp not derivative
 * @see {@link SPACE_NAME_REGEXP}
 */
export const SPACE_NAME_MAX_LENGTH = 64;
/**
 * alphanumerics start + end, middle can also contain hyphens atow.
 *
 * NOTE: this implies the value of {@link SPACE_NAME_MAX_LENGTH} but this is not
 * derived from the value. so if you change this, be sure to change/check that
 *
 * @see {@link SPACE_NAME_MAX_LENGTH}
 */
export const SPACE_NAME_REGEXP = /^[a-zA-Z][\w\-]{0,62}[a-zA-Z]$/;

/**
 * A spaces ibGib uses this rel8n name for related sync spaces, used
 * in replicating ibgib spaces.
 */
export const SYNC_SPACE_REL8N_NAME = 'syncSpace';

/**
 * ...?
 */
export const DEFAULT_SPACE_TEXT = 'space';
/**
 * Default icon specifically for spaces.
 */
export const DEFAULT_SPACE_ICON = 'sparkles-outline';
/**
 * Default description specifically for spaces.
 */
export const DEFAULT_SPACE_DESCRIPTION =
    `This is a space ibgib, which is basically a special ibgib who has behavior
to interface with data stores and/or other space(s) to provide concrete location
for ibgibs. All ibgibs can have relationships with other ibgibs, but this one
specifically either implies a physical interface to things like databases,
file systems, and similar; OR, when this space interfaces with other spaces, this
is a logical organization of ibgib locations, like when configuring clusters or
consensus algorithms.`;
/**
 * rel8n name used inside the root to those ibgib it contains.
 *
 * @example
 * ```json
 *  {
 *      ib: root,
 *      gib: ABC123,
 *      data: {...},
 *      rel8ns: {
 *          [rel8nName]: ["a^1", "b^2"]
 *      }
 *  }
 * ```
 */
export const DEFAULT_SPACE_REL8N_NAME = 'x';

/**
 * Interval in ms between polling for updates ("notifications") between local
 * space (ibgibs service) and dynamo sync space(s), or for use within a single
 * space for checking its internal state.
 *
 * Of course, long polling is very hacky, but so is using DynamoDB in the cloud
 * for a sync space. Obviously need to progress to a more mature and robust sync
 * space/outer space architecture.
 */
export const DEFAULT_LOCAL_SPACE_POLLING_INTERVAL_MS = 30_000;
export const DEFAULT_LOCAL_SPACE_POLLING_DELAY_FIRST_RUN_MS = 10_000;

/**
 * {@link DEFAULT_LOCAL_SPACE_POLLING_INTERVAL_MS} but for outer spaces, so
 * longer interval atow.
 */
export const DEFAULT_OUTER_SPACE_POLLING_INTERVAL_MS = 20_000;
/**
 * Amount of time to delay for FIRST poll execution.
 */
export const DEFAULT_OUTER_SPACE_POLLING_DELAY_FIRST_RUN_MS = 10_000;

/**
 * When a status is first created, this is used to indicate that
 * the tjp has not been set.
 */
export const STATUS_UNDEFINED_TJP_GIB = GIB;
/**
 * When a status is first created, this is used to indicate that
 * the txId has not been set.
 */
export const STATUS_UNDEFINED_TX_ID = '0';

export const SPACE_LOCK_IB_TERM = 'space_lock';
/**
 * When attempting to acquire a lock ona  space, and it is already lock, it will
 * wait a random amount of ms before trying to lock again. This is the default
 * max amount of ms before reattempting.
 */
export const DEFAULT_MAX_DELAY_MS_RETRY_LOCK_ACQUIRE = 100;
/**
 * Will retry this many times before giving up...
 */
export const DEFAULT_MAX_DELAY_RETRY_LOCK_ACQUIRE_ATTEMPTS = 100;
/**
 * We don't want someone locking a space forever by accident.
 */
export const MAX_LOCK_SECONDS_VALID = 60 * 2; // two minutes
/**
 * Default value for secondsValid when acquiring/releasing lock in/on LOCAL
 * space.
 */
export const DEFAULT_SECONDS_VALID_LOCAL = 30;
/**
 * Default value for secondsValid when acquiring/releasing lock in/on
 * OUTER space, e.g. sync spaces like aws dynamo sync space.
 */
export const DEFAULT_SECONDS_VALID_OUTER = 30;

/**
 * retry count when calling getDependencyGraph.
 *
 * ## driving use case
 *
 * dependency graph fails when concurrent merges made in sync space.
 * need to wait until previous graph merger happens then retry.
 */
export const DEFAULT_MAX_RETRIES_GET_DEPENDENCY_GRAPH_OUTERSPACE = 5;
/**
 * when getting dependency graph, sometimes we have to wait to retry
 */
export const DEFAULT_MS_BETWEEN_RETRIES_GET_DEPENDENCY_GRAPH_OUTERSPACE = 5000;
/**
 * retry count when calling getDependencyGraph.
 *
 * ## driving use case
 *
 * dependency graph fails when concurrent merges made in sync space.
 * need to wait until previous graph merger happens then retry.
 */
export const DEFAULT_MAX_RETRIES_GET_DEPENDENCY_GRAPH_LOCAL = 3;
/**
 * when getting dependency graph, sometimes we have to wait to retry
 */
export const DEFAULT_MS_BETWEEN_RETRIES_GET_DEPENDENCY_GRAPH_LOCAL = 1000;

/**
 * currently not used in ionic-gib.
 */
export const DEFAULT_TX_ID_LENGTH = 5;

import { IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/index.mjs';
import { IbGibAddr, IbGib, IbGibRel8ns, TjpIbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';

import {
    Witness,
    WitnessData_V1, WitnessResultData, WitnessResultIbGib, WitnessResultRel8ns,
} from '../witness-types.mjs';
import { IbGibSpaceAny } from './space-base-v1.mjs';
import { ENCRYPTION_REL8N_NAME } from '../../common/encrypt/encrypt-constants.mjs';
import { WitnessCmdData, WitnessCmdIbGib, WitnessCmdRel8ns, } from '../witness-cmd/witness-cmd-types.mjs';

/**
 * Marker type to show intent that it should be the spaceId, i.e.
 * space.data.uuid.
 */
export type SpaceId = string;

/**
 * Common data among all ibgib spaces.
 */
export interface IbGibSpaceData extends WitnessData_V1 {
    /**
     * redeclaration for the name of the space, to make it required as opposed
     * to the base declaration which is optional.
     *
     * does NOT have to be unique.
     */
    name: string;
    /**
     * Redeclared over {@link WitnessData_V1.uuid} just for code readability,
     * showing the id to be a {@link SpaceId}, as well making it a required
     * field.
     */
    uuid: SpaceId;
    /**
     * If true, when this space receives a command that includes incoming ibGibs
     * and ibGibAddrs, we will ensure the ibGibs have a 1-to-1 correspondence to
     * the addrs we're logging, and that the gib hashes are verified against the
     * ibGibs themselves.
     *
     * Otherwise, someone could pass in a bunch of legitimate addresses and
     * illegitimate ibGibs (that have little to do with the addresses). This
     * could at best be a coding mistake & at worst be malicious.
     */
    validateIbGibAddrsMatchIbGibs?: boolean;
    /**
     * interval between polling calls made to other spaces, e.g. sync spaces,
     * or used within a space to check inside itself for updates.
     *
     * ## notes
     *
     * * use as needed
     * * I want to just have this as a sync space setting, but since they are
     *   enciphered, I don't want to just have them sitting around in plaintext.
     *   * So I'm going to have it in the local app space, which the ibgibs
     *     service will check in order for it to decide on interval.
     *   * definitely a code smell, but I'm still resolving what a local app
     *     space is vs the service that accesses it.
     */
    longPollingIntervalMs?: number;
}

/**
 * Specifically for ibgib spaces that are implemented via a path/subpath
 * strategy, i.e., using a filesystem-like addressing mechanism.
 */
export interface IbGibSpaceData_Subpathed extends IbGibSpaceData {
    /**
     * Redeclared here to make this required (not optional)
     */
    uuid: SpaceId;
    baseDir: string | any;
    encoding: string | any;
    /**
     * this is to help mitigate against too long path limits
     */
    mitigateLongPaths?: boolean;
    /**
     * if mitigating against long path lengths, this is the deciding split.
     *
     * so if the longPathLength is 250, and a full path would be 250, then
     * that is now a long path. 249 would be a short path and would not need
     * mitigation.
     */
    longPathLength?: number;
    baseSubPath: string;
    spaceSubPath: string;
    /**
     * if a filepath is too long, things get stuffed in here
     *
     * this is to help mitigate against too long path limits
     */
    longSubPath?: string;
    ibgibsSubPath: string;
    metaSubPath: string;
    binSubPath: string;
    dnaSubPath: string;
    n?: number;
    timestamp?: string;
}

export interface IbGibSpaceRel8ns extends IbGibRel8ns_V1 {
    [ENCRYPTION_REL8N_NAME]?: IbGibAddr[];
}


/**
 * Cmds for interacting with ibgib spaces.
 *
 * Not all of these will be implemented for every space.
 *
 * ## todo
 *
 * change these commands to better structure, e.g., verb/do/mod, can/get/addrs
 * */
export type IbGibSpaceOptionsCmd =
    'get' | 'put' | 'delete';
/** Cmds for interacting with ibgib spaces.  */
export const IbGibSpaceOptionsCmd = {
    /** Retrieve ibGib(s) out of the space (does not remove them). */
    get: 'get' as IbGibSpaceOptionsCmd,
    /** Registers/imports ibGib(s) into the space. */
    put: 'put' as IbGibSpaceOptionsCmd,
    /** Delete an ibGib from a space */
    delete: 'delete' as IbGibSpaceOptionsCmd,
}

/**
 * Flags to affect the command's interpretation.
 */
export type IbGibSpaceOptionsCmdModifier =
    'can' | 'addrs' | 'latest' | 'watch' | 'unwatch' | 'tjps';
/**
 * Flags to affect the command's interpretation.
 */
export const IbGibSpaceOptionsCmdModifier = {
    /**
     * Only interested if possibility to do command.
     *
     * This can be due to authorization or other.
     */
    can: 'can' as IbGibSpaceOptionsCmdModifier,
    /**
     * Only return the addresses of ibgibs
     */
    addrs: 'addrs' as IbGibSpaceOptionsCmdModifier,
    /**
     * Only interested in the latest one(s).
     *
     * The incoming addr(s) should be the tjp(s), since "latest"
     * only makes sense with unique timelines which are referenced by
     * their tjps.
     *
     * ## notes
     *
     * ATOW I'm actually using this in the aws dynamodb ibgib space to
     * get "newer" ibgibs, not just the latest.
     */
    latest: 'latest' as IbGibSpaceOptionsCmdModifier,
    /**
     * Ask to get updates on tjps in ibGibAddrs.
     */
    watch: 'watch' as IbGibSpaceOptionsCmdModifier,
    /*
     * Ask to stop getting updates on tjps in ibGibAddrs.
     */
    unwatch: 'unwatch' as IbGibSpaceOptionsCmdModifier,
    /**
     * Get the tjp ibgibs/addrs for given ibgib(s)
     */
    tjps: 'tjps' as IbGibSpaceOptionsCmdModifier,
}

/** Information for interacting with spaces. */
export interface IbGibSpaceOptionsData
    extends WitnessCmdData<IbGibSpaceOptionsCmd, IbGibSpaceOptionsCmdModifier> {
    /**
     * If putting, this will force replacing the file.
     *
     * ## intent
     * atow this is just for `put` commands.
     */
    force?: boolean;
    /**
     */
    catchAllErrors?: boolean;
    /**
     */
    trace?: boolean;
}

export interface IbGibSpaceOptionsRel8ns extends WitnessCmdRel8ns {
}

export interface IbGibSpaceOptionsIbGib<
    TIbGib extends IbGib = IbGib_V1,
    TOptsData extends IbGibSpaceOptionsData = IbGibSpaceOptionsData,
    // TOptsRel8ns extends IbGibSpaceOptionsRel8ns = IbGibSpaceOptionsRel8ns
    TOptsRel8ns extends IbGibSpaceOptionsRel8ns = IbGibSpaceOptionsRel8ns,
> extends WitnessCmdIbGib<TIbGib, IbGibSpaceOptionsCmd, IbGibSpaceOptionsCmdModifier, TOptsData, TOptsRel8ns> {
}

/**
 * Shape of result data common to all (most) space interactions.
 *
 * This is in addition of course to {@link WitnessResultData}.
 */
export interface IbGibSpaceResultData extends WitnessResultData {
    /**
     * If the `cmd` is `canGet` or `canPut`, this holds the result that indicates
     * if you can or can't.
     */
    can?: boolean;
    /**
     * Addresses not found in a get.
     */
    addrsNotFound?: IbGibAddr[];
    /**
     * Addresses that are already in the space when requesting `put` or `canPut`.
     */
    addrsAlreadyHave?: IbGibAddr[];
    /**
     * Result map used when
     *
     * Mapping of incoming ibGibAddr -> latestAddr | null
     *
     * If there is a tjp/timeline -> maps to the latest in the store.
     * If there is no tjp -> maps to the incoming addr.
     * If the incoming addr is not found in the store -> maps to null.
     */
    latestAddrsMap?: { [addr: string]: IbGibAddr | null }
    /**
     * Map of TjpAddr -> newer LatestIbGibAddr notification.
     *
     * ## about
     *
     * When using the `watch` command modifier, a caller can subscribe to
     * updates/notifications to a timeline. When an update occurs in the space
     * via a `put` cmd that ends up creating/storing a newer ibgib address than
     * the one the receiving space knows the caller is aware of, it will try to
     * make the caller aware of the update.
     *
     * There are two ways to do this:
     * 1. Store the notification info locally until the next interaction between
     *    the space and the caller.
     * 2. Send the notification actively to the caller.
     *
     * For the first implementation of notifications, only the first will be
     * implemented. Local notifications will be implemented, but active
     * internodal notification in the general sense will be implemented at a
     * later time.
     *
     * ## notes
     *
     * * may also implement a dedicated command to watch subscriptions.
     */
    watchTjpUpdateMap?: { [tjpAddr: string]: IbGibAddr; }
}

export interface IbGibSpaceResultRel8ns extends WitnessResultRel8ns { }

export interface IbGibSpaceResultIbGib<
    TIbGib extends IbGib,
    TResultData extends IbGibSpaceResultData,
    TResultRel8ns extends IbGibSpaceResultRel8ns
>
    extends WitnessResultIbGib<TIbGib, TResultData, TResultRel8ns> {
}

/**
* Data space adapter/provider, such that a space should only have one type of...
*   * ibGib shape
*   * witness arg shape (which brings in external ibGibs)
*   * witness result shape
*
* So this interface facilitates that belief.
*/
export interface IbGibSpace<
    TIbGib extends IbGib,
    TOptionsData extends IbGibSpaceOptionsData,
    TOptionsRel8ns extends IbGibSpaceOptionsRel8ns,
    TOptionsIbGib extends IbGibSpaceOptionsIbGib<TIbGib, TOptionsData, TOptionsRel8ns>,
    TResultData extends IbGibSpaceResultData,
    TResultRel8ns extends IbGibSpaceResultRel8ns,
    TResultIbGib extends IbGibSpaceResultIbGib<TIbGib, TResultData, TResultRel8ns>,
    TData extends IbGibSpaceData = IbGibSpaceData,
    TRel8ns extends IbGibSpaceRel8ns = IbGibRel8ns,
>
    extends Witness<TOptionsIbGib, TResultIbGib, TData, TRel8ns> {
    witness(arg: TOptionsIbGib): Promise<TResultIbGib | undefined>;
}

export type SpaceLockAction = 'lock' | 'unlock';
export type SpaceLockScope = 'all' | TjpIbGibAddr;

/**
 * Data shape for {@link IbGibSpaceLockIbGib}.
 *
 * This includes the options passed in and contains any result data of the lock
 * as well.
 */
export interface IbGibSpaceLockData {
    /**
     * In-memory unique identifier associated with the lock.
     *
     * ## intent
     *
     * I intend this mainly as a device for differentiating among multiple
     * tabs open on the same browser. These share the same IndexedDB instance
     * (and thus the same space bucket), but they have different caching
     * mechanisms and interfaces to this bucket.
     */
    instanceId?: string;
    /**
     * self-explanatory
     */
    // action?: SpaceLockAction;
    /**
     * When setting the lock, this was the maximum amount of time the lock is
     * valid.
     *
     * {@link expirationUTC}
     */
    secondsValid?: number;
    /**
     * When setting the lock, this was the calculated expiration string based on
     * {@link secondsValid}.
     *
     * If the lock is not manually released, this will determine if the lock is
     * adhered to.
     */
    expirationUTC?: string;
    /**
     * The scope to which the lock applies.
     */
    scope: SpaceLockScope;
    /**
     * True if space was already locked.
     */
    alreadyLocked?: boolean;
    /**
     * True if caller's request to lock the space was executed.
     */
    success?: boolean;
    /**
     * If errored, this is the message.
     */
    errorMsg?: string;
}

/**
 * Options for the function that locks a space.

 * {@link IbGibSpaceLockData}
 * {@link IbGibSpaceLockIbGib}
 */
export interface IbGibSpaceLockOptions extends IbGibSpaceLockData {
    /**
     * the space to lock/unlock
     */
    space: IbGibSpaceAny;
}

/**
 * Rel8ns shape for {@link IbGibSpaceLockIbGib}
 *
 * marker interface atm
 */
export interface IbGibSpaceLockRel8ns extends IbGibRel8ns_V1 { }

/**
 * When locking a space, this is the ibGib that contains informatino regarding
 * the process. This includes if the lock was successful, how long the lock is
 * good for, etc.
 *
 * ## notes
 *
 * * This is meant to be completely ephemeral and there will be no gib. There
 *   may be many calls to lock/release in tight loops and atow it wouldn't seem
 *   provide us with much benefit to store this kind of metadata.
 */
export interface IbGibSpaceLockIbGib
    extends IbGib_V1<IbGibSpaceLockData, IbGibSpaceLockRel8ns> {

}

/**
 * Marker type to indicate that a string is meant to be a transmission id.
 */
export type TxId = 'string';

/**
 * @module bootstrap
 *
 * @see {@link BootstrapIbGib}
 */

import { IbGibRel8ns_V1, IbGib_V1 } from "@ibgib/ts-gib/dist/V1/index.mjs";

import { SpaceId } from "../space-types.mjs";
import { BOOTSTRAP_DATA_DEFAULT_SPACE_ID_KEY, BOOTSTRAP_DATA_KNOWN_SPACE_IDS_KEY } from "./bootstrap-constants.mjs";

/**
 * Data of a {@link BootstrapIbGib}
 */
export interface BootstrapData {
    /**
     * this id will be the default space of the bootstrap (meta space).
     *
     * The `bootstrapIbGib.rel8ns` will contain links to the space addresses,
     * with `rel8nName === spaceId`.
     *
     * So to get the default space, you need the default space address. To get
     * that address, you first read this data value, then index into
     * `bootstrap.rel8ns[spaceId]`.
     */
    [BOOTSTRAP_DATA_DEFAULT_SPACE_ID_KEY]: SpaceId;
    /**
     * List of known spaces in this bootstrap. These should be rel8n names in
     * the bootstrap's `rel8ns` map.
     */
    [BOOTSTRAP_DATA_KNOWN_SPACE_IDS_KEY]: SpaceId[];
}

/**
 * Rel8ns of a {@link BootstrapIbGib}
 *
 * atow, these will have rel8nNames of [spaceId] that point to the corresponding
 * latest spaceAddr, such that the length is always === 1.
 */
export interface BootstrapRel8ns extends IbGibRel8ns_V1 {
    // [BOOTSTRAP_REL8N_NAME_SPACE]: IbGibAddr[];
}

/**
 * # tl;dr
 *
 * a bootstrap ibgib...
 *
 * * is a meta space without hash.
 * * contain info for a "default" local space
 *   * default local space defines what space is used when not specified in space functions
 * * contains a list of known spaces available to some entity (app/robbot/etc via ibgibs svc)
 *
 * # bootstrapping "applications"
 *
 * When the application first starts, it looks to bootstrap itself.  So it will
 * look for an ibgib with this "primitive" address, i.e. where the gib is the
 * string literal 'gib'. It looks for this address inside what is called the
 * "zero space". This is a default space with default parameters that always
 * points to the same location, relative to the context (app in our current case,
 * but in the future the context could be e.g. an IoT device or AI microservice).
 *
 * So the context execution starts, creating a default zero space ibgib in
 * memory, which itself has a default location/parameters that the space looks
 * in in storage. Here it looks for the bootstrap ibgib with a known address
 * "bootstrap^gib" (atow). Inside that bootstrap ibgib, there should be at least
 * one local space referenced, or a new one must be created and then stored here
 * for future app/context startups.
 *
 * Spaces are rel8d by their `spaceId`'s in `ibgib.rel8ns`, and the `ibgib.data`
 * key (`ibgib.data.defaultSpaceId` atow) contains the default `spaceId`.  If
 * there are multiple local spaces that the bootstrap ibgib knows about, there
 * will be only one "default" set.
 *
 * ## first run
 *
 * @see {@link IbgibsService.createNewLocalSpaceAndBootstrapGib}
 * @see {@link getLocalSpace} in `helper/space.ts`
 *
 * A new local space will be created, whose parameters (including its `uuid`
 * which is its `spaceId`) contribute to its reiffied gib hash. A new bootstrap
 * ibgib is created, and in its `bootstrapIbGib.data.defaultSpaceId` we set the
 * newly created local space's id. We then rel8 the space to the bootstrap also
 * via this spaceId as the rel8nName.
 *
 * Note that when the local space is updated, its most recent address must
 * replace the address that the bootstrap ibgib is pointing to.
 *
 * @example
 *
 * Here is a copy of a bootstrap^gib.json file on March 30, 2022:
 * ```
 * {
 *   "ib":"bootstrap",
 *   "gib":"gib",
 *   "data":{
 *     "defaultSpaceId":"72af3bba9c6da224829de86982346e283469823e49862398a56510c238bad869",
 *     "spaceIds":["72af3bba9c6da224829de86982346e283469823e49862398a56510c238bad869"]
 *   },
 *   "rel8ns":{
 *     "72af3bba9c6da224829de86982346e283469823e49862398a56510c238bad869": [
 *       "witness space IonicSpace_V1 test_space_name 72af3bba9c6da224829de86982346e283469823e49862398a56510c238bad869^62879B18C2726D27262626552672868923477EE09171626A386C1982F392AC26"
 *     ]
 *   }
 * }
 * ```
 *
 * ## notes
 *
 * Usually primitives are not stored/persisted. This is because the `gib`
 * indicates that there is no hash corroboration ("guarantee") to the internal
 * data or rel8ns. However, a newly started app has to start somewhere. This
 * offers an alternative to using app storage and streamlines the app overall,
 * since instead of working with two stores (in Ionic: `Storage` and
 * `FileSystem`) we will just be working with one (`FileSystem`).
 *
 * In the future, we'll want to do a workflow here where the user
 * can start from an existing space, but for now it's just located
 * here.
 */
export interface BootstrapIbGib
    extends IbGib_V1<BootstrapData, BootstrapRel8ns> {
}

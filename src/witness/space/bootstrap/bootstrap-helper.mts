import { getIbGibAddr } from "@ibgib/ts-gib";
import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/index.mjs";
import { BOOTSTRAP_DATA_DEFAULT_SPACE_ID_KEY, BOOTSTRAP_IBGIB_ADDR } from "./bootstrap-constants.mjs";
import { validateIbGibIntrinsically } from "@ibgib/ts-gib/dist/V1/validate-helper.mjs";
import { BootstrapData, BootstrapRel8ns } from "./bootstrap-types.mjs";

export async function validateBootstrapIbGib(bootstrapSpace: IbGib_V1): Promise<boolean> {
    const lc = `[${validateBootstrapIbGib.name}]`;
    const errors: string[] = [];
    try {
        const addr = getIbGibAddr({ ibGib: bootstrapSpace });
        if (addr !== BOOTSTRAP_IBGIB_ADDR) {
            errors.push(`invalid bootstrapSpace addr. Should equal "${BOOTSTRAP_IBGIB_ADDR}" (E: ecfdbed719284db7a1aa3f867f706fe9)`);
        }
        let intrinsicErrors = await validateIbGibIntrinsically({ ibGib: bootstrapSpace });
        if (intrinsicErrors?.length ?? -1 > 0) { intrinsicErrors!.forEach(e => errors.push(e)); }

        const data = (bootstrapSpace.data as BootstrapData);
        const rel8ns = (bootstrapSpace.rel8ns as BootstrapRel8ns);

        if (Object.keys(data || {}).length === 0) {
            errors.push(`invalid bootstrapSpace data. data required (E: 5a9bd15dd0644f9b93cafbbba660cfdf)`);
        }
        if ((data.spaceIds ?? []).length === 0) {
            errors.push(`invalid bootstrapSpace, data.spaceIds required. (E: 6b91ddc12cfd41e59ded7d7502c1909f)`);
        }
        if (Object.keys(bootstrapSpace.rel8ns || {}).length === 0) {
            errors.push(`invalid bootstrapSpace rel8ns (empty). Should have at least one rel8n, with rel8nName corresponding to the spaceId (E: b188ce4ae25e49f794f35e141bc2ecde)`);
        }
        data.spaceIds.forEach((spaceId: string) => {
            if ((rel8ns[spaceId] ?? []).length === 0) {
                errors.push(`invalid bootstrap. Each spaceId listed in data should have a corresponding address in rel8ns. spaceId (${spaceId}) missing. (E: 62dd0d76e29a415a98b4b27deb8db17e)`);
            }
        });
        if (!data[BOOTSTRAP_DATA_DEFAULT_SPACE_ID_KEY]) {
            errors.push(`invalid bootstrap. data.${BOOTSTRAP_DATA_DEFAULT_SPACE_ID_KEY} required. (E: f763af2e275f445cbf1db5801bacafad)`);
        } else if ((rel8ns[data[BOOTSTRAP_DATA_DEFAULT_SPACE_ID_KEY]] ?? []).length === 0) {
            errors.push(`invalid bootstrap. data.${BOOTSTRAP_DATA_DEFAULT_SPACE_ID_KEY} (${data[BOOTSTRAP_DATA_DEFAULT_SPACE_ID_KEY]}) not found in rel8ns. (E: 44d0799d232f4a51a0b0019ebebe019f)`);
        }
        if (errors.length === 0) {
            return true;
        } else {
            console.error(`${lc} errors: ${errors.join('|')}`);
            return false;
        }
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        return false;
    }
}

import { GIB } from '@ibgib/ts-gib/dist/V1/index.mjs';

/**
 * See {@link BootstrapIbGib}
 */
export const BOOTSTRAP_IBGIB_ADDR = `bootstrap^${GIB}`;
/**
 * {@see BootstrapData}
 */
export const BOOTSTRAP_DATA_DEFAULT_SPACE_ID_KEY = `defaultSpaceId`;
/**
 * Key for index tracking known spaceIds in a bootstrap ibgib.
 * {@see BootstrapData}
 */
export const BOOTSTRAP_DATA_KNOWN_SPACE_IDS_KEY = `spaceIds`;

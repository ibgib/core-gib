import {
    firstOfEach, firstOfAll, ifWe,
    lastOfEach, lastOfAll,
    ifWeMight, iReckon, respecfully
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;
import { getUUID, } from '@ibgib/helper-gib';
import { getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../core-constants.mjs';
import {
    testSpace_createAndInit,
    testSpace_persistTransformResult,
    testSpace_putGetDelete
} from '../space-respec-helper.mjs';
import { FilesystemSpace_V1, } from './filesystem-space-v1.mjs';
import {
    PutIbGibFileOpts, PutIbGibFileResult,
    DeleteIbGibFileOpts, DeleteIbGibFilesResult,
    GetIbGibFileOpts, GetIbGibFileResult
} from './filesystem-types.mjs';

const logalot = GLOBAL_LOG_A_LOT;

/**
 * simple mock that naively implements file operations using a private map that holds ibgibs.
 */
class MockFilesystemSpace_V1 extends FilesystemSpace_V1<any, any, any, any> {
    protected getMetaStoneAddrs({ tjpGib, }: { tjpGib: string; }): Promise<string[]> {
        throw new Error('Method not implemented.');
    }

    protected async ensureDirsImpl(paths: string[]): Promise<void> {
        const lc = `${this.lc}[${this.ensureDirsImpl.name}]`;
        if (logalot) { console.log(`${lc} executed (I: 566d080b91eb7dc888ea5397e2838c23)`); }
    }
    #files: { [key: string]: IbGib_V1 } = {};

    protected async putFile({ ibGib, isDna, }: PutIbGibFileOpts): Promise<PutIbGibFileResult> {
        if (!ibGib) { throw new Error(`ibGib required (E: 8a63c7d0922565d6d14ceabd9d881f23)`); }
        let addr = getIbGibAddr({ ibGib });
        this.#files[addr] = ibGib;
        return { success: true }
    }
    protected async deleteFile({ addr, isDna, }: DeleteIbGibFileOpts): Promise<DeleteIbGibFilesResult> {
        if (!addr) { throw new Error(`addr required (E: 74167b11ed53abd5db45da2e19d4c923)`); }
        if (this.#files[addr]) { delete this.#files[addr]; }
        return { success: true }
    }
    protected async getFile({ addr, isDna, }: GetIbGibFileOpts): Promise<GetIbGibFileResult> {
        if (!addr) { throw new Error(`addr required (E: 07bcbc77f59b26266f447bedc7bab723)`); }
        if (this.#files[addr]) {
            return {
                success: true,
                ibGib: this.#files[addr],
            };
        } else {
            return {
                success: false,
                errorMsg: 'addr not found',
            }
        }
    }

}

await respecfully(sir, `[${FilesystemSpace_V1.name} specs]`, async () => {

    await testSpace_createAndInit({
        respecfulTitle: sir,
        // shortcircuitRespec: true,
        fnGetInitialSpaceData: async () => {
            return {
                name: (await getUUID()).substring(0, 8),
                uuid: await getUUID(),
            };
        },
        fnGetInitialSpaceRel8ns: () => {
            return Promise.resolve(undefined);
        },
        fnGetSpace: (initialData: any, initialRel8ns) => {
            return Promise.resolve(new MockFilesystemSpace_V1(initialData, initialRel8ns));
        },
    })

    await testSpace_putGetDelete({
        respecfulTitle: sir,
        // shortcircuitRespec: true,
        fnGetInitialSpaceData: async () => {
            return {
                name: (await getUUID()).substring(0, 8),
                uuid: await getUUID(),
            };
        },
        fnGetInitialSpaceRel8ns: () => {
            return Promise.resolve(undefined);
        },
        fnGetSpace: (initialData: any, initialRel8ns) => {
            return Promise.resolve(new MockFilesystemSpace_V1(initialData, initialRel8ns));
        },
    });

    await testSpace_persistTransformResult({
        respecfulTitle: sir,
        // shortcircuitRespec: true,
        fnGetInitialSpaceData: async () => {
            return {
                name: (await getUUID()).substring(0, 8),
                uuid: await getUUID(),
            };
        },
        fnGetInitialSpaceRel8ns: () => {
            return Promise.resolve(undefined);
        },
        fnGetSpace: (initialData: any, initialRel8ns) => {
            return Promise.resolve(new MockFilesystemSpace_V1(initialData, initialRel8ns));
        },
    });

});

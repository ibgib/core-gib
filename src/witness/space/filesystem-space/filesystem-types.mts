import { IbGibAddr } from "@ibgib/ts-gib";
import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/index.mjs";

/**
 * encoding of the file.
 *
 * I'm bringing this over from my IonicSpace_V1 which used Capacitor.
 * In that lib, this is a string mapped enum.
 */
export type Encoding = 'utf8' | 'UTF-8' | string;

/**
 * I'm bringing this over from my IonicSpace_V1 which used Capacitor.
 * In that lib, this is a string mapped enum.
 *
 * But in that case, this is a special folder, whereas with this
 * implementation that may not be the case.
 */
export type Directory = string;

/**
 * common shape of get/put/delete file function results
 */
export interface FileResult {
    success?: boolean;
    /**
     * If errored, this will contain the errorMsg.
     */
    errorMsg?: string;
}

/**
 * Options for retrieving data from the file system.
 */
export interface GetIbGibFileOpts {
    /**
     * If getting ibGib object, this is its address.
     */
    addr?: IbGibAddr;
    /**
     * Are we looking for a DNA ibgib?
     */
    isDna?: boolean;
}
/**
 * Result for retrieving an ibGib from the file system.
 */
export interface GetIbGibFileResult extends FileResult {
    /**
     * ibGib if retrieving a "regular" ibGib.
     *
     * This is used when you're not getting a pic, e.g.
     */
    ibGib?: IbGib_V1;
}

export interface PutIbGibFileOpts {
    ibGib?: IbGib_V1;
    /**
     * If true, will store in a different folder.
     */
    isDna?: boolean;
}
export interface PutIbGibFileResult extends FileResult { }

export interface DeleteIbGibFileOpts extends GetIbGibFileOpts { }
export interface DeleteIbGibFilesResult extends FileResult { }

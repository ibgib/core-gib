/**
 * @module node-filesystem-space-types
 *
 * mostly interfaces, some constants
 */


import { IbGibRel8ns_V1, } from '@ibgib/ts-gib/dist/V1/index.mjs';

import { DEFAULT_LOCAL_SPACE_POLLING_INTERVAL_MS, IBGIB_SPACE_NAME_DEFAULT, ZERO_SPACE_ID } from '../../space-constants.mjs';
import {
    IBGIB_BASE_SUBPATH, IBGIB_SPACE_SUBPATH_DEFAULT, IBGIB_BASE_DIR,
    IBGIB_ENCODING, IBGIB_IBGIBS_SUBPATH, IBGIB_META_SUBPATH,
    IBGIB_BIN_SUBPATH, IBGIB_DNA_SUBPATH,
    IBGIB_LONG_SUBPATH,
    DEFAULT_LONG_PATH_LENGTH,
    DEFAULT_FILESYSTEM_SPACE_DESCRIPTION,
} from '../filesystem-constants.mjs';
import {
    FilesystemSpaceData_V1,
    FilesystemSpaceOptionsData, FilesystemSpaceOptionsRel8ns, FilesystemSpaceOptionsIbGib,
    FilesystemSpaceResultData, FilesystemSpaceResultRel8ns, FilesystemSpaceResultIbGib,
} from '../filesystem-space-v1.mjs';


/**
 * This is the shape of data about this space itself (not the contained ibgibs' spaces).
 */
export interface NodeFilesystemSpaceData_V1 extends FilesystemSpaceData_V1 {
}

/**
 * Used in bootstrapping.
 *
 * If you change this, please bump the version
 *
 * (but of course won't be the end of the world when this doesn't happen).
 */
export const DEFAULT_NODE_FILESYSTEM_SPACE_DATA_V1: NodeFilesystemSpaceData_V1 = {
    version: '2',
    uuid: ZERO_SPACE_ID,
    name: IBGIB_SPACE_NAME_DEFAULT,
    classname: 'NodeFilesystemSpace_V1',
    baseDir: IBGIB_BASE_DIR,
    encoding: IBGIB_ENCODING,
    baseSubPath: IBGIB_BASE_SUBPATH,
    spaceSubPath: IBGIB_SPACE_SUBPATH_DEFAULT,
    ibgibsSubPath: IBGIB_IBGIBS_SUBPATH,
    metaSubPath: IBGIB_META_SUBPATH,
    binSubPath: IBGIB_BIN_SUBPATH,
    dnaSubPath: IBGIB_DNA_SUBPATH,
    longSubPath: IBGIB_LONG_SUBPATH,
    mitigateLongPaths: true,
    longPathLength: DEFAULT_LONG_PATH_LENGTH,
    persistOptsAndResultIbGibs: false,
    validateIbGibAddrsMatchIbGibs: false,
    longPollingIntervalMs: DEFAULT_LOCAL_SPACE_POLLING_INTERVAL_MS,
    allowPrimitiveArgs: false,
    catchAllErrors: true,
    description: DEFAULT_FILESYSTEM_SPACE_DESCRIPTION,
    trace: false,
}

/** Marker interface atm */
export interface NodeFilesystemSpaceRel8ns_V1 extends IbGibRel8ns_V1 { }

/**
 * Space options involve whether we're getting/putting ibgibs categorized as
 * meta, bin, dna.
 *
 * We'll leverage the fact that we don't need to get dna very often, and that
 * meta ibgibs act differently and are recorded differently.
 *
 * For example, we don't necessarily want to keep the past of certain meta
 * objects, because it may change (and thus grow) too quickly.
 */
export interface NodeFilesystemSpaceOptionsData extends FilesystemSpaceOptionsData {
}

export interface NodeFilesystemSpaceOptionsRel8ns extends FilesystemSpaceOptionsRel8ns {
}

/** Marker interface atm */
export interface NodeFilesystemSpaceOptionsIbGib
    extends FilesystemSpaceOptionsIbGib<NodeFilesystemSpaceOptionsData, NodeFilesystemSpaceOptionsRel8ns> {
}

/** Marker interface atm */
export interface NodeFilesystemSpaceResultData extends FilesystemSpaceResultData {
}

export interface NodeFilesystemSpaceResultRel8ns extends FilesystemSpaceResultRel8ns {
}

export interface NodeFilesystemSpaceResultIbGib
    extends FilesystemSpaceResultIbGib<NodeFilesystemSpaceResultData, NodeFilesystemSpaceResultRel8ns> {
}

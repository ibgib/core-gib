import { default as pathUtils } from 'path';
import { rm } from 'node:fs/promises';

import {
    firstOfAll, ifWe, ifWeMight, iReckon,
    lastOfAll, respecfully, respecfullyDear
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;
import { delay, getUUID, } from '@ibgib/helper-gib';

import {
    testSpace_createAndInit,
    testSpace_persistTransformResult,
    testSpace_putGetDelete,
    testSpace_registerNewIbGib_GetLatest,
} from '../../../space-respec-helper.mjs';
import {
    getCurrentFilename, specGetBaseDir, specGetBaseSubPath, specGetSpaceDescription,
    specGetSpaceId, specGetSpaceName
} from '../../../../../spec-helper.node.respec.mjs';
import { NodeFilesystemSpace_V1, } from '../node-filesystem-space-v1.mjs';
import { IBGIB_ENCODING } from '../../filesystem-constants.mjs';
import { NodeFilesystemSpaceData_V1 } from '../node-filesystem-space-types.mjs';


const logalot = false;
// const lc = `[${import.meta.url}]`;
const TEST_BASE_DIR_ROOT = (await getCurrentFilename(import.meta.url)).replace('testSpace_', '').substring(0, 18) + '_troot';
const lc = `[${TEST_BASE_DIR_ROOT}]`;
if (logalot) { console.log(`${lc} TEST_BASE_DIR_ROOT: ${TEST_BASE_DIR_ROOT} (I: a482e556fbf8076e3b2c6346c4dda423)`); }


const fnGetInitialRel8ns = () => {
    return Promise.resolve(undefined);
};

const fnGetSpace = async (initialData: any, initialRel8ns: any) => {
    const space = new NodeFilesystemSpace_V1(initialData, initialRel8ns);
    if (logalot) {
        console.log(`${lc} console.dir(space)... (I: 0cbfba3b9b3d43f18a5d7b2f5286dff7)`);
        console.dir(space)
    }
    return space;
};

await respecfully(sir, `[${NodeFilesystemSpace_V1.name} specs]`, async () => {
    lastOfAll(sir, async () => {
        // rm -rf TEST_BASE_DIR_ROOT
        // comment this out if you want to examine generated ibgib data
        await rm(TEST_BASE_DIR_ROOT, {
            recursive: true,
            force: true,
        });
    });

    await testSpace_createAndInit({
        respecfulTitle: sir,
        // shortcircuitRespec: true,
        // logalot: true,
        fnGetInitialSpaceData: async () => {
            const uuid = await specGetSpaceId();
            return {
                version: '1',
                classname: NodeFilesystemSpace_V1.name,
                uuid,
                name: specGetSpaceName(uuid),
                description: specGetSpaceDescription(uuid),
                baseDir: specGetBaseDir(TEST_BASE_DIR_ROOT, uuid, logalot, lc),
                baseSubPath: specGetBaseSubPath(TEST_BASE_DIR_ROOT, uuid, logalot, lc),
                binSubPath: 'bin',
                dnaSubPath: 'dna',
                encoding: IBGIB_ENCODING,
                ibgibsSubPath: 'ibgibs',
                metaSubPath: 'meta',
                spaceSubPath: 'space',
                allowPrimitiveArgs: false,
                catchAllErrors: false,
                longPollingIntervalMs: 500,
                persistOptsAndResultIbGibs: false,
                trace: false,
                validateIbGibAddrsMatchIbGibs: false,
            } satisfies NodeFilesystemSpaceData_V1;
        },
        fnGetInitialSpaceRel8ns: fnGetInitialRel8ns,
        fnGetSpace,
    })

});

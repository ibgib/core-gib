import { IBGIB_SPACE_NAME_DEFAULT } from "../space-constants.mjs";
import { Directory, Encoding } from "./filesystem-types.mjs";

/**
 * atow this is '.' (cwd)
 *
 * In original ionic space, this would be "DOCUMENTS" or similar.
 */
export const IBGIB_BASE_DIR: Directory = '.';
/**
 * atow this is 'utf8'
 *
 * this stems from original ionic space default encoding.
 */
export const IBGIB_ENCODING: Encoding = 'utf8';
/**
 * Base directory for all data of the app.
 */
export const IBGIB_BASE_SUBPATH = 'ibgib';
/**
 * Default space that is also used for bootstrapping.
 *
 * The user should provide his/her own space name that will contain their data.
 * If a custom user space name is not provided, one should be auto-generated.
 *
 * ## notes
 *
 * * the leading 000's help to put the space earlier in alphabetized listing if viewing through an OS file viewer
 */
export const IBGIB_SPACE_SUBPATH_DEFAULT = `000_${IBGIB_SPACE_NAME_DEFAULT}`;
/**
 * Subpath for "normal" ibgibs (non-meta, non-dna, non-binary, etc.).
 */
export const IBGIB_IBGIBS_SUBPATH = 'ibgibs';
/**
 * should contain special-use ibgibs to the application.
 *
 * Use case:
 *   Because some special ibgibs will be changed frequently,
 *   e.g. settings, a separate folder will be useful.
 */
export const IBGIB_META_SUBPATH = 'meta';
/**
 * Path for storing the dna for ibgibs.
 *
 * bins will be stored without hyphens in the format:
 *   [hash].[ext]
 *
 * @example
 *   641575866a7c42bda89f58de5cd1c3aa.jpg
 */
export const IBGIB_BIN_SUBPATH = 'bin';
/**
 * Path for storing the dna for ibgibs.
 *
 * ## notes
 *
 * eventually, these should most likely be stored in "colder"
 * storage, like compressed, low-priority.
 */
export const IBGIB_DNA_SUBPATH = 'dna';
/**
 * store long paths here.
 *
 * WARNING: short name used here specifically because we're trying to mitigate
 * existing technical debt outside of the the ibgib codebase. otherwise, this
 * would be much more descriptive. please don't use non-descriptive short names
 * like this in ibgib code unless it's extremely justified.
 */
export const IBGIB_LONG_SUBPATH = 'l';

/**
 * Yep, this one starts with default instead of ending with it.
 */
export const DEFAULT_FILESYSTEM_SPACE_DESCRIPTION = `This is a filesystem space. The ibgib data contains settings for the space itself, and the witness ibgib object interfaces with a back end that is hierarchical like a filesystem. Descend from this class if you're working with a similar space.`;

/**
 * when deciding what a long path length is, this is the constant used.
 *
 * this is used in help mitigating some OSs path limits of 255.
 *
 * ## notes
 *
 * i have this well below the 255 limit on some OSs. probably too defensive.
 */
export const DEFAULT_LONG_PATH_LENGTH = 240;

/**
 * used when building paths
 */
export const DEFAULT_PATH_SEPARATOR = '/';

/**
 * want to get some more metadata past the initial atom(s) and 32 sounds like a
 * decent compromise of staying relatively short and doing this.
 */
export const ARBITRARY_IB_SUBSTRING_LENGTH_FOR_MITIGATE_LONG_PATH = 32;

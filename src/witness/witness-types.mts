import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/index.mjs';
import { IbGib, IbGibWithDataAndRel8ns, IbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';

import { ErrorIbGib_V1 } from '../common/error/error-types.mjs';

/**
 * basic witness function type.
 *
 * Note atow (11/2023) I have this defined in two places: here and in the
 * {@link Witness} declaration. That declaration does not use this type
 * signature, which maybe I should change it to do so...so many things to do
 * though.
 */
export type WitnessFn<TIbGibIn extends IbGib_V1 = IbGib_V1, TIbGibOut extends IbGib_V1 = IbGib_V1> =
    (arg: TIbGibIn) => Promise<TIbGibOut | undefined>;

/**
 * A witness is a lot like an ibGib analog for a function.
 * Now remember in FP, a function takes a single arg, and
 * multiple args are actually reified single-arg functions
 * that are curried.
 *
 * So an ibGib witness is an ibGib that has a single function: `witness`.
 * What can it witness? Another ibGib. What does it return? An ibGib.
 *
 * ## notes
 *
 * * I'm not smart enough to descend this from just IbGib, so I'm doing
 *   IbGibwithDataAndRel8ns. This is more of a convenience anyway.
 *
 * * atow (11/2023) I have this defined the witness fn shape in two places: here
 *   and in the {@link WitnessFn} declaration. This interface does not use that
 *   type signature, so there is a possibility of getting out of sync.
 */
export interface Witness<
    TIbGibIn extends IbGib_V1<any, any>,
    TIbGibOut extends IbGib_V1<any, any>,
    TData extends WitnessData_V1 = WitnessData_V1,
    TRel8ns extends WitnessRel8ns_V1 = WitnessRel8ns_V1,
> extends IbGib_V1<TData, TRel8ns> {
    witness(arg: TIbGibIn): Promise<TIbGibOut | undefined>;
}

/**
 * Convenient non-generic version of the {@link Witness} interface.
 *
 * It's not quite `any` all the way, as witness arg and results are both typed
 * to `IbGib_V1`.
 */
export interface WitnessAny extends Witness<IbGib_V1, IbGib_V1, any, any> {
}

/**
 * Data that corresponds to all witnesses being implemented in V1.
 *
 * This should be expanded only sparingly, and all properties should be optional.
 */
export interface WitnessData_V1 extends IbGibData_V1 {
    /**
     * @optional string for tracking version control of witness.
     */
    version?: string;
    /**
     * @optional Name for the witness.
     */
    name?: string;
    /**
     * @optional classname of the witness.
     */
    classname?: string;
    /**
     * @optional description of the witness.
     */
    description?: string;
    /**
     * @optional
     *
     * If true, then this will allow primitive args when validating incoming
     * args.
     *
     * so if you want a witness to be able to witness, e.g., "16816^gib", then
     * set this to truthy. Otherwise, this will flag this as a validation error.
     */
    allowPrimitiveArgs?: boolean;
    /**
     * If true, any calls to `witness` should have the opt and result ibGibs
     * persisted, regardless of what the actual opt/result is. This is
     * ultimately up to the witness itself though.
     *
     * This is like providing a persistent logging feature for the witness
     * itself, as opposed to ephemeral logging via {@link trace}.
     *
     * ## implementation
     *
     * In the case of V1, WitnessBase_V1<...> has an empty function
     */
    persistOptsAndResultIbGibs?: boolean;
    /**
     * @optional
     *
     * "Should" be a unique identifier for the witness.
     */
    uuid?: string;
    /**
     * @optional configuration for `witness` call.
     *
     * If true, then this space will not catch an error in `witnessImpl`
     * function.
     *
     * ## notes
     *
     * Descendants of Witness who don't override the base `witness` function
     * (but rather override `witnessImpl` as expected) don't need to check
     * for this explicitly, since it is referenced in the base `witness`
     * function implementation.
     */
    catchAllErrors?: boolean;
    /**
     * @optional arg for verbose logging.
     *
     * Space implementations can check this value directly, or if
     * they descend from `WitnessBase`, then there is a property
     * that safely navigates this value.
     */
    trace?: boolean;
}

/**
 * marker interface atm
 */
export interface WitnessRel8ns_V1 extends IbGibRel8ns_V1 {

}

/**
 * This interface simply types our data and rel8ns to V1 style.
 */
export interface Witness_V1<
    TDataIn extends any,
    TRel8nsIn extends IbGibRel8ns_V1,
    TIbGibIn extends IbGib_V1<TDataIn, TRel8nsIn>,
    TDataOut extends any,
    TRel8nsOut extends IbGibRel8ns_V1,
    TIbGibOut extends IbGib_V1<TDataOut, TRel8nsOut> | ErrorIbGib_V1,
    TData extends WitnessData_V1 = any,
    TRel8ns extends WitnessRel8ns_V1 = WitnessRel8ns_V1,
>
    extends Witness<TIbGibIn, TIbGibOut, TData, TRel8ns> {
}


/**
 * Base information for cmd with optional modifiers to interact with a witness.
 *
 * Note that it is not necessary for a witness to listen to these types of
 * ibgibs, this is just convenient plumbing for those who wish to listen to
 * command-style ibgibs.
 */
export interface WitnessArgData {
    /**
     * Not really in use atm, but will use in the future.
     */
    version?: string;
    /**
     * Addrs of ibgibs that this witness arg is wrapping.
     */
    ibGibAddrs?: IbGibAddr[];
}

export interface WitnessArgRel8ns extends IbGibRel8ns_V1 {
}



/**
 * Shape of an arg wrapper ibgib with a couple common-use properties in data and
 * the object class itself.
 *
 * This is meant to serve as a base class for others, most notably command arg
 * ibgibs, when using witness ibgibs.
 */
export interface WitnessArgIbGib<
    TIbGib extends IbGib,
    TArgData extends WitnessArgData,
    TArgRel8ns extends WitnessArgRel8ns,
> extends IbGib_V1<TArgData, TArgRel8ns> {
    /**
     * ibgib(s) that this arg wraps.
     *
     * So when you pass in an arg to a witness, this is used often as the
     * payload. For example, a space witness acts like a repo with puts and
     * gets command args. This will contain the references to the ibgibs being
     * put into the space.
     */
    ibGibs?: TIbGib[];
}

export interface WitnessResultData {
    /**
     * The address of the options ibGib that corresponds this space result.
     *
     * So if you called `space.witness(opts)` which produced this result, this is
     * `getIbGibAddr({ibGib: opts})`.
     *
     * Perhaps I should have this as a rel8n on the actual result ibGib instead of here
     * in the data.
     */
    optsAddr: IbGibAddr;
    /**
     * true if the operation executed successfully.
     *
     * If this is a `canGet` or `canPut` `cmd`, this does NOT indicate if you
     * can or can't. For that, see the `can` property of this interface.
     */
    success?: boolean;
    /**
     * Any error messages go here. If this is populated, then the `success`
     * *should* be false (but obvious the interface can't guarantee implementation).
     */
    errors?: string[];
    /**
     * Any warnings that don't cause this operation to explicitly fail, i.e. `errors`
     * is falsy/empty.
     */
    warnings?: string[];
    /**
     * If getting address(es), they will be here.
     */
    addrs?: IbGibAddr[];
    /**
     * Addresses for ibGibs which had errors.
     */
    addrsErrored?: IbGibAddr[];
}

export interface WitnessResultRel8ns extends IbGibRel8ns_V1 { }

/**
 * Optional witness result ibgib interface.
 *
 * You do NOT have to use this class when returning results for witness ibgibs.
 * This is provided as convenience plumbing for when you do want a standard-ish
 * special result ibgib.
 */
export interface WitnessResultIbGib<
    TIbGib extends IbGib,
    TResultData extends WitnessResultData,
    TResultRel8ns extends WitnessResultRel8ns
>
    extends IbGibWithDataAndRel8ns<TResultData, TResultRel8ns> {

    /**
     * Reference to list of result ibgibs from witness operation.
     *
     * ## notes
     *
     * * this does not have to be the only property on the concrete result
     *   ibgib. From my experience so far, this is convenient for when there is
     *   one and only one obvious result from a witness operation, like a get
     *   command that returns a list of ibgibs.
     */
    ibGibs?: TIbGib[];
}

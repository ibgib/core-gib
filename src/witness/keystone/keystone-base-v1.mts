import { IbGib_V1, IbGibRel8ns_V1, } from '@ibgib/ts-gib/dist/V1/index.mjs';

import { argy_, isArg, isCommand, resulty_ } from '../witness-helper.mjs';
import { MetaspaceService } from '../space/metaspace/metaspace-types.mjs';
import { WitnessWithContextBase_V1 } from '../witness-with-context/witness-with-context-base-v1.mjs';
import { ErrorIbGib_V1 } from '../../common/error/error-types.mjs';
import { getErrorIbGib } from '../../common/error/error-helper.mjs';

import {
    KeystoneData_V1, KeystoneRel8ns_V1, KeystoneIbGib_V1,
    KeystoneOptionsIbGib, KeystoneOptionsData, KeystoneOptionsRel8ns,
    KeystoneResultIbGib, KeystoneResultData, KeystoneResultRel8ns,
    KeystoneCmd, KeystoneCmdModifier,
    // KeystoneCmd,
    // KeystoneCmdData, KeystoneCmdRel8ns, KeystoneCmdIbGib,
    // KeystoneResultData, KeystoneResultRel8ns, KeystoneResultIbGib,
} from './keystone-types.mjs';
import { validateCommonKeystoneData } from './keystone-helper.mjs';

import { PicIbGib_V1 } from '../../common/pic/pic-types.mjs';
import { CommentIbGib_V1 } from '../../common/comment/comment-types.mjs';


import { GLOBAL_LOG_A_LOT } from '../../core-constants.mjs';
const logalot = GLOBAL_LOG_A_LOT;

/**
 * Keystone is the last major piece of the ibgib architecture. This is used to
 * implement distributed identity that gets added on-chain alongside other data
 * and metadata, instead of only relying solely upon asymmetric crypto for e.g.
 * signatures.
 *
 * Override this class with specific concrete implementations of keystones,
 * which atow I believe will differ mainly in their specific challenge
 * implementations.
 *
 * ## notes
 *
 * ### composite keystones
 *
 * At first I was thinking I was going to make keystones composite (super
 * keystones with rel8ns pointing to other keystones), but at least to start
 * with, I will have a specific concrete implementation class that covers
 * multiple keystone composition.
 */
export abstract class KeystoneBase_V1<
    TChallenges, TSolutions,
    TOptionsData extends KeystoneOptionsData,
    TOptionsRel8ns extends KeystoneOptionsRel8ns,
    TOptionsIbGib extends KeystoneOptionsIbGib<IbGib_V1, TOptionsData, TOptionsRel8ns>,
    TResultData extends KeystoneResultData,
    TResultRel8ns extends KeystoneResultRel8ns,
    TResultIbGib extends KeystoneResultIbGib<IbGib_V1, TResultData, TResultRel8ns> | ErrorIbGib_V1,
    TData extends KeystoneData_V1<TChallenges, TSolutions>,
    TRel8ns extends KeystoneRel8ns_V1,
>
    extends WitnessWithContextBase_V1<
        TOptionsData, TOptionsRel8ns, TOptionsIbGib,
        TResultData, TResultRel8ns, TResultIbGib,
        TData, TRel8ns>
    implements KeystoneIbGib_V1<TData, TRel8ns> {

    /**
     * Log context for convenience with logging. (Ignore if you don't want to use this.)
     */
    protected lc: string = `[${KeystoneBase_V1.name}]`;

    /**
     * Reference to the local ibgibs service, which is one way at getting at the
     * local user space.
     */
    ibgibsSvc: MetaspaceService | undefined;

    constructor(initialData?: TData, initialRel8ns?: TRel8ns) {
        super(initialData, initialRel8ns);
    }

    /**
     * At this point in time, the arg has already been intrinsically validated,
     * as well as the internal state of this keystone.
     */
    protected async witnessImpl(arg: TOptionsIbGib): Promise<TResultIbGib | undefined> {
        const lc = `${this.lc}[${this.witnessImpl.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }

            await this.loadNewerSelfIfAvailable();

            let result: TResultIbGib | undefined = undefined;

            if (isArg({ ibGib: (arg as IbGib_V1) })) {
                result = await this.routeAndDoArg({ arg });
            } else {
                result = await this.doNonArg({ ibGib: arg });
            }

            // if we didn't get a result, try the default.
            if (!result) {
                console.warn(`${lc} result still falsy. doing default handler. (W: 924d1fc93ad5464e81f71f2783d778a8)`);
                result = await this.doDefault({ ibGib: arg });
            }

            if (!result) { console.warn(`${lc} result falsy...Could not produce result? Was doDefault implemented in concrete class? (W: 78861cb3c37546fa8f0b2bee4c50f832)`); }

            return result;
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * Base routing executes different if incoming is a cmd options arg, i.e.,
     * if the `data.cmd` is truthy (atow). {@link isArg} is expected to be true
     * at this point. If not, logs an error, **but does not throw**, and returns
     * undefined.
     *
     * Default routing checks arg for command, or if not, checks if comment/pic.
     * If neither of those, then returns undefined atow.
     *
     * Override this function to create more advanced custom routing.
     *
     * ## notes
     *
     * In general, an app ibgib acts more like a normal application in that it
     * accepts commands and not requests. Robbots are more geared to requests,
     * handled semantically.
     *
     * @see {@link isArg}
     * @see {@link doCmdArg}
     * @see {@link doDefault}.
     */
    protected async routeAndDoArg({
        arg,
    }: {
        arg: TOptionsIbGib,
    }): Promise<TResultIbGib | undefined> {
        const lc = `${this.lc}[${this.routeAndDoArg.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            const ibGib = arg as IbGib_V1;
            if (!isArg({ ibGib })) { throw new Error(`ibGib is not an arg (E: 7e5ed6e3be45480e992c75a1c21f1b4e)`); }
            if (isCommand({ ibGib })) {
                return this.doCmdArg({ arg });
            } else {
                return undefined;
            }
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            if (this.data?.catchAllErrors) {
                return (await getErrorIbGib({ rawMsg: error.message })) as TResultIbGib;
            } else {
                throw error;
            }
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // #region do cmd args

    /**
     * By default, this routes to {@link doCmdIb}, {@link doCmdGib} & {@link
     * doCmdIbgib}. This is largely to limit scope of responsibility of app to
     * basic functions. But this is not a concrete rule written in stone.
     *
     * You can always override this and route to other commands before calling
     * this with `super.doCmdArg` as a fallback (if you still want to use this
     * function.)
     *
     * Note that this @throws an error if the data.cmd is not recognized. In this
     * implementation, this occurs if it isn't an ib/gib/ibgib _command_.
     *
     * ## commands
     *
     * ### confirm
     *
     * confirm possession of the keystone via the keystone's challenges as set
     * out in the keystone's metadata.
     *
     * ### confirm final
     *
     * final confirmation of a secret of the stone. "kills" the stone once used.
     * The point is to verify if a participant has the full secret of the stone,
     * and not just mimicked part of the stone's ownership.
     *
     * ### revoke
     *
     * revoke due to being broken.
     * maybe is a flavor of the confirm final?
     */
    protected doCmdArg({
        arg,
    }: {
        arg: TOptionsIbGib,
    }): Promise<TResultIbGib> {
        const lc = `${this.lc}[${this.doCmdArg.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            if (!arg.data?.cmd) { throw new Error(`invalid cmd arg. arg.data.cmd required. (E: 470aabf26ca6406b84dbae3b9b39639d)`); }
            if (arg.data.cmd === KeystoneCmd.ib) {
                return this.doCmdIb({ arg: arg });
            } else if (arg.data.cmd === KeystoneCmd.gib) {
                return this.doCmdGib({ arg: arg });
            } else if (arg.data.cmd === KeystoneCmd.ibgib) {
                return this.doCmdIbgib({ arg: arg });
            } else {
                throw new Error(`unknown arg.data.cmd: ${arg.data.cmd} (E: 0eb81ae8c3834f47865af0ac7b7a9fd6)`);
            }
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
    protected doCmdIb({
        arg,
    }: {
        arg: TOptionsIbGib,
    }): Promise<TResultIbGib> {
        return this.extend({ arg });
    }
    /**
     * this is the primary command handler when extending a keystone, i.e.  when
     * signing some payload.
     */
    protected abstract extend({ arg }: { arg: TOptionsIbGib }): Promise<TResultIbGib>;

    protected doCmdGib({
        arg,
    }: {
        arg: TOptionsIbGib,
    }): Promise<TResultIbGib> {
        const lc = `${this.lc}[${this.doCmdGib.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            throw new Error(`not implemented in base class (E: bb16814f46984a9dafac5b85d8ec1f74)`);
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
    protected doCmdIbgib({
        arg,
    }: {
        arg: TOptionsIbGib,
    }): Promise<TResultIbGib> {
        const lc = `${this.lc}[${this.doCmdIbgib.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            throw new Error(`not implemented in base class (E: 6f3ed691d16a43f49145b2ed46fb28ba)`);
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // #endregion do cmd args

    // #region other stubbed do functions (doPic, doComment, doDefault)

    /**
     * Stubbed in base class for convenience. Doesn't have to be implemented.
     *
     * in the future, this may be able to be used for physical object
     * verification via picture.
     */
    protected doPic({
        ibGib,
    }: {
        ibGib: PicIbGib_V1,
    }): Promise<TResultIbGib | undefined> {
        const lc = `${this.lc}[${this.doPic.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            throw new Error(`not implemented in base class (E: ce5c9b30622f4b3bab198dde04de75e7)`);
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * Stubbed in base class for convenience. Doesn't have to be implemented.
     *
     * in the future, this may be able to handle natural language and route
     * accordingly.
     */
    protected doComment({
        ibGib,
    }: {
        ibGib: CommentIbGib_V1,
    }): Promise<TResultIbGib | undefined> {
        const lc = `${this.lc}[${this.doComment.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }

            throw new Error(`not implemented in base class (E: c90f5bddaa58446fb4de638816974b6c)`);
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    protected doNonArg({
        ibGib,
    }: {
        ibGib: TOptionsIbGib,
    }): Promise<TResultIbGib | undefined> {
        const lc = `${this.lc}[${this.doNonArg.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            return this.doDefault({ ibGib });
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
    /**
     * Stubbed in base class for convenience. Doesn't have to be implemented.
     */
    protected doDefault({
        ibGib,
    }: {
        ibGib: TOptionsIbGib,
    }): Promise<TResultIbGib | undefined> {
        const lc = `${this.lc}[${this.doDefault.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            throw new Error(`not implemented in base class (E: bee031bb4c974a71bae82df7cff2b4e1)`);
            // return this.doDefaultImpl({ibGib});
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    // #endregion other stubbed do functions (doPic, doComment, doDefault)

    /**
     * validates against common app qualities.
     *
     * Override this with a call to `super.validateThis` for custom validation
     * for descending app classes.
     *
     * @returns validation errors common to all apps, if any errors exist.
     */
    protected async validateThis(): Promise<string[]> {
        const lc = `${this.lc}[${this.validateThis.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            if (!this.data) { throw new Error(`(UNEXPECTED) this.data falsy? (E: bd4b0fc836774eb288b8b07ec79cb4af)`); }
            const errors = [
                // ...await super.validateThis(),
                ...validateCommonKeystoneData({ appData: this.data }),
            ];
            return errors;
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * builds an arg ibGib.
     *
     * wrapper convenience to avoid long generic calls.
     */
    async argy<
        TOptionsData extends KeystoneOptionsData = KeystoneOptionsData,
        TOptionsRel8ns extends KeystoneOptionsRel8ns = KeystoneOptionsRel8ns,
        TOptionsIbGib extends KeystoneOptionsIbGib<IbGib_V1, TOptionsData, TOptionsRel8ns> =
        KeystoneOptionsIbGib<IbGib_V1, TOptionsData, TOptionsRel8ns>
    >({
        argData,
        ibMetadata,
        noTimestamp,
        ibGibs,
    }: {
        argData: TOptionsData,
        ibMetadata?: string,
        noTimestamp?: boolean,
        ibGibs?: IbGib_V1[],
    }): Promise<TOptionsIbGib> {
        const arg = await argy_<TOptionsData, TOptionsRel8ns, TOptionsIbGib>({
            argData,
            ibMetadata,
            noTimestamp
        });

        if (ibGibs) { arg.ibGibs = ibGibs; }

        return arg;
    }

    /**
     * builds a result ibGib, if indeed a result ibgib is required.
     *
     * This is only useful in apps that have more structured inputs/outputs.
     * For those that simply accept any ibgib incoming and return a
     * primitive like ib^gib or whatever, then this is unnecessary.
     *
     * wrapper convenience to avoid long generic calls.
     */
    async resulty<
        TResultData extends KeystoneResultData = KeystoneResultData,
        TResultRel8ns extends KeystoneResultRel8ns = KeystoneResultRel8ns,
        TResultIbGib extends KeystoneResultIbGib<IbGib_V1, TResultData, TResultRel8ns> =
        KeystoneResultIbGib<IbGib_V1, TResultData, TResultRel8ns>
    >({
        resultData,
        ibGibs,
    }: {
        resultData: TResultData,
        ibGibs?: IbGib_V1[],
    }): Promise<TResultIbGib> {
        const result = await resulty_<TResultData, TResultIbGib>({
            // ibMetadata: getKeystoneResultMetadata({space: this}),
            resultData,
        });
        if (ibGibs) { result.ibGibs = ibGibs; }
        return result;
    }

}

/**
 * useful for when passing around instances of keystones where implementation
 * details are not important.
 */
export interface IbGibKeystoneAny
    extends KeystoneBase_V1<any, any, any, any, any, any, any, any, any, any> {
}

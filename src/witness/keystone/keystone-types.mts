/**
 * @module keystone-types
 */

import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { IbGib, IbGibAddr, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';

import { WitnessWithContextData_V1, WitnessWithContextRel8ns_V1 } from '../witness-with-context/witness-with-context-types.mjs';
import { WitnessCmdData, WitnessCmdIbGib, WitnessCmdRel8ns } from '../witness-cmd/witness-cmd-types.mjs';
import { WitnessResultData, WitnessResultIbGib, WitnessResultRel8ns } from '../witness-types.mjs';
// import { IbGibSpaceOptionsData } from '@ibgib/core-gib/dist/witness/space/space-types.mjs';

import { GLOBAL_LOG_A_LOT } from '../../core-constants.mjs';
import { IbGibSpaceAny } from '../space/space-base-v1.mjs';
import { MetaspaceService } from '../space/metaspace/metaspace-types.mjs';
import { IbGibKeystoneAny } from './keystone-base-v1.mjs';
/**
 * use for verbose logging.
 */
const logalot = GLOBAL_LOG_A_LOT;

export type KeystoneScope = "any" | "transform";
export type KeystoneSubScope = "any" | "";

export type ExpirationFormat = "UTC";

export interface KeystoneData_V1<TChallenges, TSolutions> extends WitnessWithContextData_V1 {
    /**
     * @optional setting, which if set, this keystone has internal knowledge of
     * its use.
     */
    scope?: string;
    /**
     *
     */
    tjpGib?: string;
    /**
     * @optional formatting of expiration timestamp
     */
    expirationFormat?: ExpirationFormat,
    /**
     * @optional timestamp at which time the keystone will be invalid.
     */
    expiration?: string;
    /**
     * If truthy, then there is a limited number of uses that the keystone can
     * be utilized (challenged/mutated).
     *
     * # intent
     *
     * temporary keystones could be used for finite, pre-known numbers of
     * "transactions".
     */
    remainingUses?: number;
    /**
     * atom corresponding to the type of challenge.
     *
     * @example 'zkp'
     * @example 'math'
     * @example 'honor'
     */
    challengeAtom: string;
    challenges: TChallenges;
    solutions: TSolutions;
}

export interface KeystoneRel8ns_V1 extends WitnessWithContextRel8ns_V1 {
}

/**
 * Base of the ibgib data for any keystone.
 *
 * concrete implementations of keystones should add customized data.
 */
export interface KeystoneIbGib_V1<KeystoneData_V1 extends IbGibData_V1, KeystoneRel8ns_V1 extends IbGibRel8ns_V1>
    extends IbGib_V1<KeystoneData_V1, KeystoneRel8ns_V1> {

}

/**
 * Cmds for interacting with ibgib spaces.
 *
 * Not all of these will be implemented for every space.
 *
 * ## todo
 *
 * change these commands to better structure, e.g., verb/do/mod, can/get/addrs
 * */
export type KeystoneCmd =
    'challenge' | 'revoke';
/** Cmds for interacting with ibgib spaces.  */
export const KeystoneCmd = {
    /**
     * it's more like a grunt that is intepreted by context from the robbot.
     *
     * my initial use of this will be to extend a keystone.
     */
    ib: 'ib' as KeystoneCmd,
    /**
     * it's more like a grunt that is intepreted by context from the robbot.
     *
     * my initial use of this will be to validate a keystone.
     */
    gib: 'gib' as KeystoneCmd,
    /**
     * third placeholder command.
     *
     * my initial use of this will be to finalize a keystone.
     */
    ibgib: 'ibgib' as KeystoneCmd,
}

/*o
 * Flags to affect the command's interpretation.
 */
export type KeystoneCmdModifier =
    'final';
/**
 * Flags to affect the command's interpretation.
 */
export const KeystoneCmdModifier = {
    /**
     * If final, then the cmd will be the last of the stone.
     *
     * ## notes
     *
     * I think this may just signal the beginning of the end, there may actually
     * technically be more commands executed by the witness. but they would be
     * cleanup commands. the point is that after this option
     */
    final: 'final' as KeystoneCmdModifier,
}

/**
 * Information commonly required for keystones.
 */
export interface KeystoneOptionsData extends WitnessCmdData<KeystoneCmd, KeystoneCmdModifier> {
    spaceId?: string;
    spaceAddr?: string;
}

export interface KeystoneOptionsRel8ns extends WitnessCmdRel8ns {
}

export interface KeystoneOptionsIbGib<
    TIbGib extends IbGib = IbGib_V1,
    TOptsData extends KeystoneOptionsData = KeystoneOptionsData,
    TOptsRel8ns extends KeystoneOptionsRel8ns = KeystoneOptionsRel8ns,
> extends WitnessCmdIbGib<TIbGib, KeystoneCmd, KeystoneCmdModifier, TOptsData, TOptsRel8ns> {
    space: IbGibSpaceAny;
    metaspace: MetaspaceService;
}

/**
 * Optional shape of result data to witness interactions.
 *
 * This is in addition of course to {@link WitnessResultData}.
 *
 * ## notes
 *
 * * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface KeystoneResultData extends WitnessResultData {
    /**
     * redeclared from {@link WitnessResultData} for explicit focus. (i.e. not
     * really necessary but it makes it more obvious that this is used.)
     *
     * ## when extending keystone
     *
     * new addrs added to the keystone dependency graph will be here.
     */
    addrs?: IbGibAddr[];
}

/**
 * Marker interface rel8ns atm...
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface KeystoneResultRel8ns extends WitnessResultRel8ns { }

/**
 * Shape of result ibgib if used for a robbot.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface KeystoneResultIbGib<
    TIbGib extends IbGib_V1,
    TResultData extends KeystoneResultData,
    TResultRel8ns extends KeystoneResultRel8ns
>
    extends WitnessResultIbGib<TIbGib, TResultData, TResultRel8ns> {
    /**
     * In addition to the {@link WitnessResultIbGib} `ibGibs` property, this is
     * provided for convenience which should differentiate between the most
     * recent (newly created) keystone ibgib and the intermediate ibgibs created
     * (e.g. dna).
     */
    resTransform: TransformResult<IbGibKeystoneAny>;
}

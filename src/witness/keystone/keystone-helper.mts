import { GLOBAL_LOG_A_LOT } from "../../core-constants.mjs";
import { KeystoneData_V1 } from "./keystone-types.mjs";

const logalot = GLOBAL_LOG_A_LOT || false;

/**
 * validation of data common to all keystones (i.e. at the base class level).
 * @returns error strings array of any validation errors. empty if none
 */
export function validateCommonKeystoneData({
    appData,
}: {
    appData: KeystoneData_V1<any, any> | undefined,
}): string[] {
    const lc = `[${validateCommonKeystoneData.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting...`); }
        if (!appData) { throw new Error(`appData required (E: e22bd1940d1d436fbbed8126c11aa529)`); }
        const errors: string[] = [];

        console.warn(`${lc} not impl yet`)
        // const {
        //     name, uuid, classname,
        // } =
        //     appData;

        // if (name) {
        //     if (!name.match(APP_NAME_REGEXP)) {
        //         errors.push(`name must match regexp: ${APP_NAME_REGEXP}`);
        //     }
        // } else {
        //     errors.push(`name required.`);
        // }

        // if (uuid) {
        //     if (!uuid.match(UUID_REGEXP)) {
        //         errors.push(`uuid must match regexp: ${UUID_REGEXP}`);
        //     }
        // } else {
        //     errors.push(`uuid required.`);
        // }

        // if (classname) {
        //     if (!classname.match(APP_NAME_REGEXP)) {
        //         errors.push(`classname must match regexp: ${APP_NAME_REGEXP}`);
        //     }
        // }

        return errors;
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

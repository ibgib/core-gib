import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear,
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import { delay, extractErrorMsg, getTimestampInTicks, getUUID } from '@ibgib/helper-gib';
import { Gib } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';

import { KeystoneData_V1, KeystoneIbGib_V1, KeystoneRel8ns_V1 } from '../keystone-types.mjs';
import { IbGibKeystoneAny } from '../keystone-base-v1.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../core-constants.mjs';
import { WitnessWithContextBase_V1 } from '../../witness-with-context/witness-with-context-base-v1.mjs';

const logalot: boolean | number = GLOBAL_LOG_A_LOT;

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;

// #region math mock/test helper functions/types

interface MathKeystoneData_V1 extends KeystoneData_V1<any, any> { }
interface MathKeystoneRel8ns_V1 extends KeystoneRel8ns_V1 { }
/**
 * this keystone has math problems as its challenges
 */
interface MathKeystoneIbGib_V1 extends KeystoneIbGib_V1<MathKeystoneData_V1, MathKeystoneRel8ns_V1> {
}
type MathType = "arithmetic";
interface MathKeystoneAddlMetadata {
    atom: 'keystone_math';
    mathType: MathType;
    tjpGib: Gib;
}
export async function createKeystone(): Promise<IbGibKeystoneAny> {
    const lc = `[${createKeystone.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: ba9c092ad0b19a7fd1c5d98fb27b5923)`); }

        throw new Error(`not impl (E: db37bc8b249841397316812a006bc223)`);

    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 *
 * @returns validation error if any, else returns null
 */
export async function validateKeystone({
    keystoneLatest,
}: {
    /**
     * the latest (i.e. "head") keystone in the keystone's timeline.
     */
    keystoneLatest?: IbGibKeystoneAny,
    /**
     * keystones are a graph, and the idea is
     */
    dependencyGraph?: { [addr: string]: IbGib_V1 },
}): Promise<string[] | null> {
    const lc = `[${validateKeystone.name}]`;
    try {
        throw new Error(`not imple yet (E: 2a80e8b2e6934b10a27df67395402c29)`);
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    }
}

// #endregion math mock/test helper functions/types

/**
 * witness for keystones that have math problems as the challenges.
 */
class MathKeystoneWitness_V1
    extends WitnessWithContextBase_V1<
        any, any, any, // options
        any, any, any, // result
        MathKeystoneData_V1, MathKeystoneRel8ns_V1> {

    protected parseAddlMetadataString<TParseResult>({ ib }: { ib: string; }): TParseResult {
        // const addlMetadataText = `${atom}_${classnameIsh}_${nameIsh}_${idIsh}`;
        if (!ib) { throw new Error(`ib required (E: f3883d3ce90c4c15907dffa1c46e77d6)`); }
        const lc = `[${this.parseAddlMetadataString.name}]`;
        try {
            const [atom, classnameIsh, nameIsh, idIsh] = ib.split('_');
            const result = { atom, classnameIsh, nameIsh, idIsh, } as unknown as MathKeystoneAddlMetadata;
            return result as TParseResult; // i'm not liking the TParseResult...hmm
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        }
    }
    protected witnessImpl(arg: any): Promise<any> {
        throw new Error('Method not implemented.');
    }

}

await respecfully(maam, lcFile, async () => {
    let lc = lcFile.concat();

    await respecfully(maam, `simple keystones (not composite)`, async () => {

        await ifWe(maam, `createKeystone`, async () => {

        });
        await ifWe(maam, `validateKeystone`, async () => { });
        await ifWe(maam, `iterateKeystone`, async () => { });
        await ifWe(maam, `createKeystoneChallenge`, async () => { });
        await ifWe(maam, `createKeystoneChallenge`, async () => { });

    });

}, { logalot: !!logalot });

import { pickRandom } from "@ibgib/helper-gib";

import {
    MathChallengeInfo, MathChallengeType, MathChallengeInfo_Add
} from "./math-keystone-types.mjs";

import { GLOBAL_LOG_A_LOT } from '../../../core-constants.mjs';
/**
 * use for verbose logging.
 */
const logalot = GLOBAL_LOG_A_LOT;

/**
 * creates a new simple math challenge. only does 'add' atow.
 *
 * @returns math challenge info
 */
export async function getMathChallenge({
    challengeType,
}: {
    /**
     * If provided, will create this type of challenge.
     */
    challengeType?: MathChallengeType,
}): Promise<MathChallengeInfo> {
    const lc = `[${getMathChallenge.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 75a805603968ca8a4b076caa2cf9bd23)`); }
        if (challengeType && challengeType !== 'add') { throw new Error(`only 'add' implemented atow (E: 1e5bc2395395b8d48d61135b63a77923)`); }

        // add
        const x = Math.ceil(Math.random() * 999999);
        const y = Math.ceil(Math.random() * 999999);
        const challenge: MathChallengeInfo_Add = {
            id: 'hmm',
            op: 'add',
            a: x,
            b: y,
        }
        return challenge;
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

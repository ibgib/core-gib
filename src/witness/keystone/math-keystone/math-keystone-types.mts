/**
 * @module math-keystone-types
 */

import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { IbGib } from '@ibgib/ts-gib/dist/types.mjs';

import {
    KeystoneData_V1, KeystoneRel8ns_V1,
    KeystoneOptionsData, KeystoneOptionsRel8ns, KeystoneOptionsIbGib,
    KeystoneResultData, KeystoneResultRel8ns, KeystoneResultIbGib,
} from '../keystone-types.mjs';


import { GLOBAL_LOG_A_LOT } from '../../../core-constants.mjs';
/**
 * use for verbose logging.
 */
const logalot = GLOBAL_LOG_A_LOT;

export interface MathKeystoneData_V1 extends KeystoneData_V1<MathChallengeInfo[], MathSolutionInfo[]> {
    challengeAtom: 'math';
}

export interface MathKeystoneRel8ns_V1 extends KeystoneRel8ns_V1 {
}

/**
 * needed? yagni?
 */
export interface MathKeystoneAddlMetadata {
    // atom: string;
    // classnameIsh: string;
    // nameIsh: string;
    // idIsh: string;
}

/**
 * keystone for math problem challenges.
 *
 * I'm using this largely as a test implementation, which both gives me a chance
 * to futz around and learn, as well as help corroborate that unit tests are
 * properly scoped.
 */
export interface MathKeystoneIbGib_V1<MathKeystoneData_V1 extends IbGibData_V1, MathKeystoneRel8ns_V1 extends IbGibRel8ns_V1>
    extends IbGib_V1<MathKeystoneData_V1, MathKeystoneRel8ns_V1> {

}

export interface MathKeystoneOptionsData extends KeystoneOptionsData {
}

export interface MathKeystoneOptionsRel8ns extends KeystoneOptionsRel8ns {
}

export interface MathKeystoneOptionsIbGib extends KeystoneOptionsIbGib {
}

/**
 * @see {@link WitnessResultData}
 */
export interface MathKeystoneResultData extends KeystoneResultData {
}

/**
 */
export interface MathKeystoneResultRel8ns extends KeystoneResultRel8ns { }

/**
 * Shape of result ibgib if used for a robbot.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface MathKeystoneResultIbGib
    extends KeystoneResultIbGib<IbGib_V1, MathKeystoneResultData, MathKeystoneResultRel8ns> {
}

export type MathChallengeOperation = 'add' | 'subtract';

/**
 * info about our (simple atow) math challenge.
 */
export interface MathChallengeInfo {
    /**
     * operation to be performed on number's `a` and `b`.
     */
    op: MathChallengeOperation;
    /**
     * id of the challenge
     */
    id: string;
    /**
     * left hand side of math challenge expression.
     */
    a: number;
    /**
     * right hande side of math challenge expression.
     */
    b: number;
}

export interface MathChallengeInfo_Add extends MathChallengeInfo {
}

/**
 * info about our (simple atow) math challenge's solution.
 */
export interface MathSolutionInfo {
    challengeId: string;
    solution: number;
}

export interface MathChallengeType {

}

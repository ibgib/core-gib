import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";
import { WitnessWithContextBase_V1 } from "../../witness-with-context/witness-with-context-base-v1.mjs";
import {
    MathKeystoneOptionsData, MathKeystoneOptionsIbGib, MathKeystoneOptionsRel8ns,
    MathKeystoneResultData, MathKeystoneResultIbGib, MathKeystoneResultRel8ns,
    MathKeystoneData_V1, MathKeystoneRel8ns_V1, MathKeystoneIbGib_V1,
    MathKeystoneAddlMetadata,
    MathChallengeInfo, MathSolutionInfo,
} from "./math-keystone-types.mjs";
import { KeystoneBase_V1 } from "../keystone-base-v1.mjs";

import { GLOBAL_LOG_A_LOT } from '../../../core-constants.mjs';
const logalot = GLOBAL_LOG_A_LOT;

/**
 * witness for keystones that have math problems as the challenges.
 *
 * @see {@link MathKeystoneIbGib_V1}
 */
export class MathKeystoneWitness_V1 extends KeystoneBase_V1<
    MathChallengeInfo[], MathSolutionInfo[],
    MathKeystoneOptionsData, MathKeystoneOptionsRel8ns, MathKeystoneOptionsIbGib,
    MathKeystoneResultData, MathKeystoneResultRel8ns, MathKeystoneResultIbGib,
    MathKeystoneData_V1, MathKeystoneRel8ns_V1> {

    protected parseAddlMetadataString<TParseResult>({ ib }: { ib: string; }): TParseResult {
        // const addlMetadataText = `${atom}_${classnameIsh}_${nameIsh}_${idIsh}`;
        if (!ib) { throw new Error(`ib required (E: f3883d3ce90c4c15907dffa1c46e77d6)`); }
        const lc = `[${this.parseAddlMetadataString.name}]`;
        try {
            const [atom, classnameIsh, nameIsh, idIsh] = ib.split('_');
            const result = { atom, classnameIsh, nameIsh, idIsh, } as MathKeystoneAddlMetadata;
            return result as TParseResult; // i'm not liking the TParseResult...hmm
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        }
    }

    /**
     * this is the primary command handler when extending a keystone, i.e.  when
     * signing some payload.
     */
    protected async extend({
        arg,
    }: {
        arg: MathKeystoneOptionsIbGib,
    }): Promise<MathKeystoneResultIbGib> {
        const lc = `${this.lc}[${this.extend.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting...`); }
            // 1 .look at the specs for the keystone
            // 2. determine requirements for keystone extension
            //   * for now, this is just the default (i.e. we skip these steps)

            let nextChallenges: MathChallengeInfo[] = await this.getNextChallenges({ arg });
            // get the next challenges to solve
            // prompt user (if required) solve challenges
            let nextSolutions: MathSolutionInfo[] = await this.getNextSolutions({ arg, nextChallenges });

            throw new Error(`not impl (E: 470834fb2562dd33b49b8ec6c9be9423)`);

            // validate nextSolutions
            // await this.validateNextSolutions({ arg, nextChallenges, nextSolutions });

            // perform keystone graph extension
            // now that we have solutions

        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    /**
     * chooses the next challenge infos in order to extend the keystone
     * timeline.
     */
    async getNextChallenges({
        arg,
    }: {
        arg: MathKeystoneOptionsIbGib,
    }): Promise<MathChallengeInfo[]> {
        const lc = `${this.lc}[${this.getNextChallenges.name}]`;
        try {

            if (logalot) { console.log(`${lc} starting... (I: e5cf6a884a093eddc1f297b2a63dd123)`); }
            throw new Error(`not impl (E: 602bdd6bfa38db5acbd798e60c861623)`);

        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    async getNextSolutions({
        arg,
        nextChallenges,
    }: {
        arg: MathKeystoneOptionsIbGib,
        nextChallenges: MathChallengeInfo[],
    }): Promise<MathSolutionInfo[]> {
        const lc = `${this.lc}[${this.getNextSolutions.name}]`;
        try {
            if (logalot) { console.log(`${lc} starting... (I: f77696f021a32ec474de3ebc2c7d7823)`); }

            throw new Error(`not impl (E: d727a9e378da4ddcb90d0be3a995daeb)`);
        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }

    async validateNextSolutions({
        arg,
    }: {
        arg: MathKeystoneOptionsIbGib,
    }): Promise<string[]> {
        const lc = `${this.lc}[${this.validateNextSolutions.name}]`;
        let errors: string[] = [];
        try {
            if (logalot) { console.log(`${lc} starting... (I: 49fda11783ede527b64bf20e8a9feb23)`); }

            throw new Error(`not impl (E: 5407f3d0b3d9729ee361a64cd411ea23)`);

        } catch (error) {
            console.error(`${lc} ${error.message}`);
            throw error;
        } finally {
            if (logalot) { console.log(`${lc} complete.`); }
        }
    }
}

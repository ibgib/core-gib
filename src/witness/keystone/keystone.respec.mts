/**
 * @module keystone.respec.mts
 *
 * @see {@link './math-keystone/math-keystone.respec.mts'}
 *
 * In order to test the keystone at the base class, you have to create a mock.
 * I've gone ahead and made a simplistic keystone at ./math-keystone which acts
 * as this base keystone test.
 */

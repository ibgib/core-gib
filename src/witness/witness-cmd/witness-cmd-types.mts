import { IbGibRel8ns_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";
import { WitnessArgData, WitnessArgIbGib } from "../witness-types.mjs";
import { IbGib } from "@ibgib/ts-gib/dist/types.mjs";

/**
 * Base information for cmd with optional modifiers to interact with a witness.
 *
 * Note that it is not necessary for a witness to listen to these types of
 * ibgibs, this is just convenient plumbing for those who wish to listen to
 * command-style ibgibs.
 */
export interface WitnessCmdData<TCmds, TCmdModifiers> extends WitnessArgData {
    /**
     * The `cmd` property is the name of the command, analogous to a function
     * name.
     */
    cmd: TCmds | string;
    /**
     * Optional modifier flag(s) to the command.
     *
     * ## notes
     *
     * An implementing class can always use/extend these or extend the interface
     * of the options data.
     */
    cmdModifiers?: (TCmdModifiers | string)[];
}

export interface WitnessCmdRel8ns extends IbGibRel8ns_V1 {
}

export interface WitnessCmdIbGib<
    TIbGib extends IbGib,
    TCmds, TCmdModifiers,
    TCmdData extends WitnessCmdData<TCmds, TCmdModifiers>,
    TCmdRel8ns extends WitnessCmdRel8ns,
> extends WitnessArgIbGib<TIbGib, TCmdData, TCmdRel8ns> {
}

/**
 * default if for some reason there is no extension in data.ext
 *
 * ## driving itnent
 *
 * implementing download pic
 */
export const DEFAULT_PIC_FILE_EXTENSION = 'png';

/**
 * todo: resolve pic/bin separation. BINARY_REL8N_NAME
 */
export const BINARY_REL8N_NAME = 'bin';
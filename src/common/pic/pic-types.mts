import { IbGibAddr, TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/index.mjs';
import { BinIbGib_V1 } from '../bin/bin-types.mjs'; // js-doc reference
import { BINARY_REL8N_NAME } from './pic-constants.mjs';



/**
 * Data for a pic ibGib
 *
 * @see {@link BinIbGib_V1}
 */
export interface PicData_V1 {
    binHash: string;
    binHashThumb?: string;
    ext?: string;
    filename: string;
    timestamp: string;
}

export interface PicRel8ns_V1 extends IbGibRel8ns_V1 {
    [BINARY_REL8N_NAME]: IbGibAddr[];
}

export interface PicIbGib_V1 extends IbGib_V1<PicData_V1, PicRel8ns_V1> {

}

export type UpdatePicPromptResult = [TransformResult<PicIbGib_V1>, TransformResult<BinIbGib_V1>];

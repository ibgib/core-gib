import { TransformResult } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/index.mjs';

/**
 * Data for a comment ibGib
 */
export interface CommentData_V1 extends IbGibData_V1 {
    text: string;
    textTimestamp?: string;
    timestamp?: string;
}

export interface CommentRel8ns_V1 extends IbGibRel8ns_V1 {
}

export interface CommentIbGib_V1 extends IbGib_V1<CommentData_V1, CommentRel8ns_V1> {
}

export type UpdateCommentPromptResult = TransformResult<CommentIbGib_V1>;

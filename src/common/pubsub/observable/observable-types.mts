/**
 * @module observable types (and some enums/constants)
 */

import { IbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { WitnessWithContextData_V1, WitnessWithContextRel8ns_V1 } from '../../../witness/witness-with-context/witness-with-context-types.mjs';
import { WitnessCmdData, WitnessCmdIbGib, WitnessCmdRel8ns } from '../../../witness/witness-cmd/witness-cmd-types.mjs';
import { Witness, WitnessAny, WitnessFn, WitnessResultData, WitnessResultIbGib, WitnessResultRel8ns } from '../../../witness/witness-types.mjs';
import { DEFAULT_DESCRIPTION_OBSERVABLE, DEFAULT_NAME_OBSERVABLE, DEFAULT_UUID_OBSERVABLE } from './observable-constants.mjs';
import { SubscriptionIbGib_V1, SubscriptionWitness } from '../subscription/subscription-types.mjs';
import { Subscription_V1 } from '../subscription/subscription-v1.mjs';
import { ErrorIbGib_V1 } from '../../error/error-types.mjs';
import { ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { ObserverWitness } from '../observer/observer-types.mjs';
// and just to show where these things are
// import { CommentIbGib_V1 } from "@ibgib/core-gib/dist/common/comment/comment-types.mjs";
// import { MetaspaceService } from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// /**
//  * @see {@link SomeEnum}
//  */
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// /**
//  * values of {@link SomeEnum}
//  */
// export const SOME_ENUM_VALUES: SomeEnum[] = Object.values(SomeEnum);

/**
 * ibgib's intrinsic data goes here.
 *
 * @see {@link IbGib_V1.data}
 */
export interface ObservableData_V1 extends WitnessWithContextData_V1 {
    // ibgib data (settings/values/etc) goes here
    // /**
    //  * docs yo
    //  */
    // setting: string;
    /**
     * if true, will replay previous events when subscribed.
     */
    replay?: boolean;
}

/**
 * rel8ns (named edges/links in DAG) go here.
 *
 * @see {@link IbGib_V1.rel8ns}
 */
export interface ObservableRel8ns_V1 extends WitnessWithContextRel8ns_V1 {
    // /**
    //  * required rel8n. for most, put in observable-constants.mts file
    //  *
    //  * @see {@link REQUIRED_REL8N_NAME}
    //  */
    // [REQUIRED_REL8N_NAME]: IbGibAddr[];
    // /**
    //  * optional rel8n. for most, put in observable-constants.mts file
    //  *
    //  * @see {@link OPTIONAL_REL8N_NAME}
    //  */
    // [OPTIONAL_REL8N_NAME]?: IbGibAddr[];
}

/**
 * this is the ibgib object itself.
 *
 * If this is a plain ibgib data only object, this acts as a dto. You may also
 * want to generate a witness ibgib, which is slightly different, for ibgibs
 * that will have behavior (i.e. methods).
 *
 * @see {@link ObservableData_V1}
 * @see {@link ObservableRel8ns_V1}
 */
export interface ObservableIbGib_V1 extends IbGib_V1<ObservableData_V1, ObservableRel8ns_V1> {

}

/**
 * Cmds for interacting with ibgib witnesses.

 * @see const {@link ObservableCmd} for individual jsdocs
 */
export type ObservableCmd =
    'subscribe';
/** Cmds for interacting with ibgib spaces. */
export const ObservableCmd = {
    /**
     * subscribe to the observable
     */
    subscribe: 'subscribe' as ObservableCmd,
}
export const OBSERVABLE_CMD_VALUES: ObservableCmd[] = Object.values(ObservableCmd);

/**
 * Flags to affect the command's interpretation.
 *
 * atow 11/2023 not used
 */
export type ObservableCmdModifier =
    'ib' | 'gib' | 'ibgib';
/**
 * Flags to affect the command's interpretation.
 *
 * atow 11/2023 not used
 */
export const ObservableCmdModifier = {
    /**
     * hmm...
     */
    ib: 'ib' as ObservableCmdModifier,
    /**
     * hmm...
     */
    gib: 'gib' as ObservableCmdModifier,
    /**
     * hmm...
     */
    ibgib: 'ibgib' as ObservableCmdModifier,
}

/** Information for interacting with spaces. */
export interface ObservableCmdData
    extends WitnessCmdData<ObservableCmd, ObservableCmdModifier> {
}

export interface ObservableCmdRel8ns extends WitnessCmdRel8ns {
}

/**
 * Shape of options ibgib if used for a command-based witness.
 */
export interface ObservableCmdIbGib
    extends WitnessCmdIbGib<
        IbGib_V1,
        ObservableCmd, ObservableCmdModifier,
        ObservableCmdData, ObservableCmdRel8ns
    > {
}

/**
 * Optional shape of result data to witness interactions.
 *
 * This is in addition of course to {@link ObservableResultData}.
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * ## notes
 *
 * * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface ObservableResultData extends WitnessResultData {
}

/**
 * Marker interface rel8ns atm...
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface ObservableResultRel8ns extends WitnessResultRel8ns { }

/**
 * Shape of result ibgib if used for a witness.
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface ObservableResultIbGib
    extends WitnessResultIbGib<IbGib_V1, ObservableResultData, ObservableResultRel8ns> {
}

/**
 * shape of underscore-delimited addl metadata string that may be present in the
 * ib (i.e. available when parsing the ib)
 *
 * This is not hard and fast and can (and should?) vary greatly per use case.
 */
export interface ObservableAddlMetadata {
    /**
     * should be observable
     */
    atom?: 'observable'
    /**
     * classname of observable **with any underscores removed**.
     */
    classnameIsh?: string;
    /**
     * name of observable witness (data.name) **with any underscores removed**.
     */
    nameIsh?: string;
    /**
     * id of witness (data.uuid) **with any underscores removed**.
     *
     * may be a substring per use case...?
     */
    idIsh?: string;
}

/**
 * Default data values for a Observable.
 *
 * If you change this, please bump the version
 *
 * (but of course won't be the end of the world when this doesn't happen).
 */
export const DEFAULT_OBSERVABLE_DATA_V1: ObservableData_V1 = {
    version: '1',
    uuid: DEFAULT_UUID_OBSERVABLE,
    name: DEFAULT_NAME_OBSERVABLE,
    description: DEFAULT_DESCRIPTION_OBSERVABLE,
    classname: `Observable_V1`,

    /**
     * if true, then the witness will attempt to persist ALL calls to
     * `witness.witness(...)`.
     */
    persistOptsAndResultIbGibs: false,
    /**
     * allow ibgibs like 42^gib ({ib: 42, gib: 'gib'} with `data` and `rel8ns` undefined)
     */
    allowPrimitiveArgs: true,
    /**
     * witnesses should be guaranteed not to throw uncaught exceptions.
     */
    catchAllErrors: false,
    /**
     * if true, would enable logging of all calls to `witness.witness(...)`
     */
    trace: false,

    // put in your custom defaults here
}
export const DEFAULT_OBSERVABLE_REL8NS_V1: ObservableRel8ns_V1 | undefined = undefined;

/**
 * v1 observABLE witness shape
 *
 * this is the "future array" that is being observed. it outputs a stream of
 * ibgibs, and can error and complete.
 *
 * @see {@link ObserverWitness}
 */
export interface ObservableWitness<
    TIbGibIn_ie_Payload extends IbGib_V1 = IbGib_V1,
    TIbGibOut_ie_ProbablyDontCare extends IbGib_V1 = IbGib_V1
> extends WitnessAny {
    get isCompleteOrErrored(): boolean;
    get isComplete(): boolean;
    get isErrored(): boolean;
    /**
     * primary function to subscribe to new
     * @param witness ibgib to fire off when a new ibgib come downs the obsesrvable's pipe
     */
    subscribe(
        observer: WitnessAny | ObserverWitness<TIbGibIn_ie_Payload>
    ): Promise<SubscriptionWitness>;
}

export interface ObservableWitnessAny extends ObservableWitness<any, any> { }

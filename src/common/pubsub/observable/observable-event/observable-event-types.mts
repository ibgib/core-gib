/**
 * @module observable-event-types (and some enums/constants)
 */

import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";
import { IbGibAddr } from "@ibgib/ts-gib/dist/types.mjs";
import { ObservableWitnessAny } from "../observable-types.mjs";

export type ObservableEventType = 'next' | 'error' | 'complete';

/**
 * ibgib's intrinsic data goes here.
 *
 * @see {@link IbGib_V1.data}
 * @see {@link IbGibData_V1}
 * @see {@link ObservableEventIbGib_V1}
 */
export interface ObservableEventData_V1 extends IbGibData_V1 {
    /**
     * type of event that happened
     */
    eventType: ObservableEventType;
    /**
     * id of src observable that generated the event.
     */
    srcObservableId: string;
    /**
     * soft link to src observable addr. (soft, i.e., not in rel8ns)
     */
    '@srcObservableAddr': IbGibAddr;
    /**
     * soft link to payload addr. (soft, i.e., not in rel8ns)
     */
    '@payloadAddr': IbGibAddr;
}

/**
 * rel8ns (named edges/links in DAG) go here.
 *
 * @see {@link IbGib_V1.rel8ns}
 * @see {@link ObservableEventIbGib_V1}
 */
export interface ObservableEventRel8ns_V1 extends IbGibRel8ns_V1 {
}

/**
 * Event wrapper passed to plain witness subscribers.
 *
 * @see {@link ObservableEventData_V1}
 * @see {@link ObservableEventRel8ns_V1}
 */
export interface ObservableEventIbGib_V1<TIbGibIn_ie_Payload extends IbGib_V1> extends IbGib_V1<ObservableEventData_V1, ObservableEventRel8ns_V1> {
    /**
     * src ibgib of the event
     */
    observableIbGib: ObservableWitnessAny;
    /**
     * event's payload
     */
    payload: TIbGibIn_ie_Payload;
}

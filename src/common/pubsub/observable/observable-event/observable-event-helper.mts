/**
 * @module observable-event-helper helper functions, utils, etc.
 */

import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";

import { GLOBAL_LOG_A_LOT } from "../../../../core-constants.mjs";
import { ObservableEventData_V1, ObservableEventIbGib_V1, ObservableEventType } from "./observable-event-types.mjs";
import { constantIbGib } from "../../../other/ibgib-helper.mjs";
import { OBSERVABLE_EVENT_ATOM } from "./observable-event-constants.mjs";
import { extractErrorMsg, getUUID } from "@ibgib/helper-gib/dist/helpers/utils-helper.mjs";
import { getIbGibAddr } from "@ibgib/ts-gib/dist/helper.mjs";
import { ObservableData_V1, ObservableWitness } from "../observable-types.mjs";
import { Ib } from "@ibgib/ts-gib/dist/types.mjs";
import { UUID_REGEXP } from "@ibgib/helper-gib/dist/constants.mjs";

/**
 * for logging. import this constant from your project.
 */
const logalot = GLOBAL_LOG_A_LOT; // change this when you want to turn off verbose logging

export async function getObservableEventIbGib<TIbGibIn_ie_Payload extends IbGib_V1>({
    eventType,
    srcObservable,
    payload,
}: {
    eventType: ObservableEventType,
    srcObservable: ObservableWitness<TIbGibIn_ie_Payload>,
    payload: TIbGibIn_ie_Payload | undefined,
}): Promise<ObservableEventIbGib_V1<TIbGibIn_ie_Payload>> {
    const lc = `[${getObservableEventIbGib.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: e2290f2f46d1993d6893a24c19100823)`); }

        // validate
        if (!srcObservable) { throw new Error(`srcObservable required (E: 3470a97bbf3802cbc8dd64339f5ef623)`); }
        if (!srcObservable.data) { throw new Error(`srcObservable.data required (E: df1786585ba849c08e7f4f1fddd30a31)`); }
        if (!srcObservable.data.uuid) { throw new Error(`srcObservable.data.uuid required (E: 173a1a53f274bc1cef879bad2288ca23)`); }
        if (!(srcObservable.data.uuid as string).match(UUID_REGEXP)) { throw new Error(`valid srcObservable.data.uuid required. must pass UUID_REGEXP: ${UUID_REGEXP.source} (E: 10f4bd1863f18250cc79cd65221da223)`); }

        const data: ObservableEventData_V1 = {
            uuid: await getUUID(),
            eventType,
            srcObservableId: srcObservable.data.uuid,
            "@srcObservableAddr": getIbGibAddr({ ibGib: srcObservable }),
            "@payloadAddr": getIbGibAddr({ ibGib: payload }),
        };

        const ibGib = await constantIbGib({
            parentPrimitiveIb: OBSERVABLE_EVENT_ATOM,
            ib: getObservableEventIb({ data }),
            data,
        }) as ObservableEventIbGib_V1<TIbGibIn_ie_Payload>;

        return ibGib;
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export function getObservableEventIb({
    data,
    classname,
}: {
    data: ObservableData_V1,
    classname?: string,
}): Ib {
    const lc = `[${getObservableEventIb.name}]`;
    try {
        if (classname) {
            if (data.classname && data.classname !== classname) { throw new Error(`classname does not match data.classname (E: 523aabc53fb944789202a5f468abd338)`); }
        } else {
            classname = data.classname;
            if (!classname) { throw new Error(`classname required (E: d4401ea682894c5fa824b38d4989aa76)`); }
        }

        if (!data.uuid) { throw new Error(`data.uuid required (E: 204cc7c20efd8a55263cdea8b1223e23)`); }

        return `${OBSERVABLE_EVENT_ATOM} ${classname} ${data.uuid}`;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

/**
 * Current schema is '[OBSERVABLE_EVENT_ATOM] [classname] [ObservableEventId]'
 *
 * NOTE this is space-delimited
 */
export function parseObservableEventIb({
    ib,
}: {
    ib: Ib,
}): {
    ObservableEventClassname: string,
    ObservableEventId: string,
} {
    const lc = `[${parseObservableEventIb.name}]`;
    try {
        if (!ib) { throw new Error(`ObservableEvent ib required (E: 6cd1184d52284912916d6f972d375113)`); }

        const pieces = ib.split(' ');

        return {
            ObservableEventClassname: pieces[1],
            ObservableEventId: pieces[2],
        };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

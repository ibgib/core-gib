/*
 * @module observable helper/util/etc. functions
 *
 * this is where you will find helper functions like those that generate
 * and parse ibs for observable.
 */

// import * as pathUtils from 'path';
// import { statSync } from 'node:fs';
// import { readFile, } from 'node:fs/promises';
// import * as readline from 'node:readline/promises';
// import { stdin, stdout } from 'node:process'; // decide if use this or not

import {
    extractErrorMsg, delay, getSaferSubstring,
    getTimestampInTicks, getUUID, pretty,
} from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { UUID_REGEXP, CLASSNAME_REGEXP, } from '@ibgib/helper-gib/dist/constants.mjs';
import { Ib, } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { validateIbGibIntrinsically, } from '@ibgib/ts-gib/dist/V1/validate-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../core-constants.mjs';
import { WitnessFormBuilder } from '../../../witness/witness-form-builder.mjs';
// import { IbGibObservableAny } from './observable-v1.mjs';
import {
    ObservableData_V1, ObservableRel8ns_V1, ObservableIbGib_V1,
} from './observable-types.mjs';
import { OBSERVABLE_NAME_REGEXP, } from './observable-constants.mjs';
import { OBSERVABLE_ATOM } from './observable-constants.mjs';
import { SUBJECT_ATOM } from '../subject/subject-constants.mjs';


/**
 * for logging. import this constant from your project.
 */
const logalot = GLOBAL_LOG_A_LOT; // remove the true to "turn off" verbose logging

export function validateCommonObservableData({
    data,
}: {
    data?: ObservableData_V1,
}): string[] {
    const lc = `[${validateCommonObservableData.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting...`); }
        if (!data) { throw new Error(`Observable Data required (E: 6d1c97931dcab8ea9f3e9b32c794b90c)`); }
        const errors: string[] = [];
        const {
            name, uuid, classname,
        } =
            data;

        if (name) {
            if (!name.match(OBSERVABLE_NAME_REGEXP)) {
                errors.push(`name must match regexp: ${OBSERVABLE_NAME_REGEXP} (E: 21bf77e398d47e9d035376510505eda2)`);
            }
        } else {
            errors.push(`name required.`);
        }

        if (uuid) {
            if (!uuid.match(UUID_REGEXP)) {
                errors.push(`uuid must match regexp: ${UUID_REGEXP} (E: e53fd08ae69e945b37ed895b864b6bd7)`);
            }
        } else {
            errors.push(`uuid required.`);
        }

        if (classname) {
            if (!classname.match(CLASSNAME_REGEXP)) {
                errors.push(`classname must match regexp: ${CLASSNAME_REGEXP}`);
            }
        }

        return errors;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function validateCommonObservableIbGib({
    ibGib,
}: {
    ibGib: ObservableIbGib_V1,
}): Promise<string[] | undefined> {
    const lc = `[${validateCommonObservableIbGib.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 53696a50efff60fd86a2f431dae6ac67)`); }
        const intrinsicErrors: string[] = await validateIbGibIntrinsically({ ibGib }) ?? [];

        if (!ibGib.data) { throw new Error(`ibGib.data required (E: 5bae607c41ed4de40ec333a40673bdb8)`); }
        const ibErrors: string[] = [];
        let { ObservableClassname, ObservableName, ObservableId } =
            parseObservableIb({ ib: ibGib.ib });
        if (!ObservableClassname) { ibErrors.push(`ObservableClassname required (E: 029f7a2405c4b7aa1f52711586f8ebfd)`); }
        if (!ObservableName) { ibErrors.push(`ObservableName required (E: e68cbc6f3183169c35117184f610573e)`); }
        if (!ObservableId) { ibErrors.push(`ObservableId required (E: 0516442854be7ec32f5928b4cf2697f7)`); }

        const dataErrors = validateCommonObservableData({ data: ibGib.data });

        let result = [...(intrinsicErrors ?? []), ...(ibErrors ?? []), ...(dataErrors ?? [])];
        if (result.length > 0) {
            return result;
        } else {
            return undefined;
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export function getObservableIb({
    data,
    classname,
}: {
    data: ObservableData_V1,
    classname?: string,
}): Ib {
    const lc = `[${getObservableIb.name}]`;
    try {
        const validationErrors = validateCommonObservableData({ data });
        if (validationErrors.length > 0) { throw new Error(`invalid Observable data: ${validationErrors} (E: fd6b0b32d8f6c4b97eebd489cdb07a4a)`); }
        if (classname) {
            if (data.classname && data.classname !== classname) { throw new Error(`classname does not match data.classname (E: 354285a56a20b9b65b06abfcd9f2e621)`); }
        } else {
            classname = data.classname;
            if (!classname) { throw new Error(`classname required (E: 040c1fd71853fa7874146999afb355c3)`); }
        }

        // ad hoc validation here. should centralize witness classname validation

        const { name, uuid } = data;
        return `witness ${OBSERVABLE_ATOM} ${classname} ${name} ${uuid}`;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

/**
 * Current schema is 'witness [OBSERVABLE_ATOM] [classname] [ObservableName] [ObservableId]'
 *
 * NOTE this is space-delimited
 */
export function parseObservableIb({
    ib,
}: {
    ib: Ib,
}): {
    ObservableClassname: string,
    ObservableName: string,
    ObservableId: string,
} {
    const lc = `[${parseObservableIb.name}]`;
    try {
        if (!ib) { throw new Error(`Observable ib required (E: 84156af64e73d479220a36f247466659)`); }

        const pieces = ib.split(' ');

        return {
            ObservableClassname: pieces[2],
            ObservableName: pieces[3],
            ObservableId: pieces[4],
        };
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

export class ObservableFormBuilder extends WitnessFormBuilder {
    protected lc: string = `[${ObservableFormBuilder.name}]`;

    constructor() {
        super();
        this.what = 'observable';
    }

    // exampleSetting({
    //     of,
    //     required,
    // }: {
    //     of: string,
    //     required?: boolean,
    // }): ObservableFormBuilder {
    //     this.addItem({
    //         // Observable.data.exampleSetting
    //         name: "exampleSetting",
    //         description: `example description`,
    //         label: "Example Label",
    //         regexp: EXAMPLE_REGEXP,
    //         regexpErrorMsg: EXAMPLE_REGEXP_DESC,
    //         dataType: 'textarea',
    //         value: of,
    //         required,
    //     });
    //     return this;
    // }

}

/**
 * helper to determine if an ibGib is observable.
 *
 * atow 11/2023 just checks ib for presence of either SUBJECT_ATOM or OBSERVABLE_ATOM
 *
 * @returns true if observable witness, else false.
 */
export function isObservable({
    ibGib,
}: {
    ibGib: IbGib_V1,
}): boolean {
    const lc = `[${isObservable.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 3aadb634377acbe42984b61c91975323)`); }
        const { ib } = ibGib;
        const observableAtoms = [OBSERVABLE_ATOM, SUBJECT_ATOM];
        return observableAtoms.some(atom => ib.includes(atom));
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

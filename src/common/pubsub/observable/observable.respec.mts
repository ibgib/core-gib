/**
 * @module subscription respec
 *
 * we gotta test our subscription
 */

// import { cwd, chdir, } from 'node:process';
// import { statSync } from 'node:fs';
// import { mkdir, } from 'node:fs/promises';
// import { ChildProcess, exec, ExecException } from 'node:child_process';
import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import {
    extractErrorMsg, delay, getSaferSubstring,
    getTimestampInTicks, getUUID, pretty,
} from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../../core-constants.mjs';
import { ObservableWitness, } from './observable-types.mjs';
import { IbGib_V1, } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { ErrorIbGib_V1 } from '../../error/error-types.mjs';
import { GIB, ROOT } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { WitnessAny, WitnessFn } from '../../../witness/witness-types.mjs';
import { Subscription_V1, } from '../subscription/subscription-v1.mjs';
import { AnonymousFnWitness_V1 } from '../../../witness/anonymous-fn/anonymous-fn-v1.mjs';
import { newupSubscription } from '../subscription/subscription-helper.mjs';
import { ObserverWitness } from '../observer/observer-types.mjs';

/**
 * for verbose logging. import this.
 */
const logalot = GLOBAL_LOG_A_LOT; // change this when you want to turn off verbose logging

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;

await respecfully(maam, `observables...`, async () => {
    await ifWe(maam, `are tested right now in subject.respec integration tests.`, async () => {
        iReckon(maam, true).asTo('remove this when have resources to do mock unit testing if we decide that is a good thing').isGonnaBe(true);
    });
});

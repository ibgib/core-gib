/**
 * @module subscription types (and some enums/constants)
 */

import { IbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { WitnessWithContextData_V1, WitnessWithContextRel8ns_V1 } from '../../../witness/witness-with-context/witness-with-context-types.mjs';
import { WitnessCmdData, WitnessCmdIbGib, WitnessCmdRel8ns } from '../../../witness/witness-cmd/witness-cmd-types.mjs';
import { Witness, WitnessAny, WitnessResultData, WitnessResultIbGib, WitnessResultRel8ns } from '../../../witness/witness-types.mjs';
import { DEFAULT_DESCRIPTION_SUBSCRIPTION, DEFAULT_NAME_SUBSCRIPTION, DEFAULT_UUID_SUBSCRIPTION } from './subscription-constants.mjs';
// and just to show where these things are
// import { CommentIbGib_V1 } from "@ibgib/core-gib/dist/common/comment/comment-types.mjs";
// import { MetaspaceService } from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// /**
//  * @see {@link SomeEnum}
//  */
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// /**
//  * values of {@link SomeEnum}
//  */
// export const SOME_ENUM_VALUES: SomeEnum[] = Object.values(SomeEnum);

/**
 * ibgib's intrinsic data goes here.
 *
 * @see {@link IbGib_V1.data}
 */
export interface SubscriptionData_V1 extends WitnessWithContextData_V1 {
    // ibgib data (settings/values/etc) goes here
    // /**
    //  * docs yo
    //  */
    // setting: string;
    // createdTimestamp: string;
    // unsubscribedTimestamp: string;
}

/**
 * rel8ns (named edges/links in DAG) go here.
 *
 * @see {@link IbGib_V1.rel8ns}
 */
export interface SubscriptionRel8ns_V1 extends WitnessWithContextRel8ns_V1 {
    // /**
    //  * required rel8n. for most, put in subscription-constants.mts file
    //  *
    //  * @see {@link REQUIRED_REL8N_NAME}
    //  */
    // [REQUIRED_REL8N_NAME]: IbGibAddr[];
    // /**
    //  * optional rel8n. for most, put in subscription-constants.mts file
    //  *
    //  * @see {@link OPTIONAL_REL8N_NAME}
    //  */
    // [OPTIONAL_REL8N_NAME]?: IbGibAddr[];
}

/**
 * this is the ibgib object itself.
 *
 * If this is a plain ibgib data only object, this acts as a dto. You may also
 * want to generate a witness ibgib, which is slightly different, for ibgibs
 * that will have behavior (i.e. methods).
 *
 * @see {@link SubscriptionData_V1}
 * @see {@link SubscriptionRel8ns_V1}
 */
export interface SubscriptionIbGib_V1 extends IbGib_V1<SubscriptionData_V1, SubscriptionRel8ns_V1> {

}

export type SubscriptionCmd =
    'unsubscribe' | 'unsubscribed';
/** Cmds for interacting with ibgib spaces. */
export const SubscriptionCmd = {
    /**
     * unsubscribe from the subscription's associated observable (that generated
     * this subscription).
     */
    unsubscribe: 'unsubscribe' as SubscriptionCmd,
    /**
     * returns boolean if this subscription is still actively subscribed to the
     * associated observable (that generated this subscription).
     */
    unsubscribed: 'unsubscribed' as SubscriptionCmd,
}

/**
 * Flags to affect the command's interpretation.
 */
export type SubscriptionCmdModifier =
    'ib' | 'gib' | 'ibgib';
/**
 * Flags to affect the command's interpretation.
 */
export const SubscriptionCmdModifier = {
    /**
     * hmm...
     */
    ib: 'ib' as SubscriptionCmdModifier,
    /**
     * hmm...
     */
    gib: 'gib' as SubscriptionCmdModifier,
    /**
     * hmm...
     */
    ibgib: 'ibgib' as SubscriptionCmdModifier,
}

/** Information for interacting with spaces. */
export interface SubscriptionCmdData
    extends WitnessCmdData<SubscriptionCmd, SubscriptionCmdModifier> {
}

export interface SubscriptionCmdRel8ns extends WitnessCmdRel8ns {
}

/**
 * Shape of options ibgib if used for a command-based witness.
 */
export interface SubscriptionCmdIbGib
    extends WitnessCmdIbGib<
        IbGib_V1,
        SubscriptionCmd, SubscriptionCmdModifier,
        SubscriptionCmdData, SubscriptionCmdRel8ns
    > {
}

/**
 * Optional shape of result data to witness interactions.
 *
 * This is in addition of course to {@link SubscriptionResultData}.
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * ## notes
 *
 * * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface SubscriptionResultData extends WitnessResultData {
}

/**
 * Marker interface rel8ns atm...
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface SubscriptionResultRel8ns extends WitnessResultRel8ns { }

/**
 * Shape of result ibgib if used for a witness.
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface SubscriptionResultIbGib
    extends WitnessResultIbGib<IbGib_V1, SubscriptionResultData, SubscriptionResultRel8ns> {
}

/**
 * shape of underscore-delimited addl metadata string that may be present in the
 * ib (i.e. available when parsing the ib)
 *
 * This is not hard and fast and can (and should?) vary greatly per use case.
 */
export interface SubscriptionAddlMetadata {
    /**
     * should be subscription
     */
    atom?: 'subscription'
    /**
     * classname of subscription **with any underscores removed**.
     */
    classnameIsh?: string;
    /**
     * name of subscription witness (data.name) **with any underscores removed**.
     */
    nameIsh?: string;
    /**
     * id of witness (data.uuid) **with any underscores removed**.
     *
     * may be a substring per use case...?
     */
    idIsh?: string;
}

/**
 * Default data values for a Subscription.
 *
 * If you change this, please bump the version
 *
 * (but of course won't be the end of the world when this doesn't happen).
 */
export const DEFAULT_SUBSCRIPTION_DATA_V1: SubscriptionData_V1 = {
    version: '1',
    uuid: DEFAULT_UUID_SUBSCRIPTION,
    name: DEFAULT_NAME_SUBSCRIPTION,
    description: DEFAULT_DESCRIPTION_SUBSCRIPTION,
    classname: `Subscription_V1`,

    /**
     * if true, then the witness will attempt to persist ALL calls to
     * `witness.witness(...)`.
     */
    persistOptsAndResultIbGibs: false,
    /**
     * allow ibgibs like 42^gib ({ib: 42, gib: 'gib'} with `data` and `rel8ns` undefined)
     */
    allowPrimitiveArgs: true,
    /**
     * witnesses should be guaranteed not to throw uncaught exceptions.
     */
    catchAllErrors: false,
    /**
     * if true, would enable logging of all calls to `witness.witness(...)`
     */
    trace: false,

    // put in your custom defaults here
}
export const DEFAULT_SUBSCRIPTION_REL8NS_V1: SubscriptionRel8ns_V1 | undefined = undefined;

/**
 * v1 subscription witness shape
 */
export interface SubscriptionWitness extends WitnessAny {
    /**
     * unsubscribe a subscriber of an observable via this subscription.
     * see `Subscription_V1`
     */
    unsubscribe(): Promise<void>;
    /**
     * flag to indicate if a subscription is still active or not.
     * see `Subscription_V1`
     */
    unsubscribed(): Promise<boolean>;
}

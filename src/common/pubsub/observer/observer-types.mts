/**
 * @module observer types (and some enums/constants)
 *
 * i don't have observers fleshed out like observables/subjects.
 *
 * i think this is more of list of shapes to be used by other concrete
 * observers, like subjects.
 */

import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { Witness, WitnessAny, } from '../../../witness/witness-types.mjs';
import { ErrorIbGib_V1 } from '../../error/error-types.mjs';



/**
 * Plain-jane, non-witness version of observer shape.
 *
 * note that even though this isn't witness-based, it still is mostly
 * concerned with ibgibs as args. Where there are non-ibgib args,
 * this should be converted in the concrete implementor to an ibgib-wrapped
 * arg and passed to the `witness` function.
 *
 * @see {@link ObserverWitness}
 */
export interface Observer<T extends IbGib_V1 = IbGib_V1> {
    next(ibGib: T): Promise<void>;
    /**
     * @optional handler for an observable stream that produces an error.
     * @param error error or error ibgib produced when observable errors out
     */
    error?(error: string | Error | ErrorIbGib_V1): Promise<void>;
    complete?(): Promise<void>;
}

/**
 * v1 observER witness shape (as opposed to observABLE)
 *
 * this is the shape of the consumer's handler, usually an anonymous function.
 *
 * so when you subscribe to an observABLE witness, you pass in this shape which
 * will contain anonymous function ibgib witnesses.
 */
export interface ObserverWitness<TIbGibIn_ie_Payload extends IbGib_V1, TIbGibOut_ie_ProbablyDontCare extends IbGib_V1 = IbGib_V1>
    extends WitnessAny, Observer<TIbGibIn_ie_Payload> {
}

/** Cmds for interacting with ibgib observer witnesses. */
export type ObserverCmd =
    'next' | 'error' | 'complete';
/** Cmds for interacting with ibgib observer witnesses. */
export const ObserverCmd = {
    /**
     * it's more like a grunt that is intepreted by context.
     */
    next: 'next' as ObserverCmd,
    /**
     * it's more like a grunt that is intepreted by context.
     */
    error: 'error' as ObserverCmd,
    /**
     * indicate to complete the observable
     */
    complete: 'complete' as ObserverCmd,
}

/**
 * @module subject types (and some enums/constants)
 */

import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';

import { WitnessCmdData, WitnessCmdIbGib, WitnessCmdRel8ns } from '../../../witness/witness-cmd/witness-cmd-types.mjs';
import { WitnessResultData, WitnessResultIbGib, WitnessResultRel8ns } from '../../../witness/witness-types.mjs';
import { DEFAULT_DESCRIPTION_SUBJECT, DEFAULT_NAME_SUBJECT, DEFAULT_UUID_SUBJECT } from './subject-constants.mjs';
import {
    ObservableCmd,
    ObservableData_V1, ObservableRel8ns_V1,
    ObservableWitness,
} from '../observable/observable-types.mjs';
import { ObserverCmd, ObserverWitness } from '../observer/observer-types.mjs';

/**
 * ibgib's intrinsic data goes here.
 *
 * @see {@link IbGib_V1.data}
 */
export interface SubjectData_V1 extends ObservableData_V1 {
    // ibgib data (settings/values/etc) goes here
    // /**
    //  * docs yo
    //  */
    // setting: string;
}

/**
 * rel8ns (named edges/links in DAG) go here.
 *
 * @see {@link IbGib_V1.rel8ns}
 */
export interface SubjectRel8ns_V1 extends ObservableRel8ns_V1 {
    // /**
    //  * required rel8n. for most, put in subject-constants.mts file
    //  *
    //  * @see {@link REQUIRED_REL8N_NAME}
    //  */
    // [REQUIRED_REL8N_NAME]: IbGibAddr[];
    // /**
    //  * optional rel8n. for most, put in subject-constants.mts file
    //  *
    //  * @see {@link OPTIONAL_REL8N_NAME}
    //  */
    // [OPTIONAL_REL8N_NAME]?: IbGibAddr[];
}

/**
 * this is the ibgib object itself.
 *
 * If this is a plain ibgib data only object, this acts as a dto. You may also
 * want to generate a witness ibgib, which is slightly different, for ibgibs
 * that will have behavior (i.e. methods).
 *
 * @see {@link SubjectData_V1}
 * @see {@link SubjectRel8ns_V1}
 */
export interface SubjectIbGib_V1 extends IbGib_V1<SubjectData_V1, SubjectRel8ns_V1> {

}

/**
 * Cmds for interacting with ibgib subject witnesses.
 *
 * this is atow (11/2023) the union of `ObservableCmd` and `ObserverCmd`.
 *
 * @see {ObservableCmd}
 * @see {ObserverCmd}
 * */
export type SubjectCmd =
    ObservableCmd | ObserverCmd;
/**
 * Cmds for interacting with ibgib subject witnesses.
 *
 * this is atow (11/2023) the union of `ObservableCmd` and `ObserverCmd`.
 *
 * @see {ObservableCmd}
 * @see {ObserverCmd}
 * */
export const SubjectCmd = {
    ...ObservableCmd,
    ...ObserverCmd,
}

/**
 * Flags to affect the command's interpretation.
 */
export type SubjectCmdModifier =
    'ib' | 'gib' | 'ibgib';
/**
 * Flags to affect the command's interpretation.
 */
export const SubjectCmdModifier = {
    /**
     * hmm...
     */
    ib: 'ib' as SubjectCmdModifier,
    /**
     * hmm...
     */
    gib: 'gib' as SubjectCmdModifier,
    /**
     * hmm...
     */
    ibgib: 'ibgib' as SubjectCmdModifier,
}

/** Information for interacting with spaces. */
export interface SubjectCmdData
    extends WitnessCmdData<SubjectCmd, SubjectCmdModifier> {
}

export interface SubjectCmdRel8ns extends WitnessCmdRel8ns {
}

/**
 * Shape of options ibgib if used for a command-based witness.
 */
export interface SubjectCmdIbGib
    extends WitnessCmdIbGib<
        IbGib_V1,
        SubjectCmd, SubjectCmdModifier,
        SubjectCmdData, SubjectCmdRel8ns
    > {
}

/**
 * Optional shape of result data to witness interactions.
 *
 * This is in addition of course to {@link SubjectResultData}.
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * ## notes
 *
 * * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface SubjectResultData extends WitnessResultData {
}

/**
 * Marker interface rel8ns atm...
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface SubjectResultRel8ns extends WitnessResultRel8ns { }

/**
 * Shape of result ibgib if used for a witness.
 *
 * so if you're sending a cmd to this witness, this should probably be the shape
 * of the result.
 *
 * I'm not sure what to do with this atm, so I'm just stubbing out...
 */
export interface SubjectResultIbGib
    extends WitnessResultIbGib<IbGib_V1, SubjectResultData, SubjectResultRel8ns> {
}

/**
 * shape of underscore-delimited addl metadata string that may be present in the
 * ib (i.e. available when parsing the ib)
 *
 * This is not hard and fast and can (and should?) vary greatly per use case.
 */
export interface SubjectAddlMetadata {
    /**
     * should be subject
     */
    atom?: 'subject'
    /**
     * classname of subject **with any underscores removed**.
     */
    classnameIsh?: string;
    /**
     * name of subject witness (data.name) **with any underscores removed**.
     */
    nameIsh?: string;
    /**
     * id of witness (data.uuid) **with any underscores removed**.
     *
     * may be a substring per use case...?
     */
    idIsh?: string;
}

/**
 * Default data values for a Subject.
 *
 * If you change this, please bump the version
 *
 * (but of course won't be the end of the world when this doesn't happen).
 */
export const DEFAULT_SUBJECT_DATA_V1: SubjectData_V1 = {
    version: '1',
    uuid: DEFAULT_UUID_SUBJECT,
    name: DEFAULT_NAME_SUBJECT,
    description: DEFAULT_DESCRIPTION_SUBJECT,
    classname: `Subject_V1`,

    /**
     * if true, then the witness will attempt to persist ALL calls to
     * `witness.witness(...)`.
     */
    persistOptsAndResultIbGibs: false,
    /**
     * allow ibgibs like 42^gib ({ib: 42, gib: 'gib'} with `data` and `rel8ns` undefined)
     */
    allowPrimitiveArgs: true,
    /**
     * witnesses should be guaranteed not to throw uncaught exceptions.
     */
    catchAllErrors: true,
    /**
     * if true, would enable logging of all calls to `witness.witness(...)`
     */
    trace: false,

    // put in your custom defaults here
}
export const DEFAULT_SUBJECT_REL8NS_V1: SubjectRel8ns_V1 | undefined = undefined;


/**
 * Subject is both an observable stream and event producer
 */
export interface SubjectWitness<
    TIbGibIn_ie_Payload extends IbGib_V1 = IbGib_V1,
    TIbGibOut_ie_ProbablyDontCare extends IbGib_V1 = IbGib_V1
> extends ObserverWitness<TIbGibIn_ie_Payload>,
    ObservableWitness<TIbGibIn_ie_Payload, TIbGibOut_ie_ProbablyDontCare> {
    asObservable(): ObservableWitness<TIbGibIn_ie_Payload, TIbGibOut_ie_ProbablyDontCare>;
}

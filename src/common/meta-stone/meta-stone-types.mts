/**
 * @module meta-stone types (and enums)
 */

import { Gib, IbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGibData_V1, IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { META_STONE_TARGET_REL8N_NAME, META_STONE_TARGET_TJP_REL8N_NAME } from './meta-stone-constants.mjs';
// and just to show where these things are
// import { CommentIbGib_V1 } from "@ibgib/core-gib/dist/common/comment/comment-types.mjs";
// import { MetaspaceService } from "@ibgib/core-gib/dist/witness/space/metaspace/metaspace-types.mjs";

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// /**
//  * @see {@link SomeEnum}
//  */
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// /**
//  * values of {@link SomeEnum}
//  */
// export const SOME_ENUM_VALUES: SomeEnum[] = Object.values(SomeEnum);

export interface MetaStoneTargetData_V1 extends IbGibData_V1 {
    /**
     * tjpGib that this stone corresponds to.
     */
    tjpGib: string;
    /**
     * true if the target is the tjp
     */
    isTjp?: boolean;
    /**
     * n of the metastone's target ibgib
     *
     * if the target ibgib does not have an `n` property (for whatever reason,
     * though it "should" atow 11/2023), then this should be -1.
     *
     * _NOTE: NOT n of this stone! stones by definition do not change and so don't have n's that keep track of changes through time._
     *
     * This property is redeclared here (in this Data interface) from its
     * original declaration in the `IbGibData_V1` to make it explicitly required
     * (non-optional), in addition to providing additional contextual
     * documentation.
     *
     * @see {@link IbGibData_V1.n}
     */
    n: number;
    /**
     * timestamp of the metastone's target ibgib
     */
    timestamp?: string;
}

/**
 * ibgib's intrinsic data goes here.
 *
 * @see {@link IbGib_V1.data}
 */
export interface MetaStoneData_V1 extends IbGibData_V1 {
    /**
     * target data that relates to the target ibGib's data (but not a straight
     * copy of ibGib.data)
     */
    targetData: MetaStoneTargetData_V1;
    /**
     * when this stone was made (local wall time). should be the same as the
     * timestampInTicks in the metastone's `ib`.
     */
    timestamp: string;
}

/**
 * rel8ns (named edges/links in DAG) go here.
 *
 * @see {@link IbGib_V1.rel8ns}
 */
export interface MetaStoneRel8ns_V1 extends IbGibRel8ns_V1 {
    [META_STONE_TARGET_REL8N_NAME]: IbGibAddr[];
    [META_STONE_TARGET_TJP_REL8N_NAME]?: IbGibAddr[];
}

/**
 * This metastone should provide metadata that corresponds to a "target" ibgib.
 *
 * In particular, it should provide a navigable subspace to search for metadata
 * regarding a target ibgib, e.g., its latest incarnation (see driving use case
 * section).
 *
 * AFAICT this metastone should be local to a space and not travel with the
 * target ibgib. When a space "ingests" an ibgib, it will be up to that space to
 * create its own metastones regarding that ibgib. This (I think) stems from
 * the process of merging timelines. see the section below on interspatial stone
 * dynamics.
 *
 * ## storage/retrieval in space
 *
 * so this metastone ibgib is special in that it should be stored in special
 * relationship to its target (target's tjpGib is in the metastone's `ib`).  in
 * my initial implementation in the filesystem space, i will be putting these
 * metastones inside the the space's "meta" subpath - but inside another
 * subfolder with the name of the tjpGib. (Like all uses of bare tjpGib without
 * `ib`, this holds a slight possibility of collision across all spaces, but
 * that's for a later time where we can tweak this approach.)
 *
 * In other non-filesystem approaches, the idea should still be that the
 * metastones enable the capability to group and analyze timelines as needed.
 * And these metastones should (at least for the time being 11/2023) be expected
 * not to travel when synchronizing/merging/pushing/pulling timeline graphs.
 *
 * This does **NOT** mean that all metadata should be stored in metastones. Far
 * from it, much metadata will be derivative ibgibs with their own timelines -
 * and indeed their own metastones.
 *
 * ## driving use case
 *
 * I am changing implementation of indexing the latest ibgib in an ibgib's
 * timeline via a special ibgib (see `SpecialIbGibType`) to using these meta
 * stones. The special ibgib indexing is naively brittle, providing a single
 * point of failure as well as a bottleneck when reading/updating the latest
 * addr.
 *
 * ## interspatial stone dynamics: when merging timelines across spaces
 *
 * merging timelines is a tricky business. if you have two different spaces
 * working on the "same" ibgib timeline (i.e. with distributed collaboration),
 * then they will have diverging timelines that share the same properties, and
 * in particular, the `n` property which keeps track of what iteration of that
 * ibgib it is in its timeline.
 *
 * So if Alice and Bob both start working on a timeline at iteration 5, and each
 * makes a local change, then when they go to merge those changes, each one will
 * have a local copy of the ibgib that says `n = 5` but they will be different
 * ibgibs. The resulting merged timeline will require (in most projected use
 * cases atow 11/2023) that both be applied. This will put, say, Bob's changes
 * being applied after Alice's in the merge space, giving it a correspondingly
 * higher value of `n`.
 *
 * So if we were to include the metastone also we would have multiple stones
 * per `n` which may mislead interpretations. We would have to include `spaceId`
 * discriminators and this would blow up graph size when transporting among
 * spaces.
 *
 * It is conceivable that this actually gets outweighed by positive benefits
 * of providing more context, but my initial feeling is again that we should
 * not spread these stones across spaces.
 *
 * @see {@link MetaStoneData_V1}
 * @see {@link MetaStoneRel8ns_V1}
 */
export interface MetaStoneIbGib_V1 extends IbGib_V1<MetaStoneData_V1, MetaStoneRel8ns_V1> {

}

/**
 * used when parsing metastone ib
 */
export interface MetaStoneIbInfo {
    atom: string,
    tjpGib: string,
    n: number,
    timestampInTicks: string,
}

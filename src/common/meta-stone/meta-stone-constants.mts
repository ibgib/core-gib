/**
 * @module meta-stone constants
 *
 * constants are in this file!
 */

// /**
//  * example enum-like type+const that I use in ibgib. sometimes i put
//  * these in the types.mts and sometimes in the const.mts, depending
//  * on context.
//  */
// export type SomeEnum =
//     'ib' | 'gib';
// export const SomeEnum = {
//     ib: 'ib' as SomeEnum,
//     gib: 'gib' as SomeEnum,
// } satisfies { [key: string]: SomeEnum };
// export const SOME_TYPE_VALUES: SomeEnum[] = Object.values(SomeEnum);

/**
 * atom used in ibs
 *
 * atow (11/2023) this should also be (as with most atoms) the ib of the parent
 * primitive ibgib used when creating a new ibgib (forking a primitive ibgib
 * with this ib).
 */
export const META_STONE_ATOM = 'meta_stone';

/**
 * default regexp for a simple name string.
 */
export const META_STONE_NAME_REGEXP = /^[a-zA-Z0-9_\-.]{1,255}$/;

/**
 * for use when creating a metastone targeting an ibgib without a tjp (i.e. a
 * stone ibgib itself).
 */
export const DEFAULT_META_STONE_TJPGIB = 'undefined';
/**
 * for use when targetIbGib.data.n is falsy or NaN
 */
export const DEFAULT_META_STONE_N = -1;
/**
 * for use when targetIbGib.data.timestamp is falsy
 */
export const DEFAULT_META_STONE_TIMESTAMP = 'undefined';

/**
 * rel8n name pointing from the metastone to its target ibgib.
 */
export const META_STONE_TARGET_REL8N_NAME = 'target';
/**
 * rel8n name pointing from the metastone to the target's temporal junction
 * point.
 */
export const META_STONE_TARGET_TJP_REL8N_NAME = 'targetTjp';

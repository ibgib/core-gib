/**
 * @module meta-stone respec
 *
 * we gotta test our meta-stone
 */

// import { cwd, chdir, } from 'node:process';
// import { statSync } from 'node:fs';
// import { mkdir, } from 'node:fs/promises';
// import { ChildProcess, exec, ExecException } from 'node:child_process';
import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import {
    extractErrorMsg, delay, getSaferSubstring,
    getTimestampInTicks, getUUID, pretty, hash, clone,
} from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { IbGib_V1, IbGibData_V1, Rel8n } from '@ibgib/ts-gib/dist/V1/types.mjs';
import { Ib } from '@ibgib/ts-gib/dist/types.mjs';
import { IB } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { getGib, getGibInfo } from '@ibgib/ts-gib/dist/V1/transforms/transform-helper.mjs';
import { Factory_V1 } from '@ibgib/ts-gib/dist/V1/factory.mjs';

import { GLOBAL_LOG_A_LOT } from '../../core-constants.mjs';
import { MetaStoneData_V1, MetaStoneIbGib_V1, MetaStoneTargetData_V1 } from './meta-stone-types.mjs';
import { getMetaStoneIb, isMetaStone, newUpMetaStone, parseMetaStoneIb } from './meta-stone-helper.mjs';
import { META_STONE_ATOM } from './meta-stone-constants.mjs';

/**
 * for verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT; // change this when you want to turn off verbose logging

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;

const testTjpGib_A = await hash({ s: '123' });
const testIbGib_A: IbGib_V1 = {
    ib: IB,
    data: {
        n: 1,
        timestamp: 'Wed, 29 Nov 2023 17:21:59 GMT',
    },
    rel8ns: {
        [Rel8n.tjp]: [`ib^${testTjpGib_A}`],
    }
}
testIbGib_A.gib = await getGib({ ibGib: testIbGib_A, hasTjp: true });

const testTargetData_A_n1: MetaStoneTargetData_V1 = {
    tjpGib: testTjpGib_A,
    n: testIbGib_A.data!.n!,
    timestamp: testIbGib_A.data!.timestamp,
}
const VALID_META_STONE_DATA_N1_A: MetaStoneData_V1 = {
    targetData: clone(testTargetData_A_n1),
    timestamp: 'Wed, 29 Nov 2023 17:24:31 GMT',
}

await respecfully(maam, `newUpMetaStone`, async () => {
    let metaStone: MetaStoneIbGib_V1;

    await ifWe(maam, `getMetaStoneIb...`, async () => {
        const targetIbGib = testIbGib_A;
        metaStone = await newUpMetaStone({ targetIbGib });
        iReckon(maam, metaStone).asTo('metaStone').isGonnaBeTruthy();
        iReckon(maam, metaStone.data).asTo('metaStone.data').isGonnaBeTruthy();
        iReckon(maam, metaStone.data?.targetData).asTo('metaStone.data.targetData').isGonnaBeTruthy();
        iReckon(maam, metaStone.data?.targetData.n).asTo('metaStone.data.targetData.n').isGonnaBe(targetIbGib.data?.n);
        iReckon(maam, metaStone.data?.targetData.tjpGib).asTo('metaStone.data.targetData.tjpGib').isGonnaBe(testTjpGib_A);
        iReckon(maam, metaStone.data?.targetData.timestamp).asTo('metaStone.data.targetData.timestamp').isGonnaBe(testIbGib_A.data?.timestamp);
        iReckon(maam, metaStone.data?.targetData.timestamp).asTo('metaStone.data.targetData.timestamp').isGonnaBeTruthy();
    });

    await ifWe(maam, `check isMetaStone...`, async () => {
        let resIsMetaStone = isMetaStone({ ibGib: metaStone });
        iReckon(maam, resIsMetaStone).asTo('resIsMetaStone').isGonnaBeTrue();
    });

});

await respecfully(maam, `isMetaStone`, async () => {
    const validIbs = ['ib', 'hi there', 'comment Hi_there_yo 12345'];

    for (let i = 0; i < validIbs.length; i++) {
        const ib = validIbs[i];

        const primitiveIbGib = Factory_V1.primitive({ ib });
        await ifWe(maam, `isMetaStone(primitive)`, async () => {
            const resIsMetaStone = isMetaStone({ ibGib: primitiveIbGib });
            iReckon(maam, resIsMetaStone).asTo('resIsMetaStone').isGonnaBeFalse();
        });

        const firstGenIbGib = (await Factory_V1.firstGen({ parentIbGib: primitiveIbGib, ib })).newIbGib;
        await ifWe(maam, `isMetaStone(firstGen)`, async () => {
            const resIsMetaStone = isMetaStone({ ibGib: firstGenIbGib });
            iReckon(maam, resIsMetaStone).asTo('resIsMetaStone').isGonnaBeFalse();
        });
    }
});

await respecfully(maam, `get/parseMetaStoneIb`, async () => {
    let ib: Ib;

    await ifWe(maam, `getMetaStoneIb`, async () => {
        ib = getMetaStoneIb({ data: VALID_META_STONE_DATA_N1_A });
        iReckon(maam, ib.startsWith(META_STONE_ATOM + ' ')).asTo('ib atom start').isGonnaBeTrue();
    });

    await ifWe(maam, `parseMetaStoneIb`, async () => {
        let info = parseMetaStoneIb({ ib });
        iReckon(maam, info.atom).asTo('info.atom').isGonnaBe(META_STONE_ATOM);
        iReckon(maam, info.n).asTo('info.n').isGonnaBe(testTargetData_A_n1.n);
        iReckon(maam, info.timestampInTicks).asTo('info.timestampInTicks').isGonnaBe(getTimestampInTicks(testTargetData_A_n1.timestamp));
        iReckon(maam, info.tjpGib).asTo('info.tjpGib').isGonnaBe(testTjpGib_A);
    });
});

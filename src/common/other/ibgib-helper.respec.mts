/**
 * @module ibgib-helper respec
 *
 * we gotta test our ibgib-helper
 */

// import { cwd, chdir, } from 'node:process';
// import { statSync } from 'node:fs';
// import { mkdir, } from 'node:fs/promises';
// import { ChildProcess, exec, ExecException } from 'node:child_process';
import * as pathUtils from 'path';

import {
    firstOfAll, firstOfEach, ifWe, ifWeMight, iReckon,
    lastOfAll, lastOfEach, respecfully, respecfullyDear
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import {
    extractErrorMsg, delay, getSaferSubstring,
    getTimestampInTicks, getUUID, pretty,
} from '@ibgib/helper-gib/dist/helpers/utils-helper.mjs';
import { GIB, IB } from '@ibgib/ts-gib/dist/V1/constants.mjs';
import { getIbGibAddr } from '@ibgib/ts-gib/dist/helper.mjs';

import { GLOBAL_LOG_A_LOT } from '../../core-constants.mjs';
import { getBinHashAndExt, getBinIb, getTimestampInfo } from './ibgib-helper.mjs';

/**
 * for verbose logging
 */
const logalot = GLOBAL_LOG_A_LOT; // change this when you want to turn off verbose logging

const lcFile: string = `[${pathUtils.basename(import.meta.url)}]`;

const VALID_TIMESTAMPS_STRINGS: string[] = [
    (new Date().toUTCString()),
    (new Date().toISOString()),
    (new Date().toString()),
    (new Date().toLocaleDateString()),
    getTimestampInTicks(),
];

const QUASI_VALID_TIMESTAMPS_NUMBERS: number[] = [
    Number.parseInt(getTimestampInTicks()),
];

const INVALID_TIMESTAMPS: any[] = [
    'wakka',
    Number.MAX_SAFE_INTEGER.toString(),
    Number.MAX_SAFE_INTEGER,
    { a: 'wakka object here' },
];

await respecfully(maam, `getTimestampInfoFromIbGib`, async () => {
    await respecfully(maam, `valid tests`, async () => {
        const timestamps = VALID_TIMESTAMPS_STRINGS.concat();
        for (let i = 0; i < timestamps.length; i++) {
            const timestamp = timestamps[i];
            const ibGib = {
                ib: IB, gib: GIB,
                data: { timestamp },
            };
            await ifWe(maam, `get info...`, async () => {
                const info = getTimestampInfo({ ibGib });
                iReckon(maam, info.valid).asTo('info.valid').isGonnaBeTrue();
                if (info.valid) {
                    iReckon(maam, info.date).asTo('info.date').isGonnaBeTruthy();
                    iReckon(maam, info.ticks).asTo('info.date').isGonnaBeTruthy();
                    iReckon(maam, info.utc).asTo('info.date').isGonnaBeTruthy();
                }
            });
        }
    });

    await respecfully(maam, `quasi valid tests (timestamps are valid ticks typeof number)`, async () => {
        const timestamps = QUASI_VALID_TIMESTAMPS_NUMBERS.concat();
        for (let i = 0; i < timestamps.length; i++) {
            const timestamp = timestamps[i];
            const ibGib = {
                ib: IB, gib: GIB,
                data: { timestamp: (timestamp as any) },
            };
            await ifWe(maam, `get info...`, async () => {
                const info = getTimestampInfo({ ibGib });
                iReckon(maam, info.valid).asTo('info.valid').isGonnaBeTrue();
                if (info.valid) {
                    iReckon(maam, info.date).asTo('info.date').isGonnaBeTruthy();
                    iReckon(maam, info.ticks).asTo('info.date').isGonnaBeTruthy();
                    iReckon(maam, info.utc).asTo('info.date').isGonnaBeTruthy();
                }
            });
        }
    });

    await respecfully(maam, `quasi valid tests (timestamps are valid ticks typeof number)`, async () => {
        const timestamps = INVALID_TIMESTAMPS.concat();
        for (let i = 0; i < timestamps.length; i++) {
            const timestamp = timestamps[i];
            const ibGib = {
                ib: IB, gib: GIB,
                data: { timestamp: (timestamp as any) },
            };
            await ifWe(maam, `get info...`, async () => {
                const info = getTimestampInfo({ ibGib });
                iReckon(maam, info.valid).asTo('info.valid').isGonnaBeFalse();
                if (!info.valid) {
                    iReckon(maam, info.emsg).asTo('info.emsg').isGonnaBeTruthy();
                }
            });
        }
    });
});

await respecfully(maam, `bin ib`, async () => {

    const binHash = await getUUID();
    const fakeGib = await getUUID();

    await respecfully(maam, `binHash only`, async () => {
        const ib = getBinIb({
            binHash,
        });

        const addr = getIbGibAddr

        const parsed = getBinHashAndExt({})

        await ifWe(maam, `getBinIb`, async () => {
            iReckon(maam, ib).asTo('ib').isGonnaBeTruthy();
            iReckon(maam, ib.split(' ').length).asTo('ib.split(" ").length').isGonnaBe(2);
        });

        await ifWe(maam, `getBinHashAndExt`, async () => {
        });

    });

    await respecfully(maam, ``, async () => {
    });

});

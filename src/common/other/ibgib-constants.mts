import { GIB } from "@ibgib/ts-gib/dist/V1/constants.mjs"
import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs"

/**
 * primitive ibgib for indicating a true value
 */
export const TRUE_GIB: IbGib_V1 = { ib: 'true', gib: GIB }
/**
 * primitive ibgib for indicating a truthy value
 */
export const TRUTHY_GIB: IbGib_V1 = { ib: 'truthy', gib: GIB };
/**
 * primitive ibgib for indicating a false value
 */
export const FALSE_GIB: IbGib_V1 = { ib: 'false', gib: GIB }
/**
 * primitive ibgib for indicating a falsy value
 */
export const FALSY_GIB: IbGib_V1 = { ib: 'falsy', gib: GIB }

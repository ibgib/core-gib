/**
 * @module
 *
 * not quite sure where these go.
 *
 * todo: check after the project is building where these other-constants fit.
 */

/**
 * default id settings where...?
 */
export const DEFAULT_UUID = undefined;

import { ROBBOT_REL8N_NAME } from '../../witness/robbot/robbot-constants.mjs';
import { SYNC_SPACE_REL8N_NAME } from '../../witness/space/space-constants.mjs';
import { DEFAULT_ROOT_REL8N_NAME, ROOT_REL8N_NAME } from '../root/root-constants.mjs';
import { TAGGED_REL8N_NAME, TAG_REL8N_NAME } from '../tag/tag-constants.mjs';


export const CONSENSUS_REL8N_NAME = 'consensus';
export const CONSENSUS_ADDR_SYNC_NAIVE_PUT_MERGE = 'sync naive put merge^gib'

export const AUTOSYNC_ALWAYS_REL8N_NAME = 'always';
export const ARCHIVE_REL8N_NAME = 'archive';
export const TRASH_REL8N_NAME = 'trash';

/**
 * These rel8n names are shown in a list view by default.
 *
 * This should perhaps go into a chat or common ux constants?
 */
export const DEFAULT_LIST_REL8N_NAMES: string[] = [
    'pic', 'comment', 'link',
    'result', 'import',
    'tagged',
    TAGGED_REL8N_NAME,
    TAG_REL8N_NAME,
    ROOT_REL8N_NAME, // hack for now to get all to show
    DEFAULT_ROOT_REL8N_NAME,
    ROBBOT_REL8N_NAME,
    SYNC_SPACE_REL8N_NAME,
];

/**
 *
 */
export const CURRENT_VERSION = '1';

/**
 * List of common special chars for english.
 *
 * ## intent
 *
 * When sanitizing input.
 */
export const ALLISH_SPECIAL_CHARS = `\`~!@#$%^&*()_\\-+=|\\\\\\]}[{"':;?/>.<,`;
export const FILENAME_SPECIAL_CHARS = ` \`~!@#$%&*()_\\-+=|\\]}[{"';?>.<,`;

/**
 * If this matches, then we will encode the data field in our storage (aws
 * anyway atow)
 */
export const IBGIB_DATA_REGEX_INDICATES_NEED_TO_ENCODE = /[^\w\s\d`~!@#$%\^&*()_\\\-+=|\]\}\[\{"':;?/>.<,]/;

/**
 * hacky scroll to bottom after items load per platform
 */
export const DEFAULT_SCROLL_DELAY_MS_WEB_HACK = 5_000;
/**
 * hacky scroll to bottom after items load per platform
 */
export const DEFAULT_SCROLL_DELAY_MS_ANDROID_HACK = 3_000;
/**
 * hacky scroll to bottom after items load per platform
 */
export const DEFAULT_SCROLL_DELAY_MS_IOS_HACK = 2_000;

/**
 * when you create a meta special ibgib, it must just be some alphanumerics.
 */
export const SPECIAL_IBGIB_TYPE_REGEXP = /^\w{1,32}$/;

/**
 * big weird decision in JS this one...
 */
export const INVALID_DATE_STRING = "Invalid Date";

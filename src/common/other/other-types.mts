// #region from common/types/legacy.ts

// todo: clean up legacy get/put functions
// I started ionic-gib without a clear architectural design, so this is "legacy"
// with respect to that. I had several functions that used these types, and I
// left them as-is at the time. after this refactor, this needs to be cleaned
// up. Perhaps this goes in capacitor-gib even?

import { IbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/index.mjs';
import { IbGibSpaceAny } from '../../witness/space/space-base-v1.mjs';

export interface FileResult {
    success?: boolean;
    /**
     * If errored, this will contain the errorMsg.
     */
    errorMsg?: string;
    /**
     * True if failed due to timing out.
     */
    timedOut?: boolean;
}

/**
 * Options for retrieving data from the file system.
 */
export interface GetIbGibOpts {
    /**
     * If getting ibGib object, this is its address.
     */
    addr?: IbGibAddr;
    /**
     * If getting multiple ibGibs, use this array of addrs.
     */
    addrs?: IbGibAddr[];
    /**
     * Are we looking for a DNA ibgib?
     */
    isDna?: boolean;
    /**
     * space from which to get the ibgib
     *
     * @default localUserSpace
     */
    space?: IbGibSpaceAny,
    /**
     * If supplied, first acquires the lock with this scope on the
     * given `space` (which defaults to localUserSpace).
     */
    lockScope?: string,
    /**
     * Cancels if can't acquire lock after approximately this time.
     */
    lockTimeoutMs?: number,
    /**
     * If true, will not get from cache and will force retrieval from the real
     * bucket. In Ionic space, this means will look in ionic storage proper.
     */
    force?: boolean;
}

/**
 * Result for retrieving an ibGib from the file system.
 */
export interface GetIbGibResult extends FileResult {
    /**
     * ibGibs if retrieving a "regular" ibGib.
     *
     * This is used when you're not getting a pic, e.g.
     */
    ibGibs?: IbGib_V1[];
    /**
     * This is used when you're getting a pic's binary content.
     */
    // binData?: any;
    /**
     * access to raw result ibgib that caller must cast to the correct shape.
     */
    rawResultIbGib?: IbGib_V1;
}

export interface PutIbGibOpts {
    /**
     * ibGib to put.
     *
     * If you only want to do just one, use this param. Otherwise, use the
     * `ibGibs` array param.
     */
    ibGib?: IbGib_V1;
    /**
     * ibGibs to put in the space.
     *
     * If you want to put more than one ibGib, use this param. If you only
     * want to put a single ibGib, you could also use the `ibGib` param.
     */
    ibGibs?: IbGib_V1[];
    /**
     * if true, will store this data in the bin folder with its hash.
     */
    // binData?: string;
    /**
     * If true, will store in a different folder.
     */
    isDna?: boolean;
    /**
     * extension to store the bindata with.
     */
    // binExt?: string;
    /**
     * If true, will replace an existing ibGib file
     */
    force?: boolean;
    /**
     * space into which we shall put the ibgib.
     *
     * @default localCurrentSpace
     */
    space?: IbGibSpaceAny,
}

/**
 * Result for putting ibgib.
 */
export interface PutIbGibResult extends FileResult {
    binHash?: string;
}

export interface DeleteIbGibOpts extends GetIbGibOpts { }
export interface DeleteIbGibResult extends FileResult { }

// #endregion from common/types/legacy.ts

/**
 * Special ibgib types, used for metadata within a space.
 *
 * Unsure where to place this in core-gib...
 *
 * ## notes
 *
 * * 10/2023
 *   * I'm changing this to also be unioned with 'string' because I am working
 *     on ibgib app and I need to consume the create special ibgib. so this
 *     should be extensible per use case in consuming apps.
 */
export type SpecialIbGibType =
    "tags" | "roots" |
    // "latest" |
    "outerspaces" | "secrets" |
    "encryptions" | "autosyncs" | "robbots" | "apps" | "history" |
    string;
/**
 * Special ibgib types, used for metadata within a space.
 *
 * Unsure where to place this...
 *
 * @see {@link SPECIAL_IBGIB_TYPE_REGEXP}
 */
export const SpecialIbGibType = {
    /** indexes all tag ibgibs within a space */
    tags: "tags" as SpecialIbGibType,
    /** indexes all root ibgibs within a space */
    roots: "roots" as SpecialIbGibType,
    // /**
    //  * Ephemeral index ibgib that maps a tjp address -> latest local address in
    //  * a space.
    //  */
    // latest: "latest" as SpecialIbGibType,
    /** indexes all outerspace ibgibs, including sync spaces, within a space */
    outerspaces: "outerspaces" as SpecialIbGibType,
    /** indexes all secret ibgibs within a space */
    secrets: "secrets" as SpecialIbGibType,
    /** indexes all encryption setting ibgibs within a space */
    encryptions: "encryptions" as SpecialIbGibType,
    /** indexes all tjp addresses that automatically sync. */
    autosyncs: "autosyncs" as SpecialIbGibType,
    robbots: "robbots" as SpecialIbGibType,
    apps: "apps" as SpecialIbGibType,
    /** timelines history */
    history: "history" as SpecialIbGibType,
}

/**
 * There has been a new ibGib that is the latest for a given tjp timeline.
 */
export interface IbGibTimelineUpdateInfo extends IbGib_V1 {
    ib: 'IbGibTimelineUpdateInfo',
    tjpAddr?: IbGibAddr;
    latestAddr: IbGibAddr;
    latestIbGib?: IbGib_V1<any>;
}

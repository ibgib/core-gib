import { IbGibAddr } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGibRel8ns_V1, IbGib_V1 } from '@ibgib/ts-gib/dist/V1/index.mjs';
import { TAG_REL8N_NAME } from './tag-constants.mjs';



export interface TagData_V1 {
    text: string;
    icon?: string;
    description?: string;
}

export interface TagRel8ns_V1 extends IbGibRel8ns_V1 {
    [TAG_REL8N_NAME]?: IbGibAddr[];
}

export interface TagIbGib_V1 extends IbGib_V1<TagData_V1, TagRel8ns_V1> {

}

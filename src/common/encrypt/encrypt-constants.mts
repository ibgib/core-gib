import { SaltStrategy } from '@ibgib/encrypt-gib/dist/index.mjs';

export const DEFAULT_ENCRYPTION_INITIAL_RECURSIONS = 50000;
export const MIN_ENCRYPTION_INITIAL_RECURSIONS = 1000;
export const MAX_ENCRYPTION_INITIAL_RECURSIONS = 10000000;
export const MIN_ENCRYPTION_RECURSIONS_PER_HASH = 1;
export const MAX_ENCRYPTION_RECURSIONS_PER_HASH = 1000;
export const MIN_ENCRYPTION_SALT_LENGTH = 50;
export const MAX_ENCRYPTION_SALT_LENGTH = 99999;
export const MIN_ENCRYPTION_PASSWORD_LENGTH = 8;
export const MAX_ENCRYPTION_PASSWORD_LENGTH = 9999999;
export const DEFAULT_ENCRYPTION_SALT_STRATEGY: SaltStrategy = SaltStrategy.appendPerHash;
export const DEFAULT_ENCRYPTION_RECURSIONS_PER_HASH = 10;
export const DEFAULT_ENCRYPTION_HASH_ALGORITHM = 'SHA-256';

/**
 * A secrets ibgib uses this rel8n name for its children secrets.
 */
export const SECRET_REL8N_NAME = 'secret';
/**
 * An encryptions ibgib uses this rel8n name for its children encryptions.
 */
export const ENCRYPTION_REL8N_NAME = 'encryption';
/**
 * Related encrypted ciphertext ibgibs will use this rel8n name.
 * Those ciphertext ibgibs will then relate to the encryption used.
 */
export const CIPHERTEXT_REL8N_NAME = 'ciphertext';
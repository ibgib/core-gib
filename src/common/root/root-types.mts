/**
 * Should be refactored to be V1 as other data types.
 */
export interface RootData {
    text: string;
    icon?: string;
    description?: string;
}

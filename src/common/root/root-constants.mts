/**
 * The main roots^gib ibgib uses this rel8n name to keep track of roots.
 *
 * NOTE: This is different than the rel8nName that a root ibgib uses to link to its ibgibs!
 * {@link DEFAULT_ROOT_REL8N_NAME}
 */
export const ROOT_REL8N_NAME = 'root';

/**
 * rel8n name used inside the root to those ibgib it contains. (root.rel8ns.x = [pic1^gib, comment2^gib, ...])
 *
 *
 * NOTE: This is different than the rel8nName 'root' that the roots^gib uses. (roots.rel8ns.root = [root1^gib, root2^gib, ...])
 * {@link ROOT_REL8N_NAME}
 *
 * @example
 * ```json
 *  {
 *      ib: root,
 *      gib: ABC123,
 *      data: {...},
 *      rel8ns: {
 *          [rel8nName]: ["a^1", "b^2"]
 *      }
 *  }
 * ```
 */
export const DEFAULT_ROOT_REL8N_NAME = 'x';

/**
 *
 */
export const DEFAULT_ROOT_TEXT = 'root';
/**
 * Default icon specifically for roots.
 */
export const DEFAULT_ROOT_ICON = 'analytics-outline';
/**
 * Default description specifically for roots.
 */
export const DEFAULT_ROOT_DESCRIPTION = 'This is a root ibgib, which is basically like a root folder that is primarily responsible for "containing" other ibgibs.';

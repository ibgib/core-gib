/**
 * Used in ErrorIbGib_V1.ib
 */
export const DEFAULT_ERROR_MSG_IB_SUBSTRING_LENGTH = 20;
/**
 * regexp for an error ibgib's (ErrorIbGib_V1) ib property.
 */
export const ERROR_IB_REGEXP = /^error (\w+) ([a-fA-F\d]{32}|undefined)$/;

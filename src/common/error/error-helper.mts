import { Ib } from '@ibgib/ts-gib/dist/types.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/index.mjs';

import { GLOBAL_LOG_A_LOT } from '../../core-constants.mjs';
import { ErrorData_V1, ErrorIbGib_V1, ErrorRel8ns_V1 } from './error-types.mjs';
import { constantIbGib } from '../other/ibgib-helper.mjs';
import { DEFAULT_ERROR_MSG_IB_SUBSTRING_LENGTH, ERROR_IB_REGEXP } from './error-constants.mjs';
import { ERROR_MSG_LOCATION_ONLY_REGEXP, ERROR_MSG_WITH_ID_CAPTURE_GROUPS_REGEXP } from '@ibgib/helper-gib';


const logalot = GLOBAL_LOG_A_LOT || false;

/**
 * Generates an ib based on a raw error msg.
 *
 * ## future
 *
 * If this is changed in the future, then without versioning of some sort, this
 * will change the error constant ibgibs that rely on this functions
 * deterministic qualities.
 *
 * @returns the error's `ib`
 */
export function getErrorIb({
    rawMsg,
}: {
    rawMsg: string,
}): Ib {
    const lc = `[${getErrorIb.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting...`); }
        const parsed = parseRawErrorMsg({ rawMsg });
        const saferText = parsed.body.replace(/\s/g, '_').replace(/\W/g, '');
        let msgSlice: string;
        if (saferText.length > DEFAULT_ERROR_MSG_IB_SUBSTRING_LENGTH) {
            msgSlice =
                saferText.substring(0, DEFAULT_ERROR_MSG_IB_SUBSTRING_LENGTH);
        } else if (saferText.length > 0) {
            msgSlice = saferText;
        } else {
            // msg only has characters/nonalphanumerics ?
            throw new Error(`(UNEXPECTED) error msg should have characters/alphanumerics... (E: a3b9cd11a44cc7892a748819c2885422)`);
        }

        return `error ${msgSlice} ${parsed.uuid ?? 'undefined'}`;
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * Parses a raw error/exception message.
 *
 * If it has sections that I personally use (usually), then it will break that
 * out. otherwise, it will just have the `rawMsg` as the msg body and raw msg.
 *
 * @see {@link ErrorData_V1}
 */
export function parseRawErrorMsg({
    rawMsg,
}: {
    rawMsg: string,
}): ErrorData_V1 {
    const lc = `[${parseRawErrorMsg.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting...`); }
        if (!rawMsg) { throw new Error(`(UNEXPECTED) rawMsg required (E: e5bd3b433a1781ebe885534cd2495622)`); }

        let data: ErrorData_V1;
        let regexResult = rawMsg.match(ERROR_MSG_WITH_ID_CAPTURE_GROUPS_REGEXP);
        if (regexResult) {
            // has id section
            const [_, location, unexpectedAtStart, body, idSection, unexpectedAtEnd] = regexResult;
            if (!body) { throw new Error(`invalid error msg body (E: a675e6855cca96519d33d44ea5400922)`); }
            data = {
                success: false,
                raw: rawMsg,
                body: body?.trim(),
                uuid: idSection.slice(4, 36),
            };
            if (location) { data.location = location; }
            if (unexpectedAtStart || unexpectedAtEnd) { data.unexpected = true; }
        } else {
            // no id or unexpected regex (maybe changed?)
            data = {
                success: false,
                raw: rawMsg,
                body: rawMsg,
            };
            let regexResultLocation = rawMsg.match(ERROR_MSG_LOCATION_ONLY_REGEXP);
            if (regexResultLocation) {
                const [_, location] = regexResultLocation;
                data.location = location;
            }
            if (rawMsg.toLowerCase().includes(`(unexpected)`)) {
                data.unexpected = true;
            }
        }

        return data;
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * Builds a "constant" error ibgib based on the given `rawMsg`.
 *
 * @returns constant error ibgib built from given `rawMsg`
 *
 * @see {@link ErrorData_V1}
 * @see {@link ErrorIbGib_V1}
 */
export function getErrorIbGib({ rawMsg }: { rawMsg: string }): Promise<ErrorIbGib_V1> {
    const lc = `[${getErrorIbGib.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting...`); }

        return constantIbGib<ErrorData_V1, ErrorRel8ns_V1>({
            parentPrimitiveIb: 'error',
            ib: getErrorIb({ rawMsg }),
            data: parseRawErrorMsg({ rawMsg }),
            ibRegExpPattern: ERROR_IB_REGEXP.source,
        });
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export function isError({
    ibGib
}: {
    ibGib: IbGib_V1
}): boolean {
    const lc = `[${isError.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting...`); }
        if (!ibGib) { throw new Error(`ibGib required (E: 1d756fbbd96f1734b97ba013537ed522)`); }
        return ibGib.ib.startsWith('error ');
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

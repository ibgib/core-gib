import {
    firstOfEach, firstOfAll, ifWe,
    lastOfEach, lastOfAll,
    ifWeMight, iReckon, respecfully
} from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

let crypto = globalThis.crypto;
let { subtle } = crypto;

import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';

import { fooClone, fooFactory, fooGetIbAndGib } from './core-helper.mjs';

/**
 * Test helper functions.
 */

await respecfully(sir, `foo clone`, async () => {

    await ifWe(sir, `should foo Clone yo (testing node/browser isomorphic packages)`, () => {
        let obj = 1;
        let clone = fooClone(obj);
        iReckon(sir, clone).asTo('clone foo yo').isGonnaBe(obj);
        // expect(1).asTo('expected fail test case').isGonnaBe(2);
    });

    await ifWe(sir, `should foo getibAndGib yo (testing node/browser isomorphic packages)`, () => {
        let result = fooGetIbAndGib();
        iReckon(sir, result.ib).asTo('ib').isGonnaBe('ib');
    });

    await ifWe(sir, `should foo factory yo (testing node/browser isomorphic packages)`, async () => {
        let result = await fooFactory();
        iReckon(sir, result).asTo('result').isGonnaBeTruthy();
        iReckon(sir, result.ib).asTo('result.ib').isGonnaBeTruthy();
    });

});

async function hash({
    s,
}: {
    s: string,
}): Promise<string> {
    if (!s) { return ''; }
    let algorithm = 'SHA-256';

    try {
        const msgUint8 = new TextEncoder().encode(s);
        const buffer = await subtle.digest(algorithm, msgUint8);
        const asArray = Array.from(new Uint8Array(buffer));
        return asArray.map(b => b.toString(16).padStart(2, '0')).join('');
    } catch (e) {
        console.error(e.message ?? e);
        throw e;
    }
}

await respecfully(sir, `non-ts-gib isomorphic crypto hashing`, async () => {

    await ifWe(sir, `should digest simple string consistently using crypto.subtle directly `, async () => {
        let h = await hash({ s: '42' });
        iReckon(sir, h).asTo('42').isGonnaBe('73475cb40a568e8da8a045ced110137e159f890ac4da883b6b17dc651b3a8049');
    });
    await ifWe(sir, `should digest simple stringified ibgib consistently using crypto.subtle directly `, async () => {
        let ibgib: IbGib_V1 = { ib: 'ib', gib: 'gib' };
        let h = await hash({ s: JSON.stringify(ibgib) }); // doesn't use ts-gib but consistent stringifying json is important
        iReckon(sir, h).asTo('ib^gib').isGonnaBe('cbad0694a257358c044611ea1fa88ace71a01a9b8409d2354d0387d8043f7671');
    });
});

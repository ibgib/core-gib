import { default as pathUtils } from 'path';

import { delay, getUUID, } from '@ibgib/helper-gib';

/**
 * helper function to get the filename for use in specs.
 *
 * @param importMetaUrl import.meta.url
 * @returns currently executing filename sans .respec.mjs
 */
export async function getCurrentFilename(importMetaUrl: string): Promise<string> {
    const lc = `[${getCurrentFilename.name}]`;
    try {
        const pieces = importMetaUrl.split(pathUtils.sep);
        let filename = pieces.at(-1)?.replace(/\.respec\.mjs$/, '')
        if (!filename) {
            console.warn(`filename not able to be gotten?`);
            filename = (new Date()).toTimeString() + (await getUUID());
        }
        return `ibgib${pathUtils.sep}${filename}`;
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    }
}

export async function specGetSpaceId(): Promise<string> {
    await delay(10);
    let uuid = await getUUID();
    let now = new Date();
    now.getMinutes();
    let timeComponent = now.getMinutes().toString() + now.getSeconds().toString() + now.getMilliseconds().toString()
    uuid = timeComponent + uuid.substring(timeComponent.length, uuid.length);
    return uuid;
}
export function specGetSpaceName(uuid: string): string {
    return uuid.substring(0, 8) + '-space';
}

export function specGetSpaceDescription(uuid: string): string {
    return 'description for ' + uuid.substring(0, 8);
}
export function specGetBaseDir(testBaseDirRoot: string, uuid: string, logalot: boolean, lc: string): string {
    const dir = pathUtils.join(".", testBaseDirRoot, specGetSpaceName(uuid) + "_baseDir");
    if (logalot) { console.log(`${lc}[${specGetBaseDir.name}] dir: ${dir} (I: 410c57b7d02ac2ddf9ca32493ad19523)`); }
    return dir;
}
export function specGetBaseSubPath(testBaseDirRoot: string, uuid: string, logalot: boolean, lc: string): string {
    return "tmp";
}

/**
 * @module core-constants
 *
 * # IMPORTANT!
 *
 * In order to be able to actually use this in an ionic-gib-like clone front end,
 * I want to be sure to include all of these. So I can't remove/comment it out here
 * without putting it _somewhere_. For example, the very first import atow is a
 * reference to @capacitor/filesystem. This should not go in this package (core-gib)
 * so I'm going to create another lib specific to ionic/capacitor and put it there
 * **before** removing it from/commenting it out in this file.
 *
 * # about this file
 *
 * I'm copying this lump sum from ionic-gib and am breaking it out slowly into
 * other more specific constant files. When a constant is moved into one of
 * those sub files, then I will remove it from this file.
 *
 * Ultimately, this should only contain global constants like the logalot
 * and timer settings.
 */

import { GIB } from "@ibgib/ts-gib/dist/V1/constants.mjs";
import { IbGib_V1 } from "@ibgib/ts-gib/dist/V1/types.mjs";


/**
 * Naive selective logging/tracing mechanism.
 *
 * I manually switch this in individual files as needed while
 * developing/troubleshooting. I can change it here to turn on extremely verbose
 * logging application wide (and turn off individual files).
 */
export const GLOBAL_LOG_A_LOT: boolean | number = false;

/**
 * Used in console.timeLog() calls.
 */
export const GLOBAL_TIMER_NAME = '[core^gib timer]';


// export const UUID_REGEXP = /^[a-zA-Z0-9_\-.]{1,256}$/;

/**
 * regular expression for a classname.
 *
 * Used in witnesses atm.
 */
export const CLASSNAME_REGEXP = /^[a-zA-Z0-9_]{1,255}$/;

/**
 * Much restricted list of chars in english.
 *
 * ## intent
 *
 * When sanitizing input.
 */
export const SAFE_SPECIAL_CHARS = `.'",!?-`;

export const IB_MAX_LENGTH_DEFAULT = 155;

/**
 * Defaults to word characters, space, tab, hyphen, and other
 * non-slash (path navigating) chars.
 *
 * Does not allow new lines or other whitespace, only tabs and spaces.
 *
 * ## atow
 *
 * `^[\\w\\t\\-|=+.&%\$#@!~\` \\[\\]\\(\\)\\{\\}]{1,${IB_MAX_LENGTH_DEFAULT}}$`
 */
export const IB_REGEXP_DEFAULT = new RegExp(`^[\\w\\t\\-|=+.&%\$#@!~\` \\[\\]\\(\\)\\{\\}]{1,${IB_MAX_LENGTH_DEFAULT}}$`);

/**
 * Ionicons const "enum"
 */
export const IONICONS = [
    'add',
    'add-circle',
    'alert',
    'alert-circle',
    'add',
    'airplane',
    'alarm',
    'albums',
    'alert',
    'alert-circle',
    'american-football',
    'analytics',
    'aperture',
    'apps',
    'archive',
    'arrow-back',
    'arrow-back-circle',
    'arrow-down',
    'arrow-down-circle',
    'arrow-forward',
    'arrow-forward-circle',
    'arrow-redo',
    'arrow-redo-circle',
    'arrow-undo',
    'arrow-undo-circle',
    'arrow-up',
    'arrow-up-circle',
    'at',
    'at-circle',
    'attach',
    'backspace',
    'bandage',
    'bar-chart',
    'barbell',
    'barcode',
    'baseball',
    'basket',
    'basketball',
    'battery-charging',
    'battery-dead',
    'battery-full',
    'battery-half',
    'beaker',
    'bed',
    'beer',
    'bicycle',
    'bluetooth',
    'boat',
    'body',
    'bonfire',
    'book',
    'bookmark',
    'bookmarks',
    'briefcase',
    'browsers',
    'brush',
    'bug',
    'build',
    'bulb',
    'bus',
    'business',
    'cafe',
    'calculator',
    'calendar',
    'call',
    'camera',
    'camera-reverse',
    'car',
    'car-sport',
    'card',
    'caret-back',
    'caret-back-circle',
    'caret-down',
    'caret-down-circle',
    'caret-forward',
    'caret-forward-circle',
    'caret-up',
    'caret-up-circle',
    'cart',
    'cash',
    'cellular',
    'chatbox',
    'chatbox-ellipses',
    'chatbubble',
    'chatbubble-ellipses',
    'chatbubbles',
    'checkbox',
    'checkmark',
    'checkmark-circle',
    'checkmark-done',
    'checkmark-done-circle',
    'chevron-back',
    'chevron-back-circle',
    'chevron-down',
    'chevron-down-circle',
    'chevron-forward',
    'chevron-forward-circle',
    'chevron-up',
    'chevron-up-circle',
    'clipboard',
    'close',
    'close-circle',
    'cloud',
    'cloud-circle',
    'cloud-done',
    'cloud-download',
    'cloud-offline',
    'cloud-upload',
    'cloudy',
    'cloudy-night',
    'code',
    'code-download',
    'code-slash',
    'code-working',
    'cog',
    'color-fill',
    'color-filter',
    'color-palette',
    'color-wand',
    'compass',
    'construct',
    'contract',
    'contrast',
    'copy',
    'create',
    'crop',
    'cube',
    'cut',
    'desktop',
    'disc',
    'document',
    'document-attach',
    'document-text',
    'documents',
    'download',
    'duplicate',
    'ear',
    'earth',
    'easel',
    'egg',
    'ellipse',
    'ellipsis-horizontal',
    'ellipsis-horizontal-circle',
    'ellipsis-vertical',
    'ellipsis-vertical-circle',
    'enter',
    'exit',
    'expand',
    'eye',
    'eye-off',
    'eyedrop',
    'fast-food',
    'female',
    'file-tray',
    'file-tray-full',
    'file-tray-stacked',
    'film',
    'filter',
    'finger-print',
    'fitness',
    'flag',
    'flame',
    'flash',
    'flash-off',
    'flashlight',
    'flask',
    'flower',
    'folder',
    'folder-open',
    'football',
    'funnel',
    'game-controller',
    'gift',
    'git-branch',
    'git-commit',
    'git-compare',
    'git-merge',
    'git-network',
    'git-pull-request',
    'glasses',
    'globe',
    'golf',
    'grid',
    'hammer',
    'hand-left',
    'hand-right',
    'happy',
    'hardware-chip',
    'headset',
    'heart',
    'heart-circle',
    'heart-dislike',
    'heart-dislike-circle',
    'heart-half',
    'help',
    'help-buoy',
    'help-circle',
    'home',
    'hourglass',
    'ice-cream',
    'image',
    'images',
    'infinite',
    'information',
    'information-circle',
    'journal',
    'key',
    'keypad',
    'language',
    'laptop',
    'layers',
    'leaf',
    'library',
    'link',
    'list',
    'list-circle',
    'locate',
    'location',
    'lock-closed',
    'lock-open',
    'log-in',
    'magnet',
    'mail',
    'mail-open',
    'mail-unread',
    'male',
    'male-female',
    'man',
    'map',
    'medal',
    'medical',
    'medkit',
    'megaphone',
    'menu',
    'mic',
    'mic-circle',
    'mic-off',
    'mic-off-circle',
    'moon',
    'move',
    'musical-note',
    'musical-notes',
    'navigate',
    'navigate-circle',
    'newspaper',
    'notifications',
    'notifications-circle',
    'notifications-off',
    'notifications-off-circle',
    'nuclear',
    'nutrition',
    'open',
    'options',
    'paper-plane',
    'partly-sunny',
    'pause',
    'pause-circle',
    'paw',
    'pencil',
    'people',
    'people-circle',
    'person',
    'person-add',
    'person-circle',
    'person-remove',
    'phone-landscape',
    'phone-portrait',
    'pie-chart',
    'pin',
    'pint',
    'pizza',
    'planet',
    'play',
    'play-back',
    'play-back-circle',
    'play-circle',
    'play-forward',
    'play-forward-circle',
    'play-skip-back',
    'play-skip-back-circle',
    'play-skip-forward',
    'play-skip-forward-circle',
    'podium',
    'power',
    'pricetag',
    'pricetags',
    'print',
    'pulse',
    'push',
    'qr-code',
    'radio',
    'radio-button-off',
    'radio-button-on',
    'rainy',
    'reader',
    'receipt',
    'recording',
    'refresh',
    'refresh-circle',
    'reload',
    'reload-circle',
    'remove',
    'remove-circle',
    'reorder-four',
    'reorder-three',
    'reorder-two',
    'repeat',
    'resize',
    'restaurant',
    'return-down-back',
    'return-down-forward',
    'return-up-back',
    'return-up-forward',
    'ribbon',
    'rocket',
    'rose',
    'sad',
    'save',
    'scan',
    'scan-circle',
    'school',
    'search',
    'search-circle',
    'send',
    'server',
    'settings',
    'shapes',
    'share',
    'share-social',
    'shield',
    'shield-checkmark',
    'shirt',
    'shuffle',
    'skull',
    'snow',
    'speedometer',
    'square',
    'star',
    'star-half',
    'stats-chart',
    'stop',
    'stop-circle',
    'stopwatch',
    'subway',
    'sunny',
    'swap-horizontal',
    'swap-vertical',
    'sync',
    'sync-circle',
    'tablet-landscape',
    'tablet-portrait',
    'tennisball',
    'terminal',
    'text',
    'thermometer',
    'thumbs-down',
    'thumbs-up',
    'thunderstorm',
    'time',
    'timer',
    'today',
    'toggle',
    'trail-sign',
    'train',
    'transgender',
    'trash',
    'trash-bin',
    'trending-down',
    'trending-up',
    'triangle',
    'trophy',
    'tv',
    'umbrella',
    'videocam',
    'volume-high',
    'volume-low',
    'volume-medium',
    'volume-mute',
    'volume-off',
    'walk',
    'wallet',
    'warning',
    'watch',
    'water',
    'wifi',
    'wine',
    'woman',
];

// #region other robbot related

/**
 * Robbot.data.name regexp
 */
export const ROBBOT_NAME_REGEXP = /^[a-zA-Z0-9_\-.]{1,255}$/;
export const ROBBOT_PREFIX_SUFFIX_REGEXP = /^[a-zA-Z0-9_\-.\s👀🤖:;&]{1,64}$/;
export const ROBBOT_PREFIX_SUFFIX_REGEXP_DESC =
    `0 to 64 alphanumerics, spaces, select special characters and emojis.`;

// #endregion other robbot related

// #region gestures

export const GESTURE_DOUBLE_CLICK_THRESHOLD_MS = 500;
/**
 * If a gesture's move threshold is less than this, then it can still be
 * considered a single punctiliar click. Otherwise, it's a move event.
 *
 * IOW, a click gesture only is considered a "click" if the onMove is triggered
 * less than this many times. If onMove is detected more than this many times,
 * then a move gesture will be triggered.
 */
export const GESTURE_CLICK_TOLERANCE_ONMOVE_THRESHOLD_COUNT = 5;

// #endregion gestures

export const SIMPLE_CONFIG_KEY_APP_VISIBLE = 'appBarVisible';
export const SIMPLE_CONFIG_KEY_APP_SELECTED = 'appBarSelectedApp';
export const SIMPLE_CONFIG_KEY_ROBBOT_VISIBLE = 'robbotBarVisible';
export const SIMPLE_CONFIG_KEY_ROBBOT_SELECTED_ADDR = 'robbotBarSelectedAddr';

// #region app

export const DEFAULT_APP_ICON = 'apps-outline';

// #endregion app

export const YOUTUBE_LINK_REG_EXP = /^https:\/\/youtu\.be\/\w+$/;

export const WEB_1_PATHS = ['welcome', 'about-us', 'your-data'];

## 0.0.1
* getting basic folder structure going
  * working mostly off of the existing ts-gib (v0.4.1)
  * removed mocha
  * trying out solutions for unit testing in both node and browser.
    * spaces should be executing in both contexts.
    * jasmine seems to be working atm for node.
      * trying just jasmine because it "should" be able to work even
        without using karma as the test runner.
      * i only need rudimentary headless running at the space level (this lib)
        because i'm not actually doing an app here.
